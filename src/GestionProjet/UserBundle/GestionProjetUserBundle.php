<?php

namespace GestionProjet\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class GestionProjetUserBundle extends Bundle
{
	public function getParent()

  {
    return 'FOSUserBundle';
  }
}
