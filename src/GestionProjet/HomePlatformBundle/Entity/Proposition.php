<?php

namespace GestionProjet\HomePlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Proposition
 *
 * @ORM\Table(name="proposition")
 * @ORM\Entity(repositoryClass="GestionProjet\HomePlatformBundle\Repository\PropositionRepository")
 */
class Proposition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idUser", type="integer")
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="entreprise", type="string", length=255)
     */
    private $entreprise;

    /**
     * @var string
     *
     * @ORM\Column(name="intituleProjet", type="string", length=255)
     */
    private $intituleProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(name="mission", type="string", length=255)
     */
    private $mission;



    /**
     * @var int
     *
     * @ORM\Column(name="accompagnant", type="integer")
     */
    private $accompagnant;

    /**
     * @var bool
     *
     * @ORM\Column(name="present", type="boolean")
     */
    private $present;

    /**
     * @var bool
     *
     * @ORM\Column(name="repas", type="boolean")
     */
    private $repas;

    /**
     * @var bool
     *
     * @ORM\Column(name="valide", type="integer")
     */
    private $valide;


    /**
     * @var date $insertDate
     *
     * @ORM\Column(name="insertDate", type="date", nullable=false)
     */
    private $insertDate;

    /**
     * Set insertDate
     *
     * @param date $date
     *
     * @return Proposition
     */
    public function setInsertDate($date)
    {
        $this->insertDate = $date;
        return $this;
    }

    /**
     * Get insertDate
     *
     * @return date
     */
    public function getInsertDate()
    {
        return $this->insertDate;
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Proposition
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Proposition
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set entreprise
     *
     * @param string $entreprise
     *
     * @return Proposition
     */
    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Get entreprise
     *
     * @return string
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * Set intituleProjet
     *
     * @param string $intituleProjet
     *
     * @return Proposition
     */
    public function setIntituleProjet($intituleProjet)
    {
        $this->intituleProjet = $intituleProjet;

        return $this;
    }

    /**
     * Get intituleProjet
     *
     * @return string
     */
    public function getIntituleProjet()
    {
        return $this->intituleProjet;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Proposition
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set mission
     *
     * @param string $mission
     *
     * @return Proposition
     */
    public function setMission($mission)
    {
        $this->mission = $mission;

        return $this;
    }

    /**
     * Get mission
     *
     * @return string
     */
    public function getMission()
    {
        return $this->mission;
    }

    /**
     * Set accompagnant
     *
     * @param integer $accompagnant
     *
     * @return Proposition
     */
    public function setAccompagnant($accompagnant)
    {
        $this->accompagnant = $accompagnant;

        return $this;
    }

    /**
     * Get accompagnant
     *
     * @return int
     */
    public function getAccompagnant()
    {
        return $this->accompagnant;
    }

    /**
     * Set present
     *
     * @param boolean $present
     *
     * @return Proposition
     */
    public function setPresent($present)
    {
        $this->present = $present;

        return $this;
    }

    /**
     * Get present
     *
     * @return bool
     */
    public function getPresent()
    {
        return $this->present;
    }

    /**
     * Set repas
     *
     * @param boolean $repas
     *
     * @return Proposition
     */
    public function setRepas($repas)
    {
        $this->repas = $repas;

        return $this;
    }

    /**
     * Get repas
     *
     * @return bool
     */
    public function getRepas()
    {
        return $this->repas;
    }

    /**
     * Set valide
     *
     * @param boolean $valide
     *
     * @return Proposition
     */
    public function setValide($valide)
    {
        $this->valide = $valide;

        return $this;
    }

    /**
     * Get valide
     *
     * @return bool
     */
    public function getValide()
    {
        return $this->valide;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return Proposition
     */
    public function setidUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer
     */
    public function getidUser()
    {
        return $this->idUser;
    }

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload the product brochure as a PDF file.")
     * @Assert\File(mimeTypes={ "application/pdf" })
     */
    private $brochure;

    /**
     * Get description
     *
     * @return string
     */
    public function getBrochure()
    {
        return $this->brochure;
    }

    /**
     * Set brochure
     *
     * @param string $brochure
     *
     * @return Proposition
     */
    public function setBrochure($brochure)
    {
        $this->brochure = $brochure;

        return $this;
    }
}

