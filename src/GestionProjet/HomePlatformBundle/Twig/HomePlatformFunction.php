<?php

/**
 * Created by PhpStorm.
 * User: foulon
 * Date: 24/03/16
 * Time: 12:08
 */

namespace GestionProjet\HomePlatformBundle\Twig;
use Symfony\Bridge\Doctrine\RegistryInterface;

class HomePlatformFunction extends \Twig_Extension
{


    protected $doctrine;

    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getName()
    {
       return 'home_paltform_function';
    }

    public  function getFunctions(){
        return array(
            'getNumberOfProposition' => new \Twig_Function_Method($this, 'getNumberOfProposition'),
            'getNumberOfValide' => new \Twig_Function_Method($this, 'getNumberOfValide'),
            'getNumberOfPending' => new \Twig_Function_Method($this, 'getNumberOfPending'),
            'getNumberOfNotValide' => new \Twig_Function_Method($this, 'getNumberOfNotValide'),
        );
    }

    public function getNumberOfProposition(){
        $repository = $this->doctrine->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $listProposition = $repository->findAll();
        return sizeof($listProposition);
    }

    public function getNumberOfValide(){
        $repository = $this->doctrine->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $listProposition = $repository->findByValide(1);
        return sizeof($listProposition);
    }

    public function getNumberOfPending(){
        $repository = $this->doctrine->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $listProposition = $repository->findByValide(0);
        return sizeof($listProposition);
    }

    public function getNumberOfNotValide(){
        $repository = $this->doctrine->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $listProposition = $repository->findByValide(2);
        return sizeof($listProposition);
    }





}

