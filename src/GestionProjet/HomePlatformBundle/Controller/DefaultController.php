<?php

namespace GestionProjet\HomePlatformBundle\Controller;

use GestionProjet\HomePlatformBundle\Entity\DemandeCompte;
use GestionProjet\HomePlatformBundle\Form\DemandeCompteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GestionProjet\HomePlatformBundle\Entity\Proposition;
use GestionProjet\HomePlatformBundle\Form\PropositionType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    public function homeAction()
    {
        return $this->render('GestionProjetHomePlatformBundle:Default:index.html.twig');
    }

    public function homepromoAction()
    {
        return $this->render('GestionProjetHomePlatformBundle:Default:indexPromo.html.twig');
    }



    public function adminAction()
    {
        return $this->render('GestionProjetHomePlatformBundle::layout.html.twig');
    }

    public function askAccountAction(Request $request)
    {
        $demandeCompte = new DemandeCompte();
        $form = $this->createForm(DemandeCompteType::class,$demandeCompte);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($demandeCompte);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Votre demande de création de compte a bien été prise en compte, un mail de validation vous sera envoyé.');
            return $this->redirect($this->generateUrl('gestion_projet_home_platform_projet_sent_success'));
        }
        return $this->render('GestionProjetHomePlatformBundle:Default:askAccount.html.twig', array(
            'form' => $form->createView(),));


    }

    public function propositionAction(Request $request){
        $proposition = new Proposition();
        $form = $this->createForm(PropositionType::class, $proposition);
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $id = $user->getId();
        $proposition->setidUser($id);
        $proposition->setValide(0);
        $proposition->setInsertDate(new \DateTime());

        if ($form->handleRequest($request)->isValid()) {


            $em = $this->getDoctrine()->getManager();


            // $file stores the uploaded PDF file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $proposition->getBrochure();

            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            // Move the file to the directory where brochures are stored
            $brochuresDir = $this->container->getParameter('kernel.root_dir').'/../web/uploads/brochures';
            $file->move($brochuresDir, $fileName);

            // Update the 'brochure' property to store the PDF file name
            // instead of its contents
            $proposition->setBrochure($fileName);

            // ... persist the $product variable or any other work
            $em->persist($proposition);
            $em->flush();


            return $this->redirect($this->generateUrl('gestion_projet_home_platform_projet_sent_success'));
        }
        return $this->render('GestionProjetHomePlatformBundle:Default:proposition.html.twig', array(
            'form' => $form->createView(),));

    }

    public function demandesAction()
    {
        return $this->render('GestionProjetHomePlatformBundle:Default:index.html.twig');
    }

    public function modalitesAction()
    {
        return $this->render('GestionProjetHomePlatformBundle:Default:modalites.html.twig');
    }


    public function successAction()
    {
        return $this->render('GestionProjetHomePlatformBundle:Default:success.html.twig');
    }

    public function viewPropositionsAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $listProposition = $repository->findAll();
        return $this->render("GestionProjetHomePlatformBundle:Default:viewPropositions.html.twig",array("listProposition" => $listProposition));

    }

    public function addAccountAction()
    {
        return $this->render('GestionProjetHomePlatformBundle:Default:addAccount.html.twig');
    }

    public function validateAction(){
        $repository = $this->getDoctrine()->getManager()->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $listProposition = $repository->findByValide(1);
        return $this->render("GestionProjetHomePlatformBundle:Default:viewValidatePropositions.html.twig",array("listProposition" => $listProposition));
    }

    public function validationAction($id,$state)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $proposition = $repository->find($id);
        $proposition->setValide($state);
        $listProposition = $repository->findAll();
        $em->flush();


        /* envoi mail */
        if (($state) == 1) {
            $message = \Swift_Message::newInstance()
                ->setSubject('ENSICAEN - Confirmation de votre proposition de stage')
                ->setFrom('validation@ensicaen.com')
                ->setTo("nicolas.foulon08@gmail.com")
                ->setBody(
                    $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        '@GestionProjetHomePlatform/Emails/acceptation.html.twig',
                        array('name' => $this->getUser()->getUsername(), 'titre' => $proposition->getDescription())
                    ),
                    'text/html'
                )/*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'Emails/registration.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )
            */
            ;
            $this->get('mailer')->send($message);
        }

        if (($state) == 2) {
            $message = \Swift_Message::newInstance()
                ->setSubject('ENSICAEN - Resultat de votre proposition de stage')
                ->setFrom('validation@ensicaen.com')
                ->setTo("nicolas.foulon08@gmail.com")
                ->setBody(
                    $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        '@GestionProjetHomePlatform/Emails/refus.html.twig',
                        array('name' => $this->getUser()->getUsername(), 'titre' => $proposition->getDescription())
                    ),
                    'text/html'
                )/*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'Emails/registration.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )
            */
            ;
            $this->get('mailer')->send($message);
        }



        return $this->redirectToRoute("gestion_projet_home_platform_view_propositions");
    }

    public function pendingAction(){
        $repository = $this->getDoctrine()->getManager()->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $listProposition = $repository->findByValide(0);
        return $this->render("GestionProjetHomePlatformBundle:Default:viewPendingPropositions.html.twig",array("listProposition" => $listProposition));
    }

    public function notValidateAction(){
        $repository = $this->getDoctrine()->getManager()->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $listProposition = $repository->findByValide(2);
        return $this->render("GestionProjetHomePlatformBundle:Default:viewNotValidatePropositions.html.twig",array("listProposition" => $listProposition));
    }

    public function getNumberOfStage(){
        $repository = $this->getDoctrine()->getManager()->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $listProposition = $repository->findAll();
        return sizeof($listProposition);
    }

    public function viewAccountAction(){
        $repository = $this->getDoctrine()->getManager()->getRepository('GestionProjetUserBundle:User');
        $listAccount = $repository->findAll();
        return $this->render("GestionProjetHomePlatformBundle:Default:viewAccount.html.twig",array("listAccount" => $listAccount));
    }

    public function viewAskingAction(){
        $repository = $this->getDoctrine()->getManager()->getRepository('GestionProjetHomePlatformBundle:DemandeCompte');
        $listAsk = $repository->findAll();
        return $this->render("GestionProjetHomePlatformBundle:Default:viewAsking.html.twig",array("listAsking" => $listAsk));
    }

    public function propositionInDetailAction($id){
        $repository = $this->getDoctrine()->getManager()->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $proposition = $repository->find($id);
        return $this->render("GestionProjetHomePlatformBundle:Default:viewDetailProposition.html.twig", array("proposition" => $proposition));
    }

    public function propositionsValidateInDetailAction(){
        $repository = $this->getDoctrine()->getManager()->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $listProposition = $repository->findByValide(1);
        return $this->render("GestionProjetHomePlatformBundle:Default:viewDetailValidatePropositions.html.twig",array("listProposition" => $listProposition));
    }


    public function viewOwnPropositionsAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $id = $user->getId();
        $listProposition = $repository->findByIdUser($id);
        return $this->render("GestionProjetHomePlatformBundle:Default:viewOwnPropositions.html.twig",array("listProposition" => $listProposition));

    }

    public function deletePropositionAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository =$em->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $propositon = $repository->find($id);

        $em->remove($propositon);
        $em->flush();

        return $this->redirectToRoute("gestion_projet_home_platform_view_own_propositions");

    }

    public function viewAllToPrintAction(){
        $repository = $this->getDoctrine()->getManager()->getRepository('GestionProjetHomePlatformBundle:Proposition');
        $listProposition = $repository->findByValide(1);
        return $this->render("GestionProjetHomePlatformBundle:Default:viewAllToPrint.html.twig",array("listProposition" => $listProposition));
    }

}
