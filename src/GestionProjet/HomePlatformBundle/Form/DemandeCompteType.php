<?php
/**
 * Created by PhpStorm.
 * User: foulon
 * Date: 05/04/16
 * Time: 12:03
 */

namespace GestionProjet\HomePlatformBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class DemandeCompteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class)
            ->add('prenom')
            ->add('entreprise')
            ->add('mail')
            ->add('message')
            ->add('save',SubmitType::class)
            ->getForm();
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionProjet\HomePlatformBundle\Entity\DemandeCompte'
        ));
    }

    public function getName()
    {
        return '';
    }

}