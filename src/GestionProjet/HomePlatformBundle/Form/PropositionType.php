<?php

namespace GestionProjet\HomePlatformBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PropositionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class)
            ->add('prenom')
            ->add('entreprise')
            ->add('intituleProjet')
            ->add('description')
            ->add('mission',ChoiceType::class,array('choices' => array('Etude' => 'etude', 'Prototypage'=>'prototypage','Recherche documentaire' =>'recherche documentaire','Veille'=>'veille','Autre'=>'autre')))
            ->add('accompagnant')
            ->add('present')
            ->add('repas')
            ->add('brochure', FileType::class, array('label' => 'Brochure (PDF file)'))
            ->add('save',SubmitType::class)
            ->getForm();
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionProjet\HomePlatformBundle\Entity\Proposition'
        ));
    }
}
