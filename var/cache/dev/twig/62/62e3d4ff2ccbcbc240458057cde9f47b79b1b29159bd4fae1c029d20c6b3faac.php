<?php

/* FOSUserBundle:ChangePassword:changePassword_content.html.twig */
class __TwigTemplate_b4f4df5d8892d626756ea2d0eb841c85eb10855beb620684b3db7e753f65c127 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_82d9da88c02bf134aa148b9917644bb602d777edd3defd2f1eeac7dadd61d45e = $this->env->getExtension("native_profiler");
        $__internal_82d9da88c02bf134aa148b9917644bb602d777edd3defd2f1eeac7dadd61d45e->enter($__internal_82d9da88c02bf134aa148b9917644bb602d777edd3defd2f1eeac7dadd61d45e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:changePassword_content.html.twig"));

        // line 2
        echo "
<div class=\"container col-md-offset-3\">


    <h2 class=\"col-md-offset-2\">Changement de mot de passe </h2></br>

";
        // line 8
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('routing')->getPath("fos_user_change_password"), "attr" => array("class" => "fos_user_change_password form-horizontal form-prop")));
        echo "
    ";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "

    <div>
        <input type=\"submit\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("change_password.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 14
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "



    </div>
";
        
        $__internal_82d9da88c02bf134aa148b9917644bb602d777edd3defd2f1eeac7dadd61d45e->leave($__internal_82d9da88c02bf134aa148b9917644bb602d777edd3defd2f1eeac7dadd61d45e_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:changePassword_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 14,  40 => 12,  34 => 9,  30 => 8,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* <div class="container col-md-offset-3">*/
/* */
/* */
/*     <h2 class="col-md-offset-2">Changement de mot de passe </h2></br>*/
/* */
/* {{ form_start(form, { 'action': path('fos_user_change_password'), 'attr': { 'class': 'fos_user_change_password form-horizontal form-prop' } }) }}*/
/*     {{ form_widget(form) }}*/
/* */
/*     <div>*/
/*         <input type="submit" value="{{ 'change_password.submit'|trans }}" />*/
/*     </div>*/
/* {{ form_end(form) }}*/
/* */
/* */
/* */
/*     </div>*/
/* */
