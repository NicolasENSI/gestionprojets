<?php

/* GestionProjetHomePlatformBundle:Default:leftBoard.html.twig */
class __TwigTemplate_e38f1c7222e2f850bf2b94a87d86a6c2ac057c602acd97385d756295ffc79822 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'board' => array($this, 'block_board'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4c3892907869248238b96ff7b60383cf7e2f21d15d0c1315558724c70ae8d879 = $this->env->getExtension("native_profiler");
        $__internal_4c3892907869248238b96ff7b60383cf7e2f21d15d0c1315558724c70ae8d879->enter($__internal_4c3892907869248238b96ff7b60383cf7e2f21d15d0c1315558724c70ae8d879_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:leftBoard.html.twig"));

        // line 1
        echo "


";
        // line 4
        $this->displayBlock('board', $context, $blocks);
        // line 25
        echo "
";
        
        $__internal_4c3892907869248238b96ff7b60383cf7e2f21d15d0c1315558724c70ae8d879->leave($__internal_4c3892907869248238b96ff7b60383cf7e2f21d15d0c1315558724c70ae8d879_prof);

    }

    // line 4
    public function block_board($context, array $blocks = array())
    {
        $__internal_3a6fa4aabb6472640bc4f6d61db1379ac70a20e572a49a923769b4bb48e550c9 = $this->env->getExtension("native_profiler");
        $__internal_3a6fa4aabb6472640bc4f6d61db1379ac70a20e572a49a923769b4bb48e550c9->enter($__internal_3a6fa4aabb6472640bc4f6d61db1379ac70a20e572a49a923769b4bb48e550c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "board"));

        // line 5
        echo "    <div class=\"row placeholders\">

        <div class=\"col-xs-6 col-sm-3 col-sm-offset-1 placeholder\">
            <img src=\"http://www.icone-png.com/png/54/53901.png\" width=\"150\" height=\"150\" class=\"img-responsive\" alt=\"Generic placeholder thumbnail\">
            <h4> <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_validate");
        echo "\"> Validés (";
        echo twig_escape_filter($this->env, $this->env->getExtension('home_paltform_function')->getNumberOfValide(), "html", null, true);
        echo ")</a></h4>
            <span class=\"text-muted\"></span>
        </div>
        <div class=\"col-xs-6 col-sm-3 placeholder\">
            <img src=\"http://ihourlyquiz.com/images/twitquizlogo.jpg\" width=\"150\" height=\"150\" class=\"img-responsive\" alt=\"Generic placeholder thumbnail\">
            <h4><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_pending");
        echo "\"> En attente (";
        echo twig_escape_filter($this->env, $this->env->getExtension('home_paltform_function')->getNumberOfPending(), "html", null, true);
        echo ")</a></h4>
            <span class=\"text-muted\"></span>
        </div>
        <div class=\"col-xs-6 col-sm-3 placeholder\">
            <img src=\"https://pixabay.com/static/uploads/photo/2013/07/12/12/40/abort-146072_960_720.png\" width=\"150\" height=\"150\" class=\"img-responsive\" alt=\"Generic placeholder thumbnail\">
            <h4><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_notValidate");
        echo "\"> Non validés (";
        echo twig_escape_filter($this->env, $this->env->getExtension('home_paltform_function')->getNumberOfNotValide(), "html", null, true);
        echo ")</a></h4>
            <span class=\"text-muted\"></span>
        </div>
    </div>

";
        
        $__internal_3a6fa4aabb6472640bc4f6d61db1379ac70a20e572a49a923769b4bb48e550c9->leave($__internal_3a6fa4aabb6472640bc4f6d61db1379ac70a20e572a49a923769b4bb48e550c9_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:leftBoard.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  70 => 19,  60 => 14,  50 => 9,  44 => 5,  38 => 4,  30 => 25,  28 => 4,  23 => 1,);
    }
}
/* */
/* */
/* */
/* {% block board %}*/
/*     <div class="row placeholders">*/
/* */
/*         <div class="col-xs-6 col-sm-3 col-sm-offset-1 placeholder">*/
/*             <img src="http://www.icone-png.com/png/54/53901.png" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">*/
/*             <h4> <a href="{{ path('gestion_projet_home_platform_validate') }}"> Validés ({{ getNumberOfValide() }})</a></h4>*/
/*             <span class="text-muted"></span>*/
/*         </div>*/
/*         <div class="col-xs-6 col-sm-3 placeholder">*/
/*             <img src="http://ihourlyquiz.com/images/twitquizlogo.jpg" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">*/
/*             <h4><a href="{{ path('gestion_projet_home_platform_pending') }}"> En attente ({{ getNumberOfPending() }})</a></h4>*/
/*             <span class="text-muted"></span>*/
/*         </div>*/
/*         <div class="col-xs-6 col-sm-3 placeholder">*/
/*             <img src="https://pixabay.com/static/uploads/photo/2013/07/12/12/40/abort-146072_960_720.png" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">*/
/*             <h4><a href="{{ path('gestion_projet_home_platform_notValidate') }}"> Non validés ({{ getNumberOfNotValide() }})</a></h4>*/
/*             <span class="text-muted"></span>*/
/*         </div>*/
/*     </div>*/
/* */
/* {%endblock%}*/
/* */
/* */
