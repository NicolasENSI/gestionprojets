<?php

/* GestionProjetHomePlatformBundle:Default:viewPropositions.html.twig */
class __TwigTemplate_1017d2499f4d2078a20dafecdbf6438a4a2d921195ec60ebd9e7fcc7158d9ace extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewPropositions.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e71416f4830bc4b814aa94df8e46efee133d8e233c25bcf3bd1d628dc45ba8dc = $this->env->getExtension("native_profiler");
        $__internal_e71416f4830bc4b814aa94df8e46efee133d8e233c25bcf3bd1d628dc45ba8dc->enter($__internal_e71416f4830bc4b814aa94df8e46efee133d8e233c25bcf3bd1d628dc45ba8dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:viewPropositions.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e71416f4830bc4b814aa94df8e46efee133d8e233c25bcf3bd1d628dc45ba8dc->leave($__internal_e71416f4830bc4b814aa94df8e46efee133d8e233c25bcf3bd1d628dc45ba8dc_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_865bf1c172d57889fcca24515e7618ba63f416dde89b8611937622f6b3c96f63 = $this->env->getExtension("native_profiler");
        $__internal_865bf1c172d57889fcca24515e7618ba63f416dde89b8611937622f6b3c96f63->enter($__internal_865bf1c172d57889fcca24515e7618ba63f416dde89b8611937622f6b3c96f63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
        
        $__internal_865bf1c172d57889fcca24515e7618ba63f416dde89b8611937622f6b3c96f63->leave($__internal_865bf1c172d57889fcca24515e7618ba63f416dde89b8611937622f6b3c96f63_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_07939d3cba2d4457aeff886555562daf819b45060629275d8a366d7a23da2a0b = $this->env->getExtension("native_profiler");
        $__internal_07939d3cba2d4457aeff886555562daf819b45060629275d8a366d7a23da2a0b->enter($__internal_07939d3cba2d4457aeff886555562daf819b45060629275d8a366d7a23da2a0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "





    <h1 class=\"page-header\">Tableau de bord</h1>
    ";
        // line 14
        try {
            $this->loadTemplate("GestionProjetHomePlatformBundle:Default:leftBoard.html.twig", "GestionProjetHomePlatformBundle:Default:viewPropositions.html.twig", 14)->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 15
        echo "

    <div class=\"table-responsive\">
        <h2>Liste de projets </h2> <h6><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_view_all_projects_to_print");
        echo "\"> [<span class=\"glyphicon glyphicon-print\"></span> Version imprimable - uniquement validés]</h6></a>
        ";
        // line 19
        try {
            $this->loadTemplate("GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig", "GestionProjetHomePlatformBundle:Default:viewPropositions.html.twig", 19)->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 20
        echo "    </div>


";
        
        $__internal_07939d3cba2d4457aeff886555562daf819b45060629275d8a366d7a23da2a0b->leave($__internal_07939d3cba2d4457aeff886555562daf819b45060629275d8a366d7a23da2a0b_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewPropositions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 20,  79 => 19,  75 => 18,  70 => 15,  63 => 14,  54 => 7,  48 => 6,  35 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/*     <h1 class="page-header">Tableau de bord</h1>*/
/*     {% include 'GestionProjetHomePlatformBundle:Default:leftBoard.html.twig' ignore missing %}*/
/* */
/* */
/*     <div class="table-responsive">*/
/*         <h2>Liste de projets </h2> <h6><a href="{{ path('gestion_projet_home_platform_view_all_projects_to_print') }}"> [<span class="glyphicon glyphicon-print"></span> Version imprimable - uniquement validés]</h6></a>*/
/*         {% include 'GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig' ignore missing %}*/
/*     </div>*/
/* */
/* */
/* {% endblock %}*/
