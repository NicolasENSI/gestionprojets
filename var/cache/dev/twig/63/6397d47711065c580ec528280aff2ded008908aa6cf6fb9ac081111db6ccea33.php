<?php

/* @WebProfiler/Profiler/header.html.twig */
class __TwigTemplate_837bb34eb4bac324bea1f8a5e8d08c34f89dab057315356c3795fa363720df1f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ec160d69c2a32fde7eb2ad2ee27bc38ac20d7d8f6a68eb94beca83ed07fd672 = $this->env->getExtension("native_profiler");
        $__internal_6ec160d69c2a32fde7eb2ad2ee27bc38ac20d7d8f6a68eb94beca83ed07fd672->enter($__internal_6ec160d69c2a32fde7eb2ad2ee27bc38ac20d7d8f6a68eb94beca83ed07fd672_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_6ec160d69c2a32fde7eb2ad2ee27bc38ac20d7d8f6a68eb94beca83ed07fd672->leave($__internal_6ec160d69c2a32fde7eb2ad2ee27bc38ac20d7d8f6a68eb94beca83ed07fd672_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 1,);
    }
}
/* <div id="header">*/
/*     <div class="container">*/
/*         <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>*/
/* */
/*         <div class="search">*/
/*             <form method="get" action="https://symfony.com/search" target="_blank">*/
/*                 <div class="form-row">*/
/*                     <input name="q" id="search-id" type="search" placeholder="search on symfony.com">*/
/*                     <button type="submit" class="btn">Search</button>*/
/*                 </div>*/
/*            </form>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
