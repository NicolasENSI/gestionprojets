<?php

/* @Twig/Exception/error.html.twig */
class __TwigTemplate_6718cb2f2ef8fbf004f7534791ce1b434699db44c435a1c54abc41765bdfae4c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Twig/Exception/error.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ba302c64e842976e440da7c84877600a7e1178f7048e2a30a8bd016a68c2f15 = $this->env->getExtension("native_profiler");
        $__internal_5ba302c64e842976e440da7c84877600a7e1178f7048e2a30a8bd016a68c2f15->enter($__internal_5ba302c64e842976e440da7c84877600a7e1178f7048e2a30a8bd016a68c2f15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5ba302c64e842976e440da7c84877600a7e1178f7048e2a30a8bd016a68c2f15->leave($__internal_5ba302c64e842976e440da7c84877600a7e1178f7048e2a30a8bd016a68c2f15_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_62bf509ba90215210ae7820c9d768646559be6740b95df2e5e8f387bcb3584b8 = $this->env->getExtension("native_profiler");
        $__internal_62bf509ba90215210ae7820c9d768646559be6740b95df2e5e8f387bcb3584b8->enter($__internal_62bf509ba90215210ae7820c9d768646559be6740b95df2e5e8f387bcb3584b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Page not found</h1>

    ";
        // line 7
        echo "    ";
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 8
            echo "        ";
            // line 9
            echo "    ";
        }
        // line 10
        echo "
    <p>
        The requested page couldn't be located. Checkout for any URL
        misspelling or <a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\">return to the homepage</a>.
    </p>
";
        
        $__internal_62bf509ba90215210ae7820c9d768646559be6740b95df2e5e8f387bcb3584b8->leave($__internal_62bf509ba90215210ae7820c9d768646559be6740b95df2e5e8f387bcb3584b8_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 13,  52 => 10,  49 => 9,  47 => 8,  44 => 7,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <h1>Page not found</h1>*/
/* */
/*     {# example security usage, see below #}*/
/*     {% if is_granted('IS_AUTHENTICATED_FULLY') %}*/
/*         {# ... #}*/
/*     {% endif %}*/
/* */
/*     <p>*/
/*         The requested page couldn't be located. Checkout for any URL*/
/*         misspelling or <a href="{{ path('homepage') }}">return to the homepage</a>.*/
/*     </p>*/
/* {% endblock %}*/
