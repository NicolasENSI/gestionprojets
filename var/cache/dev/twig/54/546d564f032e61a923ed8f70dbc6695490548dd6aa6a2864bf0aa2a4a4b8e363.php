<?php

/* GestionProjetHomePlatformBundle::layout.html.twig */
class __TwigTemplate_3d6cd0019d7b6637bad88db8f4ff9a99c133e9052cdb25fd128e5cca58bd5fb6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a82a165295f97a540f75cc4d387d1f72281eb1f9be0d1587d238bf337fc24f98 = $this->env->getExtension("native_profiler");
        $__internal_a82a165295f97a540f75cc4d387d1f72281eb1f9be0d1587d238bf337fc24f98->enter($__internal_a82a165295f97a540f75cc4d387d1f72281eb1f9be0d1587d238bf337fc24f98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE HTML>

<html>
<head>

    <meta charset=\"utf-8\">

    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">


    <title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        echo "</title>


    ";
        // line 14
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 20
        echo "



</head>

<body>

<script>

</script>





<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-sm-3 col-md-2  sidebar\">


            ";
        // line 41
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 42
            echo "                <ul class=\"nav nav-sidebar\" id=\"navTop\">
                    <li>


                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-labelledby=\"dropdownMenu1\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                            <div id=\"headerSideBar\" class=\"dropdown\">
                                <h3>ENSICAEN <span class=\"caret\"></span></h3>
                                <h5><span class=\"glyphicon glyphicon-user\"></span>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
            echo "</h5>
                            </div>

                        </a>
                        <div id=\"extraContent\"></div>




                    </li>
                </ul>
            ";
        } else {
            // line 61
            echo "
                <div id=\"headerSideBar\" class=\"dropdown\"  >
                    <h3>ENSICAEN</h3>
                    <a id=\"connexionLink\" href=\"";
            // line 64
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\"><span class=\"glyphicon glyphicon-lock\"></span>Connexion</a>
                </div>

            ";
        }
        // line 68
        echo "

            <br>
            <br>

            <ul class=\"nav nav-sidebar\">

                <li id=\"acc\" class=\"liSidebar\"><a href=\"";
        // line 75
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_homepage");
        echo "\"><span class=\"glyphicon glyphicon-home\"></span>Accueil</a></li>
                <li id=\"mod\" class=\"liSidebar\"><a href=\"";
        // line 76
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_modalites");
        echo "\"><span class=\"glyphicon glyphicon-cog\"></span>Modalités pour proposer un projet</a></li>
                <li id=\"prop\" class=\"liSidebar\"><a href=\"";
        // line 77
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_propositionPage");
        echo "\"><span class=\"glyphicon glyphicon-plus\"></span>Proposer un projet</a></li>
                ";
        // line 78
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            // line 79
            echo "

                ";
        } else {
            // line 82
            echo "
                    <li id=\"ask\"><a href=\"";
            // line 83
            echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_askingpage");
            echo "\"><span class=\"glyphicon glyphicon-comment\"></span>Demander la création d'un compte</a></li>
                ";
        }
        // line 85
        echo "
                ";
        // line 86
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            // line 87
            echo "                            ";
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 88
                echo "                                <li id=\"separator\"> Gestion </li>
                                <li><a href=\"";
                // line 89
                echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_view_propositions");
                echo "\"><span class=\"glyphicon glyphicon-th-list\"></span> Consulter les stages proposés (";
                echo twig_escape_filter($this->env, $this->env->getExtension('home_paltform_function')->getNumberOfPending(), "html", null, true);
                echo " en attente)<span class=\"text-muted\"></span> </a></li>
                                <li><a href=\"";
                // line 90
                echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_view_account");
                echo "\"><span class=\"glyphicon glyphicon-tags\"></span> Consulter la liste des entreprises</a></li>
                                <li><a href=\"";
                // line 91
                echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_view_ask");
                echo "\"><span class=\"glyphicon glyphicon-copy\"></span> Demandes de création de compte </a></li>
                                <li><a href=\"";
                // line 92
                echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
                echo "\"><span class=\"glyphicon glyphicon-tag\"></span> Ajouter un compte</a></li>
                            ";
            }
            // line 94
            echo "                    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_view_own_propositions");
            echo "\"><span class=\"glyphicon glyphicon-list-alt\"></span> Voir mes propositions de projets</a></li>



                ";
        }
        // line 99
        echo "



            </ul>
        </div>
    </div>


</div>


<div class=\"col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main\">

";
        // line 113
        $this->displayBlock('body', $context, $blocks);
        // line 116
        echo "</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/gestionprojethomeplatform/js/jquery-2.1.4.js"), "html", null, true);
        echo "\"></script>
<script>window.jQuery || document.write('<script src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/gestionprojethomeplatform/js/jquery-2.1.4.js"), "html", null, true);
        echo "\"><\\/script>')</script>
<script src=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/gestionprojethomeplatform/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>




<script>

    if(document.location.href == \"http://127.0.0.1:8000/home/modalites\") {
        \$(\".nav\").find(\"#mod\").addClass(\"active\");
    }
    if(document.location.href == \"http://127.0.0.1:8000/home\") {
        \$(\".nav\").find(\"#acc\").addClass(\"active\");
    }
    if(document.location.href == \"http://127.0.0.1:8000/home/proposition\") {
        \$(\".nav\").find(\"#prop\").addClass(\"active\");
    }

    \$(document).on('click', '.panel-heading span.clickable', function(e){
        var \$this = \$(this);
        if(!\$this.hasClass('panel-collapsed')) {
            \$this.parents('.panel').find('.panel-body').slideDown();
            \$this.addClass('panel-collapsed');
            \$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            \$this.parents('.panel').find('.panel-body').slideUp();
            \$this.removeClass('panel-collapsed');
            \$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    })




    var champsToAdd =
        \"<ul> <li><a href='";
        // line 156
        echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
        echo "'> <span class=' glyphicon glyphicon-wrench'></span>Changer de mot de passe</a></li>\" +
    \"                       <li><a href='";
        // line 157
        echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
        echo "'><span class='glyphicon glyphicon-minus'></span>Déconnexion</a> </ul>\";
        \$('#navTop').on('click', function () {
            if(document.getElementById(\"extraContent\").innerHTML ==\"\"){
                document.getElementById(\"extraContent\").innerHTML =  champsToAdd;
            }
            else{
                document.getElementById(\"extraContent\").innerHTML =  \"\";
            }

        });


</script>


</body>


</html>
";
        
        $__internal_a82a165295f97a540f75cc4d387d1f72281eb1f9be0d1587d238bf337fc24f98->leave($__internal_a82a165295f97a540f75cc4d387d1f72281eb1f9be0d1587d238bf337fc24f98_prof);

    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        $__internal_6652ff75a321abce46aed77694d17d54cc25ae093e01dc59c9052d18bf8ac792 = $this->env->getExtension("native_profiler");
        $__internal_6652ff75a321abce46aed77694d17d54cc25ae093e01dc59c9052d18bf8ac792->enter($__internal_6652ff75a321abce46aed77694d17d54cc25ae093e01dc59c9052d18bf8ac792_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Platforme de gestion de projets";
        
        $__internal_6652ff75a321abce46aed77694d17d54cc25ae093e01dc59c9052d18bf8ac792->leave($__internal_6652ff75a321abce46aed77694d17d54cc25ae093e01dc59c9052d18bf8ac792_prof);

    }

    // line 14
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_91ebe30821322fa8ad9bb8d2d90b7912923b7483614dc3b70261150ea93d9b69 = $this->env->getExtension("native_profiler");
        $__internal_91ebe30821322fa8ad9bb8d2d90b7912923b7483614dc3b70261150ea93d9b69->enter($__internal_91ebe30821322fa8ad9bb8d2d90b7912923b7483614dc3b70261150ea93d9b69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 15
        echo "        <link rel=\"stylesheet\" href=\" ";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/gestionprojethomeplatform/css/bootstrap.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/css/layout.css"), "html", null, true);
        echo "\">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600italic,600,400italic,300,300italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700italic,700,300italic,300' rel='stylesheet' type='text/css'>
    ";
        
        $__internal_91ebe30821322fa8ad9bb8d2d90b7912923b7483614dc3b70261150ea93d9b69->leave($__internal_91ebe30821322fa8ad9bb8d2d90b7912923b7483614dc3b70261150ea93d9b69_prof);

    }

    // line 113
    public function block_body($context, array $blocks = array())
    {
        $__internal_4b52e961ce138523b9f7f28fb75676f17a3d063cf64b82a5c811b2c8ba61ef15 = $this->env->getExtension("native_profiler");
        $__internal_4b52e961ce138523b9f7f28fb75676f17a3d063cf64b82a5c811b2c8ba61ef15->enter($__internal_4b52e961ce138523b9f7f28fb75676f17a3d063cf64b82a5c811b2c8ba61ef15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 114
        echo "    </div>
";
        
        $__internal_4b52e961ce138523b9f7f28fb75676f17a3d063cf64b82a5c811b2c8ba61ef15->leave($__internal_4b52e961ce138523b9f7f28fb75676f17a3d063cf64b82a5c811b2c8ba61ef15_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  321 => 114,  315 => 113,  304 => 16,  299 => 15,  293 => 14,  281 => 11,  254 => 157,  250 => 156,  213 => 122,  209 => 121,  205 => 120,  199 => 116,  197 => 113,  181 => 99,  172 => 94,  167 => 92,  163 => 91,  159 => 90,  153 => 89,  150 => 88,  147 => 87,  145 => 86,  142 => 85,  137 => 83,  134 => 82,  129 => 79,  127 => 78,  123 => 77,  119 => 76,  115 => 75,  106 => 68,  99 => 64,  94 => 61,  79 => 49,  70 => 42,  68 => 41,  45 => 20,  43 => 14,  37 => 11,  25 => 1,);
    }
}
/* <!DOCTYPE HTML>*/
/* */
/* <html>*/
/* <head>*/
/* */
/*     <meta charset="utf-8">*/
/* */
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* */
/* */
/*     <title>{% block title %}Platforme de gestion de projets{% endblock %}</title>*/
/* */
/* */
/*     {% block stylesheets %}*/
/*         <link rel="stylesheet" href=" {{ asset('bundles/gestionprojethomeplatform/css/bootstrap.css') }}">*/
/*         <link rel="stylesheet" href="{{ asset('bundles/HomePlatformBundle/css/layout.css')}}">*/
/*         <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600italic,600,400italic,300,300italic' rel='stylesheet' type='text/css'>*/
/*         <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700italic,700,300italic,300' rel='stylesheet' type='text/css'>*/
/*     {% endblock %}*/
/* */
/* */
/* */
/* */
/* </head>*/
/* */
/* <body>*/
/* */
/* <script>*/
/* */
/* </script>*/
/* */
/* */
/* */
/* */
/* */
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-sm-3 col-md-2  sidebar">*/
/* */
/* */
/*             {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*                 <ul class="nav nav-sidebar" id="navTop">*/
/*                     <li>*/
/* */
/* */
/*                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-labelledby="dropdownMenu1" role="button" aria-haspopup="true" aria-expanded="false">*/
/*                             <div id="headerSideBar" class="dropdown">*/
/*                                 <h3>ENSICAEN <span class="caret"></span></h3>*/
/*                                 <h5><span class="glyphicon glyphicon-user"></span>{{ app.user.username }}</h5>*/
/*                             </div>*/
/* */
/*                         </a>*/
/*                         <div id="extraContent"></div>*/
/* */
/* */
/* */
/* */
/*                     </li>*/
/*                 </ul>*/
/*             {% else %}*/
/* */
/*                 <div id="headerSideBar" class="dropdown"  >*/
/*                     <h3>ENSICAEN</h3>*/
/*                     <a id="connexionLink" href="{{ path('fos_user_security_login') }}"><span class="glyphicon glyphicon-lock"></span>Connexion</a>*/
/*                 </div>*/
/* */
/*             {% endif %}*/
/* */
/* */
/*             <br>*/
/*             <br>*/
/* */
/*             <ul class="nav nav-sidebar">*/
/* */
/*                 <li id="acc" class="liSidebar"><a href="{{ path('gestion_projet_home_platform_homepage') }}"><span class="glyphicon glyphicon-home"></span>Accueil</a></li>*/
/*                 <li id="mod" class="liSidebar"><a href="{{ path('gestion_projet_home_platform_modalites') }}"><span class="glyphicon glyphicon-cog"></span>Modalités pour proposer un projet</a></li>*/
/*                 <li id="prop" class="liSidebar"><a href="{{ path('gestion_projet_home_platform_propositionPage') }}"><span class="glyphicon glyphicon-plus"></span>Proposer un projet</a></li>*/
/*                 {% if is_granted('ROLE_USER') %}*/
/* */
/* */
/*                 {% else %}*/
/* */
/*                     <li id="ask"><a href="{{ path('gestion_projet_home_platform_askingpage') }}"><span class="glyphicon glyphicon-comment"></span>Demander la création d'un compte</a></li>*/
/*                 {% endif %}*/
/* */
/*                 {% if is_granted('ROLE_USER') %}*/
/*                             {% if is_granted('ROLE_ADMIN') %}*/
/*                                 <li id="separator"> Gestion </li>*/
/*                                 <li><a href="{{ path('gestion_projet_home_platform_view_propositions') }}"><span class="glyphicon glyphicon-th-list"></span> Consulter les stages proposés ({{ getNumberOfPending() }} en attente)<span class="text-muted"></span> </a></li>*/
/*                                 <li><a href="{{ path('gestion_projet_home_platform_view_account') }}"><span class="glyphicon glyphicon-tags"></span> Consulter la liste des entreprises</a></li>*/
/*                                 <li><a href="{{ path('gestion_projet_home_platform_view_ask') }}"><span class="glyphicon glyphicon-copy"></span> Demandes de création de compte </a></li>*/
/*                                 <li><a href="{{ path('fos_user_registration_register') }}"><span class="glyphicon glyphicon-tag"></span> Ajouter un compte</a></li>*/
/*                             {% endif %}*/
/*                     <li><a href="{{ path('gestion_projet_home_platform_view_own_propositions') }}"><span class="glyphicon glyphicon-list-alt"></span> Voir mes propositions de projets</a></li>*/
/* */
/* */
/* */
/*                 {% endif %}*/
/* */
/* */
/* */
/* */
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/* */
/* */
/* </div>*/
/* */
/* */
/* <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">*/
/* */
/* {%block body%}*/
/*     </div>*/
/* {%endblock%}*/
/* </div>*/
/* <!-- Bootstrap core JavaScript*/
/* ================================================== -->*/
/* <!-- Placed at the end of the document so the pages load faster -->*/
/* <script src="{{ asset('bundles/gestionprojethomeplatform/js/jquery-2.1.4.js') }}"></script>*/
/* <script>window.jQuery || document.write('<script src="{{ asset('bundles/gestionprojethomeplatform/js/jquery-2.1.4.js') }}"><\/script>')</script>*/
/* <script src="{{ asset('bundles/gestionprojethomeplatform/js/bootstrap.min.js')}}"></script>*/
/* */
/* */
/* */
/* */
/* <script>*/
/* */
/*     if(document.location.href == "http://127.0.0.1:8000/home/modalites") {*/
/*         $(".nav").find("#mod").addClass("active");*/
/*     }*/
/*     if(document.location.href == "http://127.0.0.1:8000/home") {*/
/*         $(".nav").find("#acc").addClass("active");*/
/*     }*/
/*     if(document.location.href == "http://127.0.0.1:8000/home/proposition") {*/
/*         $(".nav").find("#prop").addClass("active");*/
/*     }*/
/* */
/*     $(document).on('click', '.panel-heading span.clickable', function(e){*/
/*         var $this = $(this);*/
/*         if(!$this.hasClass('panel-collapsed')) {*/
/*             $this.parents('.panel').find('.panel-body').slideDown();*/
/*             $this.addClass('panel-collapsed');*/
/*             $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');*/
/*         } else {*/
/*             $this.parents('.panel').find('.panel-body').slideUp();*/
/*             $this.removeClass('panel-collapsed');*/
/*             $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');*/
/*         }*/
/*     })*/
/* */
/* */
/* */
/* */
/*     var champsToAdd =*/
/*         "<ul> <li><a href='{{ path("fos_user_change_password") }}'> <span class=' glyphicon glyphicon-wrench'></span>Changer de mot de passe</a></li>" +*/
/*     "                       <li><a href='{{ path("fos_user_security_logout") }}'><span class='glyphicon glyphicon-minus'></span>Déconnexion</a> </ul>";*/
/*         $('#navTop').on('click', function () {*/
/*             if(document.getElementById("extraContent").innerHTML ==""){*/
/*                 document.getElementById("extraContent").innerHTML =  champsToAdd;*/
/*             }*/
/*             else{*/
/*                 document.getElementById("extraContent").innerHTML =  "";*/
/*             }*/
/* */
/*         });*/
/* */
/* */
/* </script>*/
/* */
/* */
/* </body>*/
/* */
/* */
/* </html>*/
/* */
