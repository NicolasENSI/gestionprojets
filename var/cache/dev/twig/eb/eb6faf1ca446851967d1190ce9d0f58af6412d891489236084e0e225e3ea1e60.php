<?php

/* GestionProjetHomePlatformBundle:Default:viewAllToPrint.html.twig */
class __TwigTemplate_08b2352aaa7cb76c05b259e352814af6b653b539b40bafa23b6e29eab1107395 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4e64ffbb7fedba97c9eed0a9f34748721dbd49c3a29b0978537bb020ffa988be = $this->env->getExtension("native_profiler");
        $__internal_4e64ffbb7fedba97c9eed0a9f34748721dbd49c3a29b0978537bb020ffa988be->enter($__internal_4e64ffbb7fedba97c9eed0a9f34748721dbd49c3a29b0978537bb020ffa988be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:viewAllToPrint.html.twig"));

        // line 1
        echo "

";
        // line 3
        $this->displayBlock('body', $context, $blocks);
        // line 22
        echo "
";
        
        $__internal_4e64ffbb7fedba97c9eed0a9f34748721dbd49c3a29b0978537bb020ffa988be->leave($__internal_4e64ffbb7fedba97c9eed0a9f34748721dbd49c3a29b0978537bb020ffa988be_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_6c243e1a1bdec10282147c282469327a1c050b430ccab3d9ec6b7633166a67dd = $this->env->getExtension("native_profiler");
        $__internal_6c243e1a1bdec10282147c282469327a1c050b430ccab3d9ec6b7633166a67dd->enter($__internal_6c243e1a1bdec10282147c282469327a1c050b430ccab3d9ec6b7633166a67dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <h1> Liste des projets validés de l'année 2016</h1>

    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listProposition"]) ? $context["listProposition"] : $this->getContext($context, "listProposition")));
        foreach ($context['_seq'] as $context["_key"] => $context["proposition"]) {
            // line 8
            echo "        <p>

        <h4 class=\"page-header\">Le projet en détail : </h4>
        <h5> Intitulé : ";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "intituleProjet", array()), "html", null, true);
            echo "</h5>
        <h5> Type de projet : ";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "mission", array()), "html", null, true);
            echo "</h5>
        <h5> Entreprise : ";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "entreprise", array()), "html", null, true);
            echo "</h5>
        <h5> Nom : ";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "nom", array()), "html", null, true);
            echo " </h5>
        <h5> Prenom : ";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "prenom", array()), "html", null, true);
            echo " </h5>
        <h5> Resumé : </h5>
        <h6>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "description", array()), "html", null, true);
            echo "</h6>
</p>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['proposition'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "
";
        
        $__internal_6c243e1a1bdec10282147c282469327a1c050b430ccab3d9ec6b7633166a67dd->leave($__internal_6c243e1a1bdec10282147c282469327a1c050b430ccab3d9ec6b7633166a67dd_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewAllToPrint.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  87 => 20,  78 => 17,  73 => 15,  69 => 14,  65 => 13,  61 => 12,  57 => 11,  52 => 8,  48 => 7,  43 => 4,  37 => 3,  29 => 22,  27 => 3,  23 => 1,);
    }
}
/* */
/* */
/* {% block body %}*/
/* */
/*     <h1> Liste des projets validés de l'année 2016</h1>*/
/* */
/*     {% for proposition in listProposition %}*/
/*         <p>*/
/* */
/*         <h4 class="page-header">Le projet en détail : </h4>*/
/*         <h5> Intitulé : {{ proposition.intituleProjet }}</h5>*/
/*         <h5> Type de projet : {{ proposition.mission }}</h5>*/
/*         <h5> Entreprise : {{ proposition.entreprise }}</h5>*/
/*         <h5> Nom : {{ proposition.nom }} </h5>*/
/*         <h5> Prenom : {{ proposition.prenom }} </h5>*/
/*         <h5> Resumé : </h5>*/
/*         <h6>{{ proposition.description }}</h6>*/
/* </p>*/
/* {% endfor %}*/
/* */
/* {% endblock %}*/
/* */
/* */
