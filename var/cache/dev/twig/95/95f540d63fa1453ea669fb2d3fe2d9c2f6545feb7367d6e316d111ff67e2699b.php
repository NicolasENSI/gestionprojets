<?php

/* base.html.twig */
class __TwigTemplate_084063b6cfe3d47bfd4c68435cc8dd99e5adc83983626e85ef0b48a7bfec478d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_382c1f82ba20f8f5b93b1adf262f93ed3fcd7416b6bdd0df87d3b869114f730b = $this->env->getExtension("native_profiler");
        $__internal_382c1f82ba20f8f5b93b1adf262f93ed3fcd7416b6bdd0df87d3b869114f730b->enter($__internal_382c1f82ba20f8f5b93b1adf262f93ed3fcd7416b6bdd0df87d3b869114f730b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
   <!-- HTML meta refresh URL redirection -->
   <meta http-equiv=\"refresh\"
   content=\"0; url=";
        // line 6
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_homepage");
        echo "\">
</head>
<body>
</body>
</html>
";
        
        $__internal_382c1f82ba20f8f5b93b1adf262f93ed3fcd7416b6bdd0df87d3b869114f730b->leave($__internal_382c1f82ba20f8f5b93b1adf262f93ed3fcd7416b6bdd0df87d3b869114f730b_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 6,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/*    <!-- HTML meta refresh URL redirection -->*/
/*    <meta http-equiv="refresh"*/
/*    content="0; url={{ path('gestion_projet_home_platform_homepage') }}">*/
/* </head>*/
/* <body>*/
/* </body>*/
/* </html>*/
/* */
