<?php

/* FOSUserBundle:ChangePassword:changePassword.html.twig */
class __TwigTemplate_10de577b80c22b85818f927c4c8169f9bc2cd48e694a38fbdc9effcba3bba2a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:ChangePassword:changePassword.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ce3f78a7bc712be95e93ef5c262d61bda06421b4fc55b26200abc4f22224f7d = $this->env->getExtension("native_profiler");
        $__internal_0ce3f78a7bc712be95e93ef5c262d61bda06421b4fc55b26200abc4f22224f7d->enter($__internal_0ce3f78a7bc712be95e93ef5c262d61bda06421b4fc55b26200abc4f22224f7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:changePassword.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0ce3f78a7bc712be95e93ef5c262d61bda06421b4fc55b26200abc4f22224f7d->leave($__internal_0ce3f78a7bc712be95e93ef5c262d61bda06421b4fc55b26200abc4f22224f7d_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f2e2e8776d69d1575ca1351eced8ae2e2a6751550e68204bb63aec509ae4d0ca = $this->env->getExtension("native_profiler");
        $__internal_f2e2e8776d69d1575ca1351eced8ae2e2a6751550e68204bb63aec509ae4d0ca->enter($__internal_f2e2e8776d69d1575ca1351eced8ae2e2a6751550e68204bb63aec509ae4d0ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:ChangePassword:changePassword_content.html.twig", "FOSUserBundle:ChangePassword:changePassword.html.twig", 4)->display($context);
        
        $__internal_f2e2e8776d69d1575ca1351eced8ae2e2a6751550e68204bb63aec509ae4d0ca->leave($__internal_f2e2e8776d69d1575ca1351eced8ae2e2a6751550e68204bb63aec509ae4d0ca_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:changePassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:ChangePassword:changePassword_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
