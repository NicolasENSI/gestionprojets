<?php

/* FOSUserBundle:Resetting:checkEmail.html.twig */
class __TwigTemplate_f2cbf3085d35af900d66c746bf70ba2595e727f49931088707d3546f2fa6b1d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:checkEmail.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0dfca87d2d501ccce219a84820249aa9ee1f245de5add9fb0e9f558a5b5cc726 = $this->env->getExtension("native_profiler");
        $__internal_0dfca87d2d501ccce219a84820249aa9ee1f245de5add9fb0e9f558a5b5cc726->enter($__internal_0dfca87d2d501ccce219a84820249aa9ee1f245de5add9fb0e9f558a5b5cc726_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0dfca87d2d501ccce219a84820249aa9ee1f245de5add9fb0e9f558a5b5cc726->leave($__internal_0dfca87d2d501ccce219a84820249aa9ee1f245de5add9fb0e9f558a5b5cc726_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_8e2aae826c2b34d17491315ff13c08f4f5d40ed4af775eb7ac4b71f86afc5c2c = $this->env->getExtension("native_profiler");
        $__internal_8e2aae826c2b34d17491315ff13c08f4f5d40ed4af775eb7ac4b71f86afc5c2c->enter($__internal_8e2aae826c2b34d17491315ff13c08f4f5d40ed4af775eb7ac4b71f86afc5c2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.check_email", array("%email%" => (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email"))), "FOSUserBundle"), "html", null, true);
        echo "
</p>
";
        
        $__internal_8e2aae826c2b34d17491315ff13c08f4f5d40ed4af775eb7ac4b71f86afc5c2c->leave($__internal_8e2aae826c2b34d17491315ff13c08f4f5d40ed4af775eb7ac4b71f86afc5c2c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* <p>*/
/* {{ 'resetting.check_email'|trans({'%email%': email}) }}*/
/* </p>*/
/* {% endblock %}*/
/* */
