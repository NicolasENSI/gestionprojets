<?php

/* @GestionProjetHomePlatform/Emails/refus.html.twig */
class __TwigTemplate_bad2851805f43f760a6dcf2eab55d1354614cdf1334d587e5b1216158e05544f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_00032047173f39858f7b687720ee99ce35e98702b73254ccb2cb1f0f0c44961c = $this->env->getExtension("native_profiler");
        $__internal_00032047173f39858f7b687720ee99ce35e98702b73254ccb2cb1f0f0c44961c->enter($__internal_00032047173f39858f7b687720ee99ce35e98702b73254ccb2cb1f0f0c44961c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@GestionProjetHomePlatform/Emails/refus.html.twig"));

        // line 2
        echo "<h3>Monsieur/Madame ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo ", nous aovns le regret de vous annoncer que votre projet \"";
        echo twig_escape_filter($this->env, (isset($context["titre"]) ? $context["titre"] : $this->getContext($context, "titre")), "html", null, true);
        echo "\" n'a pas été retenu </h3>

";
        // line 5
        echo "
<h4>Vous pouvez aller voir l'état de vos autres projets <a href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_view_own_propositions");
        echo "\"> ici </a> </h4>

<h4>Vous pouvez également proposer d'autres projet <a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_propositionPage");
        echo "\"> ici </a> </h4>";
        
        $__internal_00032047173f39858f7b687720ee99ce35e98702b73254ccb2cb1f0f0c44961c->leave($__internal_00032047173f39858f7b687720ee99ce35e98702b73254ccb2cb1f0f0c44961c_prof);

    }

    public function getTemplateName()
    {
        return "@GestionProjetHomePlatform/Emails/refus.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 8,  33 => 6,  30 => 5,  22 => 2,);
    }
}
/* {# app/Resources/views/Emails/registration.html.twig #}*/
/* <h3>Monsieur/Madame {{ name }}, nous aovns le regret de vous annoncer que votre projet "{{ titre }}" n'a pas été retenu </h3>*/
/* */
/* {# example, assuming you have a route named "login" #}*/
/* */
/* <h4>Vous pouvez aller voir l'état de vos autres projets <a href="{{ path('gestion_projet_home_platform_view_own_propositions') }}"> ici </a> </h4>*/
/* */
/* <h4>Vous pouvez également proposer d'autres projet <a href="{{ path('gestion_projet_home_platform_propositionPage') }}"> ici </a> </h4>*/
