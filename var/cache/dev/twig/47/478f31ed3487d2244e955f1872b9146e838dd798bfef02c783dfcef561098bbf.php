<?php

/* FOSUserBundle:Registration:register_content.html.twig */
class __TwigTemplate_390ebdb8b7e21f8aa12ad6687dca7f623747d6975aff08b2856b8281067cf662 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a6e1387bc4ddae34055f7f83ea93517f70d185553fa3b9ac2c0ee603842aada2 = $this->env->getExtension("native_profiler");
        $__internal_a6e1387bc4ddae34055f7f83ea93517f70d185553fa3b9ac2c0ee603842aada2->enter($__internal_a6e1387bc4ddae34055f7f83ea93517f70d185553fa3b9ac2c0ee603842aada2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register_content.html.twig"));

        // line 2
        echo "
<div class=\"container col-md-offset-3\">

    <h1 class=\"col-md-offset-2\">Ajouter un compte utilisateur </h1></br>

";
        // line 7
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('routing')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register"), "attr" => array("class" => "form-group")));
        echo "
    ";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 12
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    </div>";
        
        $__internal_a6e1387bc4ddae34055f7f83ea93517f70d185553fa3b9ac2c0ee603842aada2->leave($__internal_a6e1387bc4ddae34055f7f83ea93517f70d185553fa3b9ac2c0ee603842aada2_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 12,  38 => 10,  33 => 8,  29 => 7,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* <div class="container col-md-offset-3">*/
/* */
/*     <h1 class="col-md-offset-2">Ajouter un compte utilisateur </h1></br>*/
/* */
/* {{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'},'attr': {'class': 'form-group'}}) }}*/
/*     {{ form_widget(form) }}*/
/*     <div>*/
/*         <input type="submit" value="{{ 'registration.submit'|trans }}" />*/
/*     </div>*/
/* {{ form_end(form) }}*/
/* */
/*     </div>*/
