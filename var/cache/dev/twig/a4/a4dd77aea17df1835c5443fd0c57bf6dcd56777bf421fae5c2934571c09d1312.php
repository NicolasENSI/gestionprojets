<?php

/* GestionProjetHomePlatformBundle:Default:viewPendingPropositions.html.twig */
class __TwigTemplate_81df09b4011d0c244bd078c85e56ece9f58a61034b61205406f374caa8186b45 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewPendingPropositions.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28986a0dd87484f3d825e6d499bfc51e8fd3548e1b5a1fe34b4df9ddf99ad2fa = $this->env->getExtension("native_profiler");
        $__internal_28986a0dd87484f3d825e6d499bfc51e8fd3548e1b5a1fe34b4df9ddf99ad2fa->enter($__internal_28986a0dd87484f3d825e6d499bfc51e8fd3548e1b5a1fe34b4df9ddf99ad2fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:viewPendingPropositions.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_28986a0dd87484f3d825e6d499bfc51e8fd3548e1b5a1fe34b4df9ddf99ad2fa->leave($__internal_28986a0dd87484f3d825e6d499bfc51e8fd3548e1b5a1fe34b4df9ddf99ad2fa_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_fb5f1fa8cf4df23e2dc49f889c9908c1fd7ddc7f595be6c5236110d8339113a8 = $this->env->getExtension("native_profiler");
        $__internal_fb5f1fa8cf4df23e2dc49f889c9908c1fd7ddc7f595be6c5236110d8339113a8->enter($__internal_fb5f1fa8cf4df23e2dc49f889c9908c1fd7ddc7f595be6c5236110d8339113a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
        
        $__internal_fb5f1fa8cf4df23e2dc49f889c9908c1fd7ddc7f595be6c5236110d8339113a8->leave($__internal_fb5f1fa8cf4df23e2dc49f889c9908c1fd7ddc7f595be6c5236110d8339113a8_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_181f507cd5a70e99352cf344b66b313d1b1752b01335986bfd65e67c91bb96aa = $this->env->getExtension("native_profiler");
        $__internal_181f507cd5a70e99352cf344b66b313d1b1752b01335986bfd65e67c91bb96aa->enter($__internal_181f507cd5a70e99352cf344b66b313d1b1752b01335986bfd65e67c91bb96aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "




    <h1 class=\"page-header\">Tableau de bord</h1>

    ";
        // line 14
        try {
            $this->loadTemplate("GestionProjetHomePlatformBundle:Default:leftBoard.html.twig", "GestionProjetHomePlatformBundle:Default:viewPendingPropositions.html.twig", 14)->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 15
        echo "

    <div class=\"table-responsive\">
        <h2>Liste de projets</h2>
        ";
        // line 19
        try {
            $this->loadTemplate("GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig", "GestionProjetHomePlatformBundle:Default:viewPendingPropositions.html.twig", 19)->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 20
        echo "    </div>

";
        
        $__internal_181f507cd5a70e99352cf344b66b313d1b1752b01335986bfd65e67c91bb96aa->leave($__internal_181f507cd5a70e99352cf344b66b313d1b1752b01335986bfd65e67c91bb96aa_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewPendingPropositions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 20,  76 => 19,  70 => 15,  63 => 14,  54 => 7,  48 => 6,  35 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* */
/* */
/* */
/* */
/*     <h1 class="page-header">Tableau de bord</h1>*/
/* */
/*     {% include 'GestionProjetHomePlatformBundle:Default:leftBoard.html.twig' ignore missing %}*/
/* */
/* */
/*     <div class="table-responsive">*/
/*         <h2>Liste de projets</h2>*/
/*         {% include 'GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig' ignore missing %}*/
/*     </div>*/
/* */
/* {% endblock %}*/
