<?php

/* GestionProjetHomePlatformBundle:Default:viewAccount.html.twig */
class __TwigTemplate_c21015670a68435a02d8da9e9a22783b397c562697fd1539cc6647abfc21655f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewAccount.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b5cfc9591bfe2e68d6567a7ed2c6ebc9a8adafb6028a1b58de32920c2850278 = $this->env->getExtension("native_profiler");
        $__internal_7b5cfc9591bfe2e68d6567a7ed2c6ebc9a8adafb6028a1b58de32920c2850278->enter($__internal_7b5cfc9591bfe2e68d6567a7ed2c6ebc9a8adafb6028a1b58de32920c2850278_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:viewAccount.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7b5cfc9591bfe2e68d6567a7ed2c6ebc9a8adafb6028a1b58de32920c2850278->leave($__internal_7b5cfc9591bfe2e68d6567a7ed2c6ebc9a8adafb6028a1b58de32920c2850278_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_64fa06efe4e0bea61cb8f418b1a06cb7e255cc7f7de22e25faae732d0885c74d = $this->env->getExtension("native_profiler");
        $__internal_64fa06efe4e0bea61cb8f418b1a06cb7e255cc7f7de22e25faae732d0885c74d->enter($__internal_64fa06efe4e0bea61cb8f418b1a06cb7e255cc7f7de22e25faae732d0885c74d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
        
        $__internal_64fa06efe4e0bea61cb8f418b1a06cb7e255cc7f7de22e25faae732d0885c74d->leave($__internal_64fa06efe4e0bea61cb8f418b1a06cb7e255cc7f7de22e25faae732d0885c74d_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_896b50c15426fe9e3e89578239aa96eee0a6689c6064e38ead7ddee8ff6743dc = $this->env->getExtension("native_profiler");
        $__internal_896b50c15426fe9e3e89578239aa96eee0a6689c6064e38ead7ddee8ff6743dc->enter($__internal_896b50c15426fe9e3e89578239aa96eee0a6689c6064e38ead7ddee8ff6743dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "
<div class=\"table-responsive\">
    <h2>Liste de compte entreprise</h2>
    <table class=\"table table-striped\">

        <tr>
            <th> ID </th>
            <th> Login </th>
            <th> Email </th>
            <th> Confirmé</th>
            <th> ToolBox</th>
        </tr>


        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listAccount"]) ? $context["listAccount"] : $this->getContext($context, "listAccount")));
        foreach ($context['_seq'] as $context["_key"] => $context["account"]) {
            // line 22
            echo "            <tr>
                <td> ";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["account"], "id", array()), "html", null, true);
            echo " </td>
                <td> ";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["account"], "username", array()), "html", null, true);
            echo " </td>
                <td> ";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["account"], "email", array()), "html", null, true);
            echo " </td>
                <td>
                    ";
            // line 27
            if ($this->getAttribute($context["account"], "enabled", array())) {
                // line 28
                echo "                        <img src=\"http://www.webasun.com/wp-content/themes/webasun-theme-2014/images/coche-verte.png\" alt=\"Validé\" height=\"32\" width=\"32\">
                    ";
            } else {
                // line 30
                echo "                        <img src=\"http://iconizer.net/files/Farm-fresh/orig/hourglass.png\" alt=\"En attente\" height=\"32\" width=\"32\">
                    ";
            }
            // line 32
            echo "

                </td>
                <td> supprimer / modifier / envoyer mail</td>


            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['account'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "
    </table>
</div>
";
        
        $__internal_896b50c15426fe9e3e89578239aa96eee0a6689c6064e38ead7ddee8ff6743dc->leave($__internal_896b50c15426fe9e3e89578239aa96eee0a6689c6064e38ead7ddee8ff6743dc_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewAccount.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 40,  100 => 32,  96 => 30,  92 => 28,  90 => 27,  85 => 25,  81 => 24,  77 => 23,  74 => 22,  70 => 21,  54 => 7,  48 => 6,  35 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* <div class="table-responsive">*/
/*     <h2>Liste de compte entreprise</h2>*/
/*     <table class="table table-striped">*/
/* */
/*         <tr>*/
/*             <th> ID </th>*/
/*             <th> Login </th>*/
/*             <th> Email </th>*/
/*             <th> Confirmé</th>*/
/*             <th> ToolBox</th>*/
/*         </tr>*/
/* */
/* */
/*         {% for account in listAccount %}*/
/*             <tr>*/
/*                 <td> {{ account.id }} </td>*/
/*                 <td> {{ account.username }} </td>*/
/*                 <td> {{ account.email }} </td>*/
/*                 <td>*/
/*                     {% if(account.enabled) %}*/
/*                         <img src="http://www.webasun.com/wp-content/themes/webasun-theme-2014/images/coche-verte.png" alt="Validé" height="32" width="32">*/
/*                     {% else %}*/
/*                         <img src="http://iconizer.net/files/Farm-fresh/orig/hourglass.png" alt="En attente" height="32" width="32">*/
/*                     {% endif %}*/
/* */
/* */
/*                 </td>*/
/*                 <td> supprimer / modifier / envoyer mail</td>*/
/* */
/* */
/*             </tr>*/
/*         {% endfor %}*/
/* */
/*     </table>*/
/* </div>*/
/* {% endblock %}*/
/* */
/* */
/* */
