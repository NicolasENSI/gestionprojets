<?php

/* GestionProjetHomePlatformBundle:Default:index.html.twig */
class __TwigTemplate_69a51f4d929474d7e1eee0e45443c7848bbfae6b4f55bcca3ec79b98ac59f55e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_57658facc7179d7d86d40ddfbc1a85e8d6a4df3726e31a5eeff59195a04cd7ee = $this->env->getExtension("native_profiler");
        $__internal_57658facc7179d7d86d40ddfbc1a85e8d6a4df3726e31a5eeff59195a04cd7ee->enter($__internal_57658facc7179d7d86d40ddfbc1a85e8d6a4df3726e31a5eeff59195a04cd7ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_57658facc7179d7d86d40ddfbc1a85e8d6a4df3726e31a5eeff59195a04cd7ee->leave($__internal_57658facc7179d7d86d40ddfbc1a85e8d6a4df3726e31a5eeff59195a04cd7ee_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_b604fe0b994e5665a5feadd5d90386e44505bab43f417828531351bb05830673 = $this->env->getExtension("native_profiler");
        $__internal_b604fe0b994e5665a5feadd5d90386e44505bab43f417828531351bb05830673->enter($__internal_b604fe0b994e5665a5feadd5d90386e44505bab43f417828531351bb05830673_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
        
        $__internal_b604fe0b994e5665a5feadd5d90386e44505bab43f417828531351bb05830673->leave($__internal_b604fe0b994e5665a5feadd5d90386e44505bab43f417828531351bb05830673_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_e1ea5b785576a827d7df8ac343c7a3932d8f83025032acd064de6fb04394e0ea = $this->env->getExtension("native_profiler");
        $__internal_e1ea5b785576a827d7df8ac343c7a3932d8f83025032acd064de6fb04394e0ea->enter($__internal_e1ea5b785576a827d7df8ac343c7a3932d8f83025032acd064de6fb04394e0ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "
  <div class=\"container  col-md-12 col-lg-12\">

      <div class=\"starter-template\">
          <img id=logoensi\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/images/logoensi.jpg"), "html", null, true);
        echo "\" align=\"center\" height=\"100\" >
        <h1>Proposer un projet à l'ENSICAEN en <span style=\"background-color: #2b669a; color: white\"> 3  étapes </span></h1>


      </div>

      <hr class=\"featurette-divider\">

      <div class=\"row featurette\">
          <div class=\"col-md-7\">
              <h2 class=\"featurette-heading\"> <span class=\"glyphicon glyphicon-cog\"></span> Création de compte. <span class=\"text-muted\"> Remplissez <a class=\"homelink\" href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_askingpage");
        echo "\">ce formulaire</a></span></h2>
              <p class=\"lead\">Avant de pouvoir proposer un sujet de projet, il vous faut un compte, remplissez le formulaire et attendez de recevoir un mail de confirmation avec vos idenifiants.</p>
          </div>
          <div class=\"col-md-5\">
              <a href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_askingpage");
        echo "\"><img class=\"featurette-image img-responsive center-block\"  alt=\"Generic placeholder image\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/images/one.png"), "html", null, true);
        echo "\" height='200' width=\"200\">
          </a>
          </div>
      </div>

      <hr class=\"featurette-divider\">

      <div class=\"row featurette\">
          <div class=\"col-md-7 col-md-push-5\">
              <h2 class=\"featurette-heading\"> <span class=\"glyphicon glyphicon-lock\"></span> Connectez vous ! <span class=\"text-muted\"> <a class=\"homelink\" href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
        echo "\"> Page de connexion</a></span></h2>
              <p class=\"lead\"> A partir de maintenant vous allez pouvoir faire des propositions de projet.</p>
          </div>
          <div class=\"col-md-5 col-md-pull-7\">
              <img class=\"featurette-image img-responsive center-block\" data-src=\"holder.js/500x500/auto\" alt=\"Generic placeholder image\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/images/two.png"), "html", null, true);
        echo "\" height='200' width=\"200\">
          </div>
      </div>

      <hr class=\"featurette-divider\">

      <div class=\"row featurette\">
          <div class=\"col-md-7\">
              <h2 class=\"featurette-heading\"> <span class=\"glyphicon glyphicon-th-list\"></span>Remplissez les formulaires de propositions.<span class=\"text-muted\"> Et voilà !</span></h2>
              <p class=\"lead\">Remplissez <a href=\"";
        // line 47
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_propositionPage");
        echo "\" class=\"homelink\"  > le formulaire</a> et vous serez notifié quand la proposition de projet aura été acceptée.</p>
          </div>
          <div class=\"col-md-5\">
              <img class=\"featurette-image img-responsive center-block\" data-src=\"holder.js/500x500/auto\" alt=\"Generic placeholder image\" src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/images/three.png"), "html", null, true);
        echo "\" height='200' width=\"200\">
          </div>
      </div>

    </div>

";
        
        $__internal_e1ea5b785576a827d7df8ac343c7a3932d8f83025032acd064de6fb04394e0ea->leave($__internal_e1ea5b785576a827d7df8ac343c7a3932d8f83025032acd064de6fb04394e0ea_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 50,  113 => 47,  101 => 38,  94 => 34,  80 => 25,  73 => 21,  60 => 11,  54 => 7,  48 => 6,  35 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/*   <div class="container  col-md-12 col-lg-12">*/
/* */
/*       <div class="starter-template">*/
/*           <img id=logoensi" src="{{ asset('bundles/HomePlatformBundle/images/logoensi.jpg') }}" align="center" height="100" >*/
/*         <h1>Proposer un projet à l'ENSICAEN en <span style="background-color: #2b669a; color: white"> 3  étapes </span></h1>*/
/* */
/* */
/*       </div>*/
/* */
/*       <hr class="featurette-divider">*/
/* */
/*       <div class="row featurette">*/
/*           <div class="col-md-7">*/
/*               <h2 class="featurette-heading"> <span class="glyphicon glyphicon-cog"></span> Création de compte. <span class="text-muted"> Remplissez <a class="homelink" href="{{ path('gestion_projet_home_platform_askingpage') }}">ce formulaire</a></span></h2>*/
/*               <p class="lead">Avant de pouvoir proposer un sujet de projet, il vous faut un compte, remplissez le formulaire et attendez de recevoir un mail de confirmation avec vos idenifiants.</p>*/
/*           </div>*/
/*           <div class="col-md-5">*/
/*               <a href="{{ path('gestion_projet_home_platform_askingpage') }}"><img class="featurette-image img-responsive center-block"  alt="Generic placeholder image" src="{{ asset('bundles/HomePlatformBundle/images/one.png') }}" height='200' width="200">*/
/*           </a>*/
/*           </div>*/
/*       </div>*/
/* */
/*       <hr class="featurette-divider">*/
/* */
/*       <div class="row featurette">*/
/*           <div class="col-md-7 col-md-push-5">*/
/*               <h2 class="featurette-heading"> <span class="glyphicon glyphicon-lock"></span> Connectez vous ! <span class="text-muted"> <a class="homelink" href="{{ path('fos_user_security_login') }}"> Page de connexion</a></span></h2>*/
/*               <p class="lead"> A partir de maintenant vous allez pouvoir faire des propositions de projet.</p>*/
/*           </div>*/
/*           <div class="col-md-5 col-md-pull-7">*/
/*               <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="{{ asset('bundles/HomePlatformBundle/images/two.png') }}" height='200' width="200">*/
/*           </div>*/
/*       </div>*/
/* */
/*       <hr class="featurette-divider">*/
/* */
/*       <div class="row featurette">*/
/*           <div class="col-md-7">*/
/*               <h2 class="featurette-heading"> <span class="glyphicon glyphicon-th-list"></span>Remplissez les formulaires de propositions.<span class="text-muted"> Et voilà !</span></h2>*/
/*               <p class="lead">Remplissez <a href="{{ path('gestion_projet_home_platform_propositionPage') }}" class="homelink"  > le formulaire</a> et vous serez notifié quand la proposition de projet aura été acceptée.</p>*/
/*           </div>*/
/*           <div class="col-md-5">*/
/*               <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="{{ asset('bundles/HomePlatformBundle/images/three.png') }}" height='200' width="200">*/
/*           </div>*/
/*       </div>*/
/* */
/*     </div>*/
/* */
/* {% endblock %}*/
/* */
/* */
/* */
