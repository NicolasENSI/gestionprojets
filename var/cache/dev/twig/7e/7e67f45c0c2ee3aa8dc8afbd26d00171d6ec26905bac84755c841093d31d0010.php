<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_d88a73c08ebfcee2172f1c92209d279b7cfa820dbd8980a896f545dbf3e197a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_70fc50fc61dee9f44d5503198976834f892fa933cddd8b532e6728ccfd0f3472 = $this->env->getExtension("native_profiler");
        $__internal_70fc50fc61dee9f44d5503198976834f892fa933cddd8b532e6728ccfd0f3472->enter($__internal_70fc50fc61dee9f44d5503198976834f892fa933cddd8b532e6728ccfd0f3472_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_70fc50fc61dee9f44d5503198976834f892fa933cddd8b532e6728ccfd0f3472->leave($__internal_70fc50fc61dee9f44d5503198976834f892fa933cddd8b532e6728ccfd0f3472_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_bd098d98a3dbe3009f0ea028b73c6320d24e8832cab3bf59712935081abdffa2 = $this->env->getExtension("native_profiler");
        $__internal_bd098d98a3dbe3009f0ea028b73c6320d24e8832cab3bf59712935081abdffa2->enter($__internal_bd098d98a3dbe3009f0ea028b73c6320d24e8832cab3bf59712935081abdffa2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Registration:register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_bd098d98a3dbe3009f0ea028b73c6320d24e8832cab3bf59712935081abdffa2->leave($__internal_bd098d98a3dbe3009f0ea028b73c6320d24e8832cab3bf59712935081abdffa2_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Registration:register_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
