<?php

/* GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig */
class __TwigTemplate_5a0708551c8208e47bfc99752823fc9071e25005307a524d6b6f6c87fe2a6b8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'hearderproposition' => array($this, 'block_hearderproposition'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ae4298bee68005c85287e1770ec611285a3f6950bc9012bb45494dac5066755 = $this->env->getExtension("native_profiler");
        $__internal_8ae4298bee68005c85287e1770ec611285a3f6950bc9012bb45494dac5066755->enter($__internal_8ae4298bee68005c85287e1770ec611285a3f6950bc9012bb45494dac5066755_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig"));

        // line 1
        $this->displayBlock('hearderproposition', $context, $blocks);
        
        $__internal_8ae4298bee68005c85287e1770ec611285a3f6950bc9012bb45494dac5066755->leave($__internal_8ae4298bee68005c85287e1770ec611285a3f6950bc9012bb45494dac5066755_prof);

    }

    public function block_hearderproposition($context, array $blocks = array())
    {
        $__internal_e046e12e4253e36719f63208b5fe712655e1960d6f02362e0d2073acfd150388 = $this->env->getExtension("native_profiler");
        $__internal_e046e12e4253e36719f63208b5fe712655e1960d6f02362e0d2073acfd150388->enter($__internal_e046e12e4253e36719f63208b5fe712655e1960d6f02362e0d2073acfd150388_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hearderproposition"));

        // line 2
        echo "    <table class=\"table table-striped\">

        <tr>
            <th> Entreprise</th>
            <th> Intitulé du projet</th>
            <th> Proposé le</th>
            <th> Description</th>
            <th> Status</th>
            <th> Détails</th>
        </tr>


        ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listProposition"]) ? $context["listProposition"] : $this->getContext($context, "listProposition")));
        foreach ($context['_seq'] as $context["_key"] => $context["proposition"]) {
            // line 15
            echo "            <tr>
                <td> ";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "entreprise", array()), "html", null, true);
            echo " </td>
                <td> ";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "intituleProjet", array()), "html", null, true);
            echo " </td>
                <td> ";
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["proposition"], "insertDate", array()), "m/d/Y"), "html", null, true);
            echo "</td>
                <td> ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "description", array()), "html", null, true);
            echo " </td>
                ";
            // line 20
            if (($this->getAttribute($context["proposition"], "valide", array()) == 0)) {
                // line 21
                echo "                    <td>
                        <button type=\"button\" class=\"btn btn-info\">En attente</button>
                    </td>
                ";
            }
            // line 25
            echo "                ";
            if (($this->getAttribute($context["proposition"], "valide", array()) == 1)) {
                // line 26
                echo "                    <td>
                        <button type=\"button\" class=\"btn btn-success\">Validé</button>
                    </td>
                ";
            }
            // line 30
            echo "
                ";
            // line 31
            if (($this->getAttribute($context["proposition"], "valide", array()) == 2)) {
                // line 32
                echo "                    <td>
                        <button type=\"button\" class=\"btn btn-danger\">Refusé</button>
                    </td>
                ";
            }
            // line 36
            echo "                <td><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_propostion_in_detail", array("id" => $this->getAttribute($context["proposition"], "id", array()))), "html", null, true);
            echo "\">
                        <button type=\"button\" class=\"btn btn-default\">Voir détails &raquo;</button>
                    </a></td>


            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['proposition'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "    </table>
";
        
        $__internal_e046e12e4253e36719f63208b5fe712655e1960d6f02362e0d2073acfd150388->leave($__internal_e046e12e4253e36719f63208b5fe712655e1960d6f02362e0d2073acfd150388_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  114 => 43,  100 => 36,  94 => 32,  92 => 31,  89 => 30,  83 => 26,  80 => 25,  74 => 21,  72 => 20,  68 => 19,  64 => 18,  60 => 17,  56 => 16,  53 => 15,  49 => 14,  35 => 2,  23 => 1,);
    }
}
/* {% block hearderproposition %}*/
/*     <table class="table table-striped">*/
/* */
/*         <tr>*/
/*             <th> Entreprise</th>*/
/*             <th> Intitulé du projet</th>*/
/*             <th> Proposé le</th>*/
/*             <th> Description</th>*/
/*             <th> Status</th>*/
/*             <th> Détails</th>*/
/*         </tr>*/
/* */
/* */
/*         {% for proposition in listProposition %}*/
/*             <tr>*/
/*                 <td> {{ proposition.entreprise }} </td>*/
/*                 <td> {{ proposition.intituleProjet }} </td>*/
/*                 <td> {{ proposition.insertDate|date("m/d/Y") }}</td>*/
/*                 <td> {{ proposition.description }} </td>*/
/*                 {% if  proposition.valide  == 0 %}*/
/*                     <td>*/
/*                         <button type="button" class="btn btn-info">En attente</button>*/
/*                     </td>*/
/*                 {% endif %}*/
/*                 {% if proposition.valide  == 1 %}*/
/*                     <td>*/
/*                         <button type="button" class="btn btn-success">Validé</button>*/
/*                     </td>*/
/*                 {% endif %}*/
/* */
/*                 {% if proposition.valide  == 2 %}*/
/*                     <td>*/
/*                         <button type="button" class="btn btn-danger">Refusé</button>*/
/*                     </td>*/
/*                 {% endif %}*/
/*                 <td><a href="{{ path('gestion_projet_home_platform_propostion_in_detail', {'id': proposition.id}) }}">*/
/*                         <button type="button" class="btn btn-default">Voir détails &raquo;</button>*/
/*                     </a></td>*/
/* */
/* */
/*             </tr>*/
/*         {% endfor %}*/
/*     </table>*/
/* {% endblock %}*/
