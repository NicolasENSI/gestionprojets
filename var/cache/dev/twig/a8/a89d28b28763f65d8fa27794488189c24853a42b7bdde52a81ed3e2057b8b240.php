<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_28f7525ca5d806d66cb85b01bdd98b6fe284b6f217bb3e11197bd100b1c66bd5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2451608d754600fe4cf35e84fe678c67b9609cac3c344e511420bcc68ec55efb = $this->env->getExtension("native_profiler");
        $__internal_2451608d754600fe4cf35e84fe678c67b9609cac3c344e511420bcc68ec55efb->enter($__internal_2451608d754600fe4cf35e84fe678c67b9609cac3c344e511420bcc68ec55efb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_2451608d754600fe4cf35e84fe678c67b9609cac3c344e511420bcc68ec55efb->leave($__internal_2451608d754600fe4cf35e84fe678c67b9609cac3c344e511420bcc68ec55efb_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_382628565c889adff290aa3de7ac4f6f236fa9afa4bca559c57a0a34ef6cd497 = $this->env->getExtension("native_profiler");
        $__internal_382628565c889adff290aa3de7ac4f6f236fa9afa4bca559c57a0a34ef6cd497->enter($__internal_382628565c889adff290aa3de7ac4f6f236fa9afa4bca559c57a0a34ef6cd497_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        echo "
";
        
        $__internal_382628565c889adff290aa3de7ac4f6f236fa9afa4bca559c57a0a34ef6cd497->leave($__internal_382628565c889adff290aa3de7ac4f6f236fa9afa4bca559c57a0a34ef6cd497_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_99c4e563ed4c5d80d77ad06845bd33fa99e6b6402698aeaaf5c17e9baf7e8076 = $this->env->getExtension("native_profiler");
        $__internal_99c4e563ed4c5d80d77ad06845bd33fa99e6b6402698aeaaf5c17e9baf7e8076->enter($__internal_99c4e563ed4c5d80d77ad06845bd33fa99e6b6402698aeaaf5c17e9baf7e8076_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_99c4e563ed4c5d80d77ad06845bd33fa99e6b6402698aeaaf5c17e9baf7e8076->leave($__internal_99c4e563ed4c5d80d77ad06845bd33fa99e6b6402698aeaaf5c17e9baf7e8076_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_f1fcb9fd986e9cdc31135cd57a2eebbff0f941d73ce87c39cf2d5856c44f0b19 = $this->env->getExtension("native_profiler");
        $__internal_f1fcb9fd986e9cdc31135cd57a2eebbff0f941d73ce87c39cf2d5856c44f0b19->enter($__internal_f1fcb9fd986e9cdc31135cd57a2eebbff0f941d73ce87c39cf2d5856c44f0b19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_f1fcb9fd986e9cdc31135cd57a2eebbff0f941d73ce87c39cf2d5856c44f0b19->leave($__internal_f1fcb9fd986e9cdc31135cd57a2eebbff0f941d73ce87c39cf2d5856c44f0b19_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.subject'|trans({'%username%': user.username}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
