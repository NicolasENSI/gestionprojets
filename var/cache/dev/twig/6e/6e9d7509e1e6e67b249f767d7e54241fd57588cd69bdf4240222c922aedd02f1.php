<?php

/* GestionProjetHomePlatformBundle:Default:viewNotValidatePropositions.html.twig */
class __TwigTemplate_021d6297302107b50654ffa610277729e638104ecf8a98befc882a58c6ae82c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewNotValidatePropositions.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_12cfd41173ea6dfef8b8cb7df5c552c3180710ab0196f1377c025a670fb6e48f = $this->env->getExtension("native_profiler");
        $__internal_12cfd41173ea6dfef8b8cb7df5c552c3180710ab0196f1377c025a670fb6e48f->enter($__internal_12cfd41173ea6dfef8b8cb7df5c552c3180710ab0196f1377c025a670fb6e48f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:viewNotValidatePropositions.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_12cfd41173ea6dfef8b8cb7df5c552c3180710ab0196f1377c025a670fb6e48f->leave($__internal_12cfd41173ea6dfef8b8cb7df5c552c3180710ab0196f1377c025a670fb6e48f_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_e7fef3f552fe0d8b8def22a1536aa4fa348aed81c39e7d67cca132df04769db6 = $this->env->getExtension("native_profiler");
        $__internal_e7fef3f552fe0d8b8def22a1536aa4fa348aed81c39e7d67cca132df04769db6->enter($__internal_e7fef3f552fe0d8b8def22a1536aa4fa348aed81c39e7d67cca132df04769db6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
        
        $__internal_e7fef3f552fe0d8b8def22a1536aa4fa348aed81c39e7d67cca132df04769db6->leave($__internal_e7fef3f552fe0d8b8def22a1536aa4fa348aed81c39e7d67cca132df04769db6_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_25a72b1a298cffd2fe571c065dff5867e99b81bcbd55d939336e5f8ab18e517f = $this->env->getExtension("native_profiler");
        $__internal_25a72b1a298cffd2fe571c065dff5867e99b81bcbd55d939336e5f8ab18e517f->enter($__internal_25a72b1a298cffd2fe571c065dff5867e99b81bcbd55d939336e5f8ab18e517f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "




    <h1 class=\"page-header\">Tableau de bord</h1>

    ";
        // line 14
        try {
            $this->loadTemplate("GestionProjetHomePlatformBundle:Default:leftBoard.html.twig", "GestionProjetHomePlatformBundle:Default:viewNotValidatePropositions.html.twig", 14)->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 15
        echo "

    <div class=\"table-responsive\">
        <h2>Liste de projets</h2>
        ";
        // line 19
        try {
            $this->loadTemplate("GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig", "GestionProjetHomePlatformBundle:Default:viewNotValidatePropositions.html.twig", 19)->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 20
        echo "    </div>

";
        
        $__internal_25a72b1a298cffd2fe571c065dff5867e99b81bcbd55d939336e5f8ab18e517f->leave($__internal_25a72b1a298cffd2fe571c065dff5867e99b81bcbd55d939336e5f8ab18e517f_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewNotValidatePropositions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 20,  76 => 19,  70 => 15,  63 => 14,  54 => 7,  48 => 6,  35 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* */
/* */
/* */
/* */
/*     <h1 class="page-header">Tableau de bord</h1>*/
/* */
/*     {% include 'GestionProjetHomePlatformBundle:Default:leftBoard.html.twig' ignore missing %}*/
/* */
/* */
/*     <div class="table-responsive">*/
/*         <h2>Liste de projets</h2>*/
/*         {% include 'GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig' ignore missing %}*/
/*     </div>*/
/* */
/* {% endblock %}*/
