<?php

/* GestionProjetHomePlatformBundle:Default:askAccount.html.twig */
class __TwigTemplate_818e6053bda196b6cd689f51a304dac86b857d3c82859adc7868d8f95d81321d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a8c0a2013fa7d21edade111d27f00a74398ad32645356e0a471c8117e85cf5a = $this->env->getExtension("native_profiler");
        $__internal_4a8c0a2013fa7d21edade111d27f00a74398ad32645356e0a471c8117e85cf5a->enter($__internal_4a8c0a2013fa7d21edade111d27f00a74398ad32645356e0a471c8117e85cf5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:askAccount.html.twig"));

        // line 1
        echo "





<!doctype html>
<html lang=\"en\" class=\"no-js\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,700,400' rel='stylesheet' type='text/css'>
    <link rel=\"stylesheet\" href=\" ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/gestionprojethomeplatform/css/bootstrap.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/css/reset.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/css/style.css"), "html", null, true);
        echo "\">


    <script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/js/modernizr.js"), "html", null, true);
        echo "\"  ></script> <!-- Modernizr -->

    <title>ENSICAEN - Proposition  de projet</title>
</head>
<body>
<section class=\"cd-intro\">
    <div class=\"pull-left\"><span class=\"glyphicon glyphicon-remove \"></span> </div>
    <div class=\"cd-intro-content mask\">

        <h1 data-content=\"Demande de création de compte \"><span>Demande de création de compte</span></h1>
        <div class=\"action-wrapper\">
            ";
        // line 31
        echo "
            ";
        // line 32
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 93
        echo "        </div>
    </div>
</section>

<script src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/js/jquery-2.1.4.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/js/js/main.js"), "html", null, true);
        echo "\"></script> <!-- Resource jQuery -->
</body>
</html>



";
        
        $__internal_4a8c0a2013fa7d21edade111d27f00a74398ad32645356e0a471c8117e85cf5a->leave($__internal_4a8c0a2013fa7d21edade111d27f00a74398ad32645356e0a471c8117e85cf5a_prof);

    }

    // line 32
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ff84f2dcbeda88cd5667cbb48a69abd3b9974034f9aa3e6261777e64962a5af5 = $this->env->getExtension("native_profiler");
        $__internal_ff84f2dcbeda88cd5667cbb48a69abd3b9974034f9aa3e6261777e64962a5af5->enter($__internal_ff84f2dcbeda88cd5667cbb48a69abd3b9974034f9aa3e6261777e64962a5af5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 33
        echo "

                <div class=\"\">





                    ";
        // line 41
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("class" => "form-horizontal")));
        echo "

                    <div class=\"form-group\">
                        <label for=\"name\" class=\"\"> Entreprise ou établissement  </label>
                        <div class=\"col-sm-10\">
                            ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "entreprise", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label for=\"name\" class=\"col-sm-2 control-label\">Nom</label>
                        <div class=\"col-sm-10\">
                            ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label for=\"name\" class=\"col-sm-2 control-label\">Prénom</label>
                        <div class=\"col-sm-10\">
                            ";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label for=\"email\" class=\"col-sm-2 control-label\">Email</label>
                        <div class=\"col-sm-10\">
                            ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mail", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label for=\"message\" class=\"col-sm-2 control-label\">Message</label>
                        <div class=\"col-sm-10\">
                            ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "message", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                        </div>
                    </div>


                    <div class=\"form-group\">
                        ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "save", array()), 'widget', array("attr" => array("class" => "btn btn-primary col-sm-3 col-sm-offset-2")));
        echo "
                    </div>



                    ";
        // line 85
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "




                </div>

            ";
        
        $__internal_ff84f2dcbeda88cd5667cbb48a69abd3b9974034f9aa3e6261777e64962a5af5->leave($__internal_ff84f2dcbeda88cd5667cbb48a69abd3b9974034f9aa3e6261777e64962a5af5_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:askAccount.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 85,  168 => 80,  159 => 74,  149 => 67,  139 => 60,  129 => 53,  119 => 46,  111 => 41,  101 => 33,  95 => 32,  81 => 98,  77 => 97,  71 => 93,  69 => 32,  66 => 31,  52 => 19,  46 => 16,  42 => 15,  38 => 14,  23 => 1,);
    }
}
/* */
/* */
/* */
/* */
/* */
/* */
/* <!doctype html>*/
/* <html lang="en" class="no-js">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* */
/*     <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,700,400' rel='stylesheet' type='text/css'>*/
/*     <link rel="stylesheet" href=" {{ asset('bundles/gestionprojethomeplatform/css/bootstrap.css') }}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/HomePlatformBundle/css/reset.css')}}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/HomePlatformBundle/css/style.css')}}">*/
/* */
/* */
/*     <script src="{{ asset('bundles/HomePlatformBundle/js/modernizr.js')}}"  ></script> <!-- Modernizr -->*/
/* */
/*     <title>ENSICAEN - Proposition  de projet</title>*/
/* </head>*/
/* <body>*/
/* <section class="cd-intro">*/
/*     <div class="pull-left"><span class="glyphicon glyphicon-remove "></span> </div>*/
/*     <div class="cd-intro-content mask">*/
/* */
/*         <h1 data-content="Demande de création de compte "><span>Demande de création de compte</span></h1>*/
/*         <div class="action-wrapper">*/
/*             {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/*             {% block fos_user_content %}*/
/* */
/* */
/*                 <div class="">*/
/* */
/* */
/* */
/* */
/* */
/*                     {{ form_start(form, {'attr': {'class': 'form-horizontal'}}) }}*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="name" class=""> Entreprise ou établissement  </label>*/
/*                         <div class="col-sm-10">*/
/*                             {{ form_widget(form.entreprise, {'attr': {'class': 'form-control'}}) }}*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="name" class="col-sm-2 control-label">Nom</label>*/
/*                         <div class="col-sm-10">*/
/*                             {{ form_widget(form.nom,{'attr': {'class': 'form-control'}}) }}*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="name" class="col-sm-2 control-label">Prénom</label>*/
/*                         <div class="col-sm-10">*/
/*                             {{ form_widget(form.prenom, {'attr': {'class': 'form-control'}}) }}*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="email" class="col-sm-2 control-label">Email</label>*/
/*                         <div class="col-sm-10">*/
/*                             {{ form_widget(form.mail, {'attr': {'class': 'form-control'}}) }}*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="message" class="col-sm-2 control-label">Message</label>*/
/*                         <div class="col-sm-10">*/
/*                             {{ form_widget(form.message, {'attr': {'class': 'form-control'}}) }}*/
/*                         </div>*/
/*                     </div>*/
/* */
/* */
/*                     <div class="form-group">*/
/*                         {{ form_widget(form.save, {'attr': {'class': 'btn btn-primary col-sm-3 col-sm-offset-2'}}) }}*/
/*                     </div>*/
/* */
/* */
/* */
/*                     {{ form_end(form) }}*/
/* */
/* */
/* */
/* */
/*                 </div>*/
/* */
/*             {% endblock fos_user_content %}*/
/*         </div>*/
/*     </div>*/
/* </section>*/
/* */
/* <script src="{{ asset('bundles/HomePlatformBundle/js/jquery-2.1.4.js')}}"></script>*/
/* <script src="{{ asset('bundles/HomePlatformBundle/js/js/main.js')}}"></script> <!-- Resource jQuery -->*/
/* </body>*/
/* </html>*/
/* */
/* */
/* */
/* */
