<?php

/* FOSUserBundle:Registration:checkEmail.html.twig */
class __TwigTemplate_8ecae1a97242aabd7128de543462f512bfc5ba7beb9acd28fc9b59223db32465 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:checkEmail.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac257118c5b2afc2d60522497e232c057900f2e99ad1bce234118b4ca5f3c0a1 = $this->env->getExtension("native_profiler");
        $__internal_ac257118c5b2afc2d60522497e232c057900f2e99ad1bce234118b4ca5f3c0a1->enter($__internal_ac257118c5b2afc2d60522497e232c057900f2e99ad1bce234118b4ca5f3c0a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ac257118c5b2afc2d60522497e232c057900f2e99ad1bce234118b4ca5f3c0a1->leave($__internal_ac257118c5b2afc2d60522497e232c057900f2e99ad1bce234118b4ca5f3c0a1_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_65a7933eb84923798b2f6ac9d2a9e7ee7f56d280b2daef150b5b6bba5959b0f6 = $this->env->getExtension("native_profiler");
        $__internal_65a7933eb84923798b2f6ac9d2a9e7ee7f56d280b2daef150b5b6bba5959b0f6->enter($__internal_65a7933eb84923798b2f6ac9d2a9e7ee7f56d280b2daef150b5b6bba5959b0f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_65a7933eb84923798b2f6ac9d2a9e7ee7f56d280b2daef150b5b6bba5959b0f6->leave($__internal_65a7933eb84923798b2f6ac9d2a9e7ee7f56d280b2daef150b5b6bba5959b0f6_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>*/
/* {% endblock fos_user_content %}*/
/* */
