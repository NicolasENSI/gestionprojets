<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_45480607498d69be542f7900f8591d404e028f2e2a37870e4345c5edc405c6f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e2198a0ef5f95efe22888881b0eb104aa586af2802c2dec2e4e6f523e0cdd9f8 = $this->env->getExtension("native_profiler");
        $__internal_e2198a0ef5f95efe22888881b0eb104aa586af2802c2dec2e4e6f523e0cdd9f8->enter($__internal_e2198a0ef5f95efe22888881b0eb104aa586af2802c2dec2e4e6f523e0cdd9f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e2198a0ef5f95efe22888881b0eb104aa586af2802c2dec2e4e6f523e0cdd9f8->leave($__internal_e2198a0ef5f95efe22888881b0eb104aa586af2802c2dec2e4e6f523e0cdd9f8_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_d979680613928565821e4f721f59b8153394414fbfb591b2bfe12380d2626dd9 = $this->env->getExtension("native_profiler");
        $__internal_d979680613928565821e4f721f59b8153394414fbfb591b2bfe12380d2626dd9->enter($__internal_d979680613928565821e4f721f59b8153394414fbfb591b2bfe12380d2626dd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_d979680613928565821e4f721f59b8153394414fbfb591b2bfe12380d2626dd9->leave($__internal_d979680613928565821e4f721f59b8153394414fbfb591b2bfe12380d2626dd9_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_68e62c8038119803d82e9e3fc6328ae895d80a700b3f4c4b8fa018d36e1c1fd5 = $this->env->getExtension("native_profiler");
        $__internal_68e62c8038119803d82e9e3fc6328ae895d80a700b3f4c4b8fa018d36e1c1fd5->enter($__internal_68e62c8038119803d82e9e3fc6328ae895d80a700b3f4c4b8fa018d36e1c1fd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_68e62c8038119803d82e9e3fc6328ae895d80a700b3f4c4b8fa018d36e1c1fd5->leave($__internal_68e62c8038119803d82e9e3fc6328ae895d80a700b3f4c4b8fa018d36e1c1fd5_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_025113f868d8ff7dfc6452d3eeb2f5ad4d527ed24ea2db65b12feba7064f834a = $this->env->getExtension("native_profiler");
        $__internal_025113f868d8ff7dfc6452d3eeb2f5ad4d527ed24ea2db65b12feba7064f834a->enter($__internal_025113f868d8ff7dfc6452d3eeb2f5ad4d527ed24ea2db65b12feba7064f834a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_025113f868d8ff7dfc6452d3eeb2f5ad4d527ed24ea2db65b12feba7064f834a->leave($__internal_025113f868d8ff7dfc6452d3eeb2f5ad4d527ed24ea2db65b12feba7064f834a_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
