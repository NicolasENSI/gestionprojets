<?php

/* GestionProjetHomePlatformBundle:Default:viewDetailProposition.html.twig */
class __TwigTemplate_6fbbc53fe83e0bf126301e023e567c4f7e9e2968f5da107b66de6b0f9bfcc682 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewDetailProposition.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d115834e901eb165d07c3b981687b96149b387fefe0dfa2b687062fc2dc4cd89 = $this->env->getExtension("native_profiler");
        $__internal_d115834e901eb165d07c3b981687b96149b387fefe0dfa2b687062fc2dc4cd89->enter($__internal_d115834e901eb165d07c3b981687b96149b387fefe0dfa2b687062fc2dc4cd89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:viewDetailProposition.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d115834e901eb165d07c3b981687b96149b387fefe0dfa2b687062fc2dc4cd89->leave($__internal_d115834e901eb165d07c3b981687b96149b387fefe0dfa2b687062fc2dc4cd89_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_565e807f1115fb6f4fb1923422345e8d537f366a957e7f1ef844811933020571 = $this->env->getExtension("native_profiler");
        $__internal_565e807f1115fb6f4fb1923422345e8d537f366a957e7f1ef844811933020571->enter($__internal_565e807f1115fb6f4fb1923422345e8d537f366a957e7f1ef844811933020571_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "
";
        
        $__internal_565e807f1115fb6f4fb1923422345e8d537f366a957e7f1ef844811933020571->leave($__internal_565e807f1115fb6f4fb1923422345e8d537f366a957e7f1ef844811933020571_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_14c8d89f5334b58286bb64ff651fbd7cb86d988d16d24c9c29257f06e64e0a1c = $this->env->getExtension("native_profiler");
        $__internal_14c8d89f5334b58286bb64ff651fbd7cb86d988d16d24c9c29257f06e64e0a1c->enter($__internal_14c8d89f5334b58286bb64ff651fbd7cb86d988d16d24c9c29257f06e64e0a1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "
 <div class=\"col-sm-9 col-sm-offset-4 col-md-10 col-md-offset-2 main\">

     <h1 class=\"page-header\">Le projet en détail : </h1>
     <h3> Intitulé : ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "intituleProjet", array()), "html", null, true);
        echo "</h3>
     <h3> Type de projet : ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "mission", array()), "html", null, true);
        echo "</h3>
     <h3> Entreprise : ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "entreprise", array()), "html", null, true);
        echo "</h3>
     <h3> Nom : ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "nom", array()), "html", null, true);
        echo " </h3>
     <h3> Prenom : ";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "prenom", array()), "html", null, true);
        echo " </h3>
    ";
        // line 17
        if ($this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "brochure", array())) {
            // line 18
            echo "     <h3> Sujet en détail : <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/brochures/" . $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "brochure", array()))), "html", null, true);
            echo "\">Voir détail (PDF)</a> </h3>
     ";
        }
        // line 20
        echo "     <h3> Resumé : </h3>
     <h4>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "description", array()), "html", null, true);
        echo "</h4>



     ";
        // line 25
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 26
            echo "
     ";
            // line 27
            if (($this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "valide", array()) == 0)) {
                // line 28
                echo "        <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_validation_project", array("id" => $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "id", array()), "state" => 1)), "html", null, true);
                echo "\">
            <button type=\"button\" class=\"btn btn-success\">Valider le projet</button>
        </a>
        <button type=\"button\" class=\"btn btn-default\">Contacter</button>
     <a href=\"";
                // line 32
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_validation_project", array("id" => $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "id", array()), "state" => 2)), "html", null, true);
                echo "\">
        <button type=\"button\" class=\"btn btn-danger\">Refuser le projet</button>
         </a>
     ";
            }
            // line 36
            echo "
     ";
            // line 37
            if (($this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "valide", array()) == 1)) {
                // line 38
                echo "     <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_validation_project", array("id" => $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "id", array()), "state" => 2)), "html", null, true);
                echo "\">
     <button type=\"button\" class=\"btn btn-danger\"> Dévalider le projet </button>
         </a>
     <button type=\"button\" class=\"btn btn-default\">Contacter</button>
     ";
            }
            // line 43
            echo "


     ";
            // line 46
            if (($this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "valide", array()) == 2)) {
                // line 47
                echo "     <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_validation_project", array("id" => $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "id", array()), "state" => 1)), "html", null, true);
                echo "\">
     <button type=\"button\" class=\"btn btn-danger\"> Valider quand même le projet </button>
         </a>
     <button type=\"button\" class=\"btn btn-default\">Contacter</button>
     ";
            }
            // line 52
            echo "
     ";
        }
        // line 54
        echo "
     ";
        // line 55
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            // line 56
            echo "     <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_delete_proposition", array("id" => $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : $this->getContext($context, "proposition")), "id", array()))), "html", null, true);
            echo "\">
         <button type=\"button\" class=\"btn btn-danger\"> Supprimer cette proposition </button>
     </a>

         </div>
     ";
        }
        // line 62
        echo "



";
        
        $__internal_14c8d89f5334b58286bb64ff651fbd7cb86d988d16d24c9c29257f06e64e0a1c->leave($__internal_14c8d89f5334b58286bb64ff651fbd7cb86d988d16d24c9c29257f06e64e0a1c_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewDetailProposition.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 62,  160 => 56,  158 => 55,  155 => 54,  151 => 52,  142 => 47,  140 => 46,  135 => 43,  126 => 38,  124 => 37,  121 => 36,  114 => 32,  106 => 28,  104 => 27,  101 => 26,  99 => 25,  92 => 21,  89 => 20,  83 => 18,  81 => 17,  77 => 16,  73 => 15,  69 => 14,  65 => 13,  61 => 12,  55 => 8,  49 => 7,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'GestionProjetHomePlatformBundle::layout.html.twig' %}*/
/* */
/* {% block title %}*/
/* */
/* {% endblock %}*/
/* */
/* {% block body %}*/
/* */
/*  <div class="col-sm-9 col-sm-offset-4 col-md-10 col-md-offset-2 main">*/
/* */
/*      <h1 class="page-header">Le projet en détail : </h1>*/
/*      <h3> Intitulé : {{ proposition.intituleProjet }}</h3>*/
/*      <h3> Type de projet : {{ proposition.mission }}</h3>*/
/*      <h3> Entreprise : {{ proposition.entreprise }}</h3>*/
/*      <h3> Nom : {{ proposition.nom }} </h3>*/
/*      <h3> Prenom : {{ proposition.prenom }} </h3>*/
/*     {% if proposition.brochure %}*/
/*      <h3> Sujet en détail : <a href="{{ asset('uploads/brochures/' ~ proposition.brochure) }}">Voir détail (PDF)</a> </h3>*/
/*      {% endif %}*/
/*      <h3> Resumé : </h3>*/
/*      <h4>{{ proposition.description }}</h4>*/
/* */
/* */
/* */
/*      {% if is_granted('ROLE_ADMIN') %}*/
/* */
/*      {% if proposition.valide == 0 %}*/
/*         <a href="{{ path('gestion_projet_home_platform_validation_project', {'id': proposition.id, 'state' : 1}) }}">*/
/*             <button type="button" class="btn btn-success">Valider le projet</button>*/
/*         </a>*/
/*         <button type="button" class="btn btn-default">Contacter</button>*/
/*      <a href="{{ path('gestion_projet_home_platform_validation_project', {'id': proposition.id, 'state' : 2}) }}">*/
/*         <button type="button" class="btn btn-danger">Refuser le projet</button>*/
/*          </a>*/
/*      {% endif %}*/
/* */
/*      {% if proposition.valide == 1 %}*/
/*      <a href="{{ path('gestion_projet_home_platform_validation_project', {'id': proposition.id, 'state' : 2}) }}">*/
/*      <button type="button" class="btn btn-danger"> Dévalider le projet </button>*/
/*          </a>*/
/*      <button type="button" class="btn btn-default">Contacter</button>*/
/*      {% endif %}*/
/* */
/* */
/* */
/*      {% if proposition.valide == 2 %}*/
/*      <a href="{{ path('gestion_projet_home_platform_validation_project', {'id': proposition.id, 'state' : 1}) }}">*/
/*      <button type="button" class="btn btn-danger"> Valider quand même le projet </button>*/
/*          </a>*/
/*      <button type="button" class="btn btn-default">Contacter</button>*/
/*      {% endif %}*/
/* */
/*      {% endif %}*/
/* */
/*      {% if is_granted('ROLE_USER') %}*/
/*      <a href="{{ path('gestion_projet_home_platform_delete_proposition', {'id': proposition.id}) }}">*/
/*          <button type="button" class="btn btn-danger"> Supprimer cette proposition </button>*/
/*      </a>*/
/* */
/*          </div>*/
/*      {% endif %}*/
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
