<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_bdb8833d3719fed3158de95c3a68b8e8225684add1529dc1dcf5b3a3b534d7f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f79f6ea62802d810ccc39f00cc1aad99b39557995963f18903f53ee8a946e43 = $this->env->getExtension("native_profiler");
        $__internal_7f79f6ea62802d810ccc39f00cc1aad99b39557995963f18903f53ee8a946e43->enter($__internal_7f79f6ea62802d810ccc39f00cc1aad99b39557995963f18903f53ee8a946e43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7f79f6ea62802d810ccc39f00cc1aad99b39557995963f18903f53ee8a946e43->leave($__internal_7f79f6ea62802d810ccc39f00cc1aad99b39557995963f18903f53ee8a946e43_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_f46364aee217570c982c25d30e87b357578f69d7ec713de05791ea54ba9bf85a = $this->env->getExtension("native_profiler");
        $__internal_f46364aee217570c982c25d30e87b357578f69d7ec713de05791ea54ba9bf85a->enter($__internal_f46364aee217570c982c25d30e87b357578f69d7ec713de05791ea54ba9bf85a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_f46364aee217570c982c25d30e87b357578f69d7ec713de05791ea54ba9bf85a->leave($__internal_f46364aee217570c982c25d30e87b357578f69d7ec713de05791ea54ba9bf85a_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_57a08a743758a4ef0e80e0535601b6f3ad59ee7bc4eec23a1eb08fef71e14577 = $this->env->getExtension("native_profiler");
        $__internal_57a08a743758a4ef0e80e0535601b6f3ad59ee7bc4eec23a1eb08fef71e14577->enter($__internal_57a08a743758a4ef0e80e0535601b6f3ad59ee7bc4eec23a1eb08fef71e14577_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_57a08a743758a4ef0e80e0535601b6f3ad59ee7bc4eec23a1eb08fef71e14577->leave($__internal_57a08a743758a4ef0e80e0535601b6f3ad59ee7bc4eec23a1eb08fef71e14577_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_d95208847249b6f234527539089e13208681d5d81d352d7bf081a1c610c1c4a2 = $this->env->getExtension("native_profiler");
        $__internal_d95208847249b6f234527539089e13208681d5d81d352d7bf081a1c610c1c4a2->enter($__internal_d95208847249b6f234527539089e13208681d5d81d352d7bf081a1c610c1c4a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_d95208847249b6f234527539089e13208681d5d81d352d7bf081a1c610c1c4a2->leave($__internal_d95208847249b6f234527539089e13208681d5d81d352d7bf081a1c610c1c4a2_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
