<?php

/* GestionProjetHomePlatformBundle:Default:proposition.html.twig */
class __TwigTemplate_b13702d8fa23f343a0ed7421afc20ec70ec212b70057049c4ec81216f71cad11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:proposition.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_91e7679fcad67f5001b2e9480d390333e9881b8a03669f8d5b926685e7acd97a = $this->env->getExtension("native_profiler");
        $__internal_91e7679fcad67f5001b2e9480d390333e9881b8a03669f8d5b926685e7acd97a->enter($__internal_91e7679fcad67f5001b2e9480d390333e9881b8a03669f8d5b926685e7acd97a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:proposition.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_91e7679fcad67f5001b2e9480d390333e9881b8a03669f8d5b926685e7acd97a->leave($__internal_91e7679fcad67f5001b2e9480d390333e9881b8a03669f8d5b926685e7acd97a_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_c25ecf55f9c7237a7d2c10f3422b953bf62370ed9aecceaac88c4761c9ea87e7 = $this->env->getExtension("native_profiler");
        $__internal_c25ecf55f9c7237a7d2c10f3422b953bf62370ed9aecceaac88c4761c9ea87e7->enter($__internal_c25ecf55f9c7237a7d2c10f3422b953bf62370ed9aecceaac88c4761c9ea87e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
        
        $__internal_c25ecf55f9c7237a7d2c10f3422b953bf62370ed9aecceaac88c4761c9ea87e7->leave($__internal_c25ecf55f9c7237a7d2c10f3422b953bf62370ed9aecceaac88c4761c9ea87e7_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_602d27274c62d8eff2452fd34f33b77c2fc0916967213b6f60ad779e09ca00e3 = $this->env->getExtension("native_profiler");
        $__internal_602d27274c62d8eff2452fd34f33b77c2fc0916967213b6f60ad779e09ca00e3->enter($__internal_602d27274c62d8eff2452fd34f33b77c2fc0916967213b6f60ad779e09ca00e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "

    <div class=\"container  col-md-10 col-lg-10 col-md-offset-1\">

        <div class=\"starter-template\">
            <img id=logoensi\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/images/logoensi.jpg"), "html", null, true);
        echo "\" align=\"center\" height=\"100\" >
        <h1>Formulaire de proposition d'un projet </h1></br>
        </div>

        ";
        // line 16
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("class" => "form-horizontal form-prop")));
        echo "

        ";
        // line 19
        echo "

        <div class=\"form-group\">
            ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label "), "label" => "Nom"));
        echo "
            <div class=\"col-sm-10\">
                ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Prénom"));
        echo "
            <div class=\"col-sm-10\">
                ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "entreprise", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Entreprise"));
        echo "
            <div class=\"col-sm-10\">
                ";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "entreprise", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "intituleProjet", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Intitulé du projet"));
        echo "
            <div class=\"col-sm-10\">
                ";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "intituleProjet", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Description du projet"));
        echo "
            <div class=\"col-sm-10\">
                ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mission", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Type de mission (1-etude, 2-prototypage, 3-veille, 4-recherche documentaire)"));
        echo "
            <div class=\"col-sm-10\">
                ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mission", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "accompagnant", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Je viens accompagné de "));
        echo "
            <div class=\"col-sm-2\">
                ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "accompagnant", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 71
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "present", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Je serai présent à la réunion"));
        echo "
            <div class=\"col-sm-1\">
                ";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "present", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "repas", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Je serai présent au repas"));
        echo "
            <div class=\"col-sm-1\">
                ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "repas", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>



        ";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "repas", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Fichier descriptif (optionnel)"));
        echo "
            ";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "brochure", array()), 'widget');
        echo "
        </div>



        <div class=\"form-group\">
            <div class=\"form-group\">
                ";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "save", array()), 'widget', array("attr" => array("class" => "btn btn-primary col-sm-3 col-sm-offset-2 form-prop-save")));
        echo "
            </div>
        </div>
    ";
        // line 97
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "



    </div>

";
        
        $__internal_602d27274c62d8eff2452fd34f33b77c2fc0916967213b6f60ad779e09ca00e3->leave($__internal_602d27274c62d8eff2452fd34f33b77c2fc0916967213b6f60ad779e09ca00e3_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:proposition.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 97,  210 => 94,  200 => 87,  196 => 86,  187 => 80,  182 => 78,  174 => 73,  169 => 71,  161 => 66,  156 => 64,  148 => 59,  143 => 57,  135 => 52,  130 => 50,  122 => 45,  117 => 43,  109 => 38,  104 => 36,  96 => 31,  91 => 29,  83 => 24,  78 => 22,  73 => 19,  68 => 16,  61 => 12,  54 => 7,  48 => 6,  35 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* */
/*     <div class="container  col-md-10 col-lg-10 col-md-offset-1">*/
/* */
/*         <div class="starter-template">*/
/*             <img id=logoensi" src="{{ asset('bundles/HomePlatformBundle/images/logoensi.jpg') }}" align="center" height="100" >*/
/*         <h1>Formulaire de proposition d'un projet </h1></br>*/
/*         </div>*/
/* */
/*         {{ form_start(form, {'attr': {'class': 'form-horizontal form-prop'}}) }}*/
/* */
/*         {# Le formulaire, avec URL de soumission vers la routea completer comme on l'a vu #}*/
/* */
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.nom, "Nom", {'label_attr': {'class': 'col-sm-2 control-label '}}) }}*/
/*             <div class="col-sm-10">*/
/*                 {{ form_widget(form.nom, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.prenom, "Prénom", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-10">*/
/*                 {{ form_widget(form.prenom, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.entreprise, "Entreprise", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-10">*/
/*                 {{ form_widget(form.entreprise, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.intituleProjet, "Intitulé du projet", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-10">*/
/*                 {{ form_widget(form.intituleProjet, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.description, "Description du projet", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-10">*/
/*                 {{ form_widget(form.description, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.mission, "Type de mission (1-etude, 2-prototypage, 3-veille, 4-recherche documentaire)", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-10">*/
/*                 {{ form_widget(form.mission, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.accompagnant, "Je viens accompagné de ", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-2">*/
/*                 {{ form_widget(form.accompagnant, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.present, "Je serai présent à la réunion", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-1">*/
/*                 {{ form_widget(form.present, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.repas, "Je serai présent au repas", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-1">*/
/*                 {{ form_widget(form.repas, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/* */
/* */
/*         {{ form_label(form.repas, "Fichier descriptif (optionnel)", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             {{ form_widget(form.brochure) }}*/
/*         </div>*/
/* */
/* */
/* */
/*         <div class="form-group">*/
/*             <div class="form-group">*/
/*                 {{ form_widget(form.save, {'attr': {'class': 'btn btn-primary col-sm-3 col-sm-offset-2 form-prop-save'}}) }}*/
/*             </div>*/
/*         </div>*/
/*     {{ form_rest(form) }}*/
/* */
/* */
/* */
/*     </div>*/
/* */
/* {% endblock %}*/
/* */
