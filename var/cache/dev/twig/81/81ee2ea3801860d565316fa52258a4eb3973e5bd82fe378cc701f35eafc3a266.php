<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_025b308e786fab3e45f7de25e31a3dd44030d1844453c4daebd2d1202195720b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_788d7352b03ab93c3832d45db16610e763a24f4788e01eb5f0770416979022ef = $this->env->getExtension("native_profiler");
        $__internal_788d7352b03ab93c3832d45db16610e763a24f4788e01eb5f0770416979022ef->enter($__internal_788d7352b03ab93c3832d45db16610e763a24f4788e01eb5f0770416979022ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_788d7352b03ab93c3832d45db16610e763a24f4788e01eb5f0770416979022ef->leave($__internal_788d7352b03ab93c3832d45db16610e763a24f4788e01eb5f0770416979022ef_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_67115fe8b4aec1745614c96c876c2c7046407cf67699e319c8607de4652a6e99 = $this->env->getExtension("native_profiler");
        $__internal_67115fe8b4aec1745614c96c876c2c7046407cf67699e319c8607de4652a6e99->enter($__internal_67115fe8b4aec1745614c96c876c2c7046407cf67699e319c8607de4652a6e99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_67115fe8b4aec1745614c96c876c2c7046407cf67699e319c8607de4652a6e99->leave($__internal_67115fe8b4aec1745614c96c876c2c7046407cf67699e319c8607de4652a6e99_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_3347f47730f68a938ec8814c15ba5f3706f2ede03fa7117462824b250fc86d00 = $this->env->getExtension("native_profiler");
        $__internal_3347f47730f68a938ec8814c15ba5f3706f2ede03fa7117462824b250fc86d00->enter($__internal_3347f47730f68a938ec8814c15ba5f3706f2ede03fa7117462824b250fc86d00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_3347f47730f68a938ec8814c15ba5f3706f2ede03fa7117462824b250fc86d00->leave($__internal_3347f47730f68a938ec8814c15ba5f3706f2ede03fa7117462824b250fc86d00_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_d0fe1cc2e735f1d66f53c4a2342da293f84df1135be2a3f8d7925a47a469a6e8 = $this->env->getExtension("native_profiler");
        $__internal_d0fe1cc2e735f1d66f53c4a2342da293f84df1135be2a3f8d7925a47a469a6e8->enter($__internal_d0fe1cc2e735f1d66f53c4a2342da293f84df1135be2a3f8d7925a47a469a6e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_d0fe1cc2e735f1d66f53c4a2342da293f84df1135be2a3f8d7925a47a469a6e8->leave($__internal_d0fe1cc2e735f1d66f53c4a2342da293f84df1135be2a3f8d7925a47a469a6e8_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
