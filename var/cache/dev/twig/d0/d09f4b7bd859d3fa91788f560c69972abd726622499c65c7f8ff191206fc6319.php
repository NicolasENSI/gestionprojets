<?php

/* FOSUserBundle::layout.html.twig */
class __TwigTemplate_4da8bfe1943b9474b5095eb75c1feb4e615f69194cb4588c6ab1f622cce83374 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 4
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "FOSUserBundle::layout.html.twig", 4);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6a48e74c0af9b28820426ff41d6c540fe528d92ec5adb80569b5ef87ec3eafd = $this->env->getExtension("native_profiler");
        $__internal_e6a48e74c0af9b28820426ff41d6c540fe528d92ec5adb80569b5ef87ec3eafd->enter($__internal_e6a48e74c0af9b28820426ff41d6c540fe528d92ec5adb80569b5ef87ec3eafd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e6a48e74c0af9b28820426ff41d6c540fe528d92ec5adb80569b5ef87ec3eafd->leave($__internal_e6a48e74c0af9b28820426ff41d6c540fe528d92ec5adb80569b5ef87ec3eafd_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_6f08ea04695994721053cca6cc4ea47adaabe16f8ac5fc56b04a8d5912841678 = $this->env->getExtension("native_profiler");
        $__internal_6f08ea04695994721053cca6cc4ea47adaabe16f8ac5fc56b04a8d5912841678->enter($__internal_6f08ea04695994721053cca6cc4ea47adaabe16f8ac5fc56b04a8d5912841678_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "


  ";
        // line 12
        echo "  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
        foreach ($context['_seq'] as $context["key"] => $context["messages"]) {
            // line 13
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 14
                echo "      <div class=\"alert alert-";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\">
        ";
                // line 15
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($context["message"], array(), "FOSUserBundle"), "html", null, true);
                echo "
      </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
  ";
        // line 21
        echo "  ";
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 24
        echo "  </div>

";
        
        $__internal_6f08ea04695994721053cca6cc4ea47adaabe16f8ac5fc56b04a8d5912841678->leave($__internal_6f08ea04695994721053cca6cc4ea47adaabe16f8ac5fc56b04a8d5912841678_prof);

    }

    // line 21
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ffb3d5357c08845a0b1c00fa1efeb7997db213bbf47a08882559ba67c92f3fb1 = $this->env->getExtension("native_profiler");
        $__internal_ffb3d5357c08845a0b1c00fa1efeb7997db213bbf47a08882559ba67c92f3fb1->enter($__internal_ffb3d5357c08845a0b1c00fa1efeb7997db213bbf47a08882559ba67c92f3fb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 22
        echo "
  ";
        
        $__internal_ffb3d5357c08845a0b1c00fa1efeb7997db213bbf47a08882559ba67c92f3fb1->leave($__internal_ffb3d5357c08845a0b1c00fa1efeb7997db213bbf47a08882559ba67c92f3fb1_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 22,  91 => 21,  82 => 24,  79 => 21,  76 => 19,  70 => 18,  61 => 15,  56 => 14,  51 => 13,  46 => 12,  41 => 8,  35 => 7,  11 => 4,);
    }
}
/* {# src/OC/UserBundle/Resources/views/layout.html.twig #}*/
/* */
/* {# On étend notre layout #}*/
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* {# Dans notre layout, il faut définir le block body #}*/
/* {% block body %}*/
/* */
/* */
/* */
/*   {# On affiche les messages flash que définissent les contrôleurs du bundle #}*/
/*   {% for key, messages in app.session.flashbag.all() %}*/
/*     {% for message in messages %}*/
/*       <div class="alert alert-{{ key }}">*/
/*         {{ message|trans({}, 'FOSUserBundle') }}*/
/*       </div>*/
/*     {% endfor %}*/
/*   {% endfor %}*/
/* */
/*   {# On définit ce block, dans lequel vont venir s'insérer les autres vues du bundle #}*/
/*   {% block fos_user_content %}*/
/* */
/*   {% endblock fos_user_content %}*/
/*   </div>*/
/* */
/* {% endblock %}*/
