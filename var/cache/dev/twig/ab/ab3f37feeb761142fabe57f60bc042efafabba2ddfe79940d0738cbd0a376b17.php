<?php

/* GestionProjetHomePlatformBundle:Default:viewValidatePropositions.html.twig */
class __TwigTemplate_91d24dd38603e5d337cadf454835d9c8a55016bc6344c0cb476276ef74c09aac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewValidatePropositions.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_520bde12339184734b6eba3ae29bca51178d0e6d855e7cb725649140bcba504e = $this->env->getExtension("native_profiler");
        $__internal_520bde12339184734b6eba3ae29bca51178d0e6d855e7cb725649140bcba504e->enter($__internal_520bde12339184734b6eba3ae29bca51178d0e6d855e7cb725649140bcba504e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:viewValidatePropositions.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_520bde12339184734b6eba3ae29bca51178d0e6d855e7cb725649140bcba504e->leave($__internal_520bde12339184734b6eba3ae29bca51178d0e6d855e7cb725649140bcba504e_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_ed90e768022510230d6b783dd97472ecf6c51f7a7a7dae5f1eebb6ff4d8488d1 = $this->env->getExtension("native_profiler");
        $__internal_ed90e768022510230d6b783dd97472ecf6c51f7a7a7dae5f1eebb6ff4d8488d1->enter($__internal_ed90e768022510230d6b783dd97472ecf6c51f7a7a7dae5f1eebb6ff4d8488d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
        
        $__internal_ed90e768022510230d6b783dd97472ecf6c51f7a7a7dae5f1eebb6ff4d8488d1->leave($__internal_ed90e768022510230d6b783dd97472ecf6c51f7a7a7dae5f1eebb6ff4d8488d1_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_6e50bfb7717a1d5d474df069be0bf3526cbd72987520c727aa92f0ce77c97589 = $this->env->getExtension("native_profiler");
        $__internal_6e50bfb7717a1d5d474df069be0bf3526cbd72987520c727aa92f0ce77c97589->enter($__internal_6e50bfb7717a1d5d474df069be0bf3526cbd72987520c727aa92f0ce77c97589_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "





    <h1 class=\"page-header\">Tableau de bord</h1>
    ";
        // line 14
        try {
            $this->loadTemplate("GestionProjetHomePlatformBundle:Default:leftBoard.html.twig", "GestionProjetHomePlatformBundle:Default:viewValidatePropositions.html.twig", 14)->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 15
        echo "

    <div class=\"table-responsive\">
        <h2>Liste de projets <validés></validés></h2>
        ";
        // line 19
        $this->loadTemplate("@GestionProjetHomePlatform/Default/headerArrayProposition.html.twig", "GestionProjetHomePlatformBundle:Default:viewValidatePropositions.html.twig", 19)->display($context);
        // line 20
        echo "
";
        
        $__internal_6e50bfb7717a1d5d474df069be0bf3526cbd72987520c727aa92f0ce77c97589->leave($__internal_6e50bfb7717a1d5d474df069be0bf3526cbd72987520c727aa92f0ce77c97589_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewValidatePropositions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 20,  76 => 19,  70 => 15,  63 => 14,  54 => 7,  48 => 6,  35 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/*     <h1 class="page-header">Tableau de bord</h1>*/
/*     {% include 'GestionProjetHomePlatformBundle:Default:leftBoard.html.twig' ignore missing %}*/
/* */
/* */
/*     <div class="table-responsive">*/
/*         <h2>Liste de projets <validés></validés></h2>*/
/*         {% include '@GestionProjetHomePlatform/Default/headerArrayProposition.html.twig'  %}*/
/* */
/* {% endblock %}*/
