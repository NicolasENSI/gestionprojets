<?php

/* GestionProjetHomePlatformBundle:Default:modalites.html.twig */
class __TwigTemplate_b2eb9746f8bd43d500edde30d59e06e097605efcfe7fc9ac9d04b214f7737e1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:modalites.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9a1251452e3e725e1b547f0baa55a2f237fb5ed1072dee09b86282f31dbbfb6 = $this->env->getExtension("native_profiler");
        $__internal_a9a1251452e3e725e1b547f0baa55a2f237fb5ed1072dee09b86282f31dbbfb6->enter($__internal_a9a1251452e3e725e1b547f0baa55a2f237fb5ed1072dee09b86282f31dbbfb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:modalites.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a9a1251452e3e725e1b547f0baa55a2f237fb5ed1072dee09b86282f31dbbfb6->leave($__internal_a9a1251452e3e725e1b547f0baa55a2f237fb5ed1072dee09b86282f31dbbfb6_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_0c71ee3663cdafaa86c3c5eb2fd221b41d822af4267f171687ec8fed70a844f7 = $this->env->getExtension("native_profiler");
        $__internal_0c71ee3663cdafaa86c3c5eb2fd221b41d822af4267f171687ec8fed70a844f7->enter($__internal_0c71ee3663cdafaa86c3c5eb2fd221b41d822af4267f171687ec8fed70a844f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "


    <div class=\"placeholders col-md-10 col-lg-10 col-md-offset-1\">
        <div class=\"starter-template\">
            <img id=logoensi\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/images/logoensi.jpg"), "html", null, true);
        echo "\" align=\"center\" height=\"100\" >
        <h1 class=\"page-header\">Modalités de proposition des projets</h1>
            </div>
        <div class=\" placeholder col-md-12\"><p>
            <h4 class=\"infoMod\">Vous trouverez dans cette rubrique l'ensemble des informations necessaires
                à prendre en compte avant de proposer un projet. N'hesitez pas à nous contacter par mail
                si vous avez des questions supplémentaires. </h4>
        </div>

        <br>
        <div class=\"placeholder row col-md-12\">

            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Type de prestation</h3>

                </div>
                <div class=\"panel-body\">
                    Etude, prototypage, veille, recherche documentaire…
                </div>
            </div>

            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Format de la prestation</h3>

                </div>
                <div class=\"panel-body \">
                    Un groupe d'élèves ingénieurs (1, 2 ou 3 élèves) en fin de cycle (3ème année) </br>
                    Temps consacré par élève et par semaine : 1 à 2 journées</br>
                    Début : mi-septembre</br>
                    Fin : mi-février</br>
                </div>
            </div>

            <div class=\"panel panel-default\">
                <div class=\"panel-heading\"><h3 class=\"panel-title\">Encadrement</h3>
                </div>

                <div class=\"panel-body\">
                    Un tuteur école et un tuteur en entreprise.
                </div>
            </div>

            <div class=\"panel panel-default\">
                <div class=\"panel-heading\"><h3 class=\"panel-title\">Modalités pratiques</h3> <span
                            class=\"pull-right clickable\"><i class=\"glyphicon glyphicon-chevron-up\"></i></span></div>
                <div class=\"panel-body\" style=\"display: none;\">
                    Proposition d'un sujet au plus tard le 12 septembre 2015</br>
                    Présentation aux élèves ingénieurs le 25 septembre 2015</br>
                    Affectation des projets le 1er octobre 2015 (vœux entreprises et étudiants)</br>
                </div>
            </div>

            <div class=\"panel panel-default\">
                <div class=\"panel-heading\"><h3 class=\"panel-title\">Coût de la prestation</h3> <span
                            class=\"pull-right clickable\"><i class=\"glyphicon glyphicon-chevron-up\"></i></span></div>
                <div class=\"panel-body\" style=\"display: none;\">
                    La mise en œuvre d'un projet est assortie d'une convention et représente un coût hors taxes fixé
                    forfaitairement à 1 200 euros. Ce montant couvre l'ensemble de la prestation (encadrement, petit
                    consommable, entretien et renouvellement des équipements) à l'exclusion de tous les équipements ou
                    composants spéciaux et les licences d'exploitation, non disponibles à l'ENSICAEN.
                </div>
            </div>

            <div class=\"panel panel-default\">
                <div class=\"panel-heading\"><h3 class=\"panel-title\">Financements possibles</h3> <span
                            class=\"pull-right clickable\"><i class=\"glyphicon glyphicon-chevron-up\"></i></span></div>
                <div class=\"panel-body\" style=\"display: none;\">
                    OSEO, Crédit d’impôt, Prestation Technologique Réseau, Impulsion Conseil…
                </div>
            </div>

            <div class=\"panel panel-default\">
                <div class=\"panel-heading\"><h3 class=\"panel-title\">Suites envisageables</h3> <span
                            class=\"pull-right clickable\"><i class=\"glyphicon glyphicon-chevron-up\"></i></span></div>
                <div class=\"panel-body\" style=\"display: none;\">
                    Stages de longue durée d'élèves ingénieurs dans l'entreprise (5 à 6 mois),</br>
                    Embauche d'ingénieurs de l'ENSICAEN,</br>
                    Thèse de doctorat, thèse CIFRE (aide ANRT)</br>
                    Prestation de R&D,</br>
                    Contrat de recherche</br>
                </div>
            </div>
        </div>


    </div>

    <script>


    </script>


";
        
        $__internal_0c71ee3663cdafaa86c3c5eb2fd221b41d822af4267f171687ec8fed70a844f7->leave($__internal_0c71ee3663cdafaa86c3c5eb2fd221b41d822af4267f171687ec8fed70a844f7_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:modalites.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 9,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* {% block body %}*/
/* */
/* */
/* */
/*     <div class="placeholders col-md-10 col-lg-10 col-md-offset-1">*/
/*         <div class="starter-template">*/
/*             <img id=logoensi" src="{{ asset('bundles/HomePlatformBundle/images/logoensi.jpg') }}" align="center" height="100" >*/
/*         <h1 class="page-header">Modalités de proposition des projets</h1>*/
/*             </div>*/
/*         <div class=" placeholder col-md-12"><p>*/
/*             <h4 class="infoMod">Vous trouverez dans cette rubrique l'ensemble des informations necessaires*/
/*                 à prendre en compte avant de proposer un projet. N'hesitez pas à nous contacter par mail*/
/*                 si vous avez des questions supplémentaires. </h4>*/
/*         </div>*/
/* */
/*         <br>*/
/*         <div class="placeholder row col-md-12">*/
/* */
/*             <div class="panel panel-default">*/
/*                 <div class="panel-heading">*/
/*                     <h3 class="panel-title">Type de prestation</h3>*/
/* */
/*                 </div>*/
/*                 <div class="panel-body">*/
/*                     Etude, prototypage, veille, recherche documentaire…*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="panel panel-default">*/
/*                 <div class="panel-heading">*/
/*                     <h3 class="panel-title">Format de la prestation</h3>*/
/* */
/*                 </div>*/
/*                 <div class="panel-body ">*/
/*                     Un groupe d'élèves ingénieurs (1, 2 ou 3 élèves) en fin de cycle (3ème année) </br>*/
/*                     Temps consacré par élève et par semaine : 1 à 2 journées</br>*/
/*                     Début : mi-septembre</br>*/
/*                     Fin : mi-février</br>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="panel panel-default">*/
/*                 <div class="panel-heading"><h3 class="panel-title">Encadrement</h3>*/
/*                 </div>*/
/* */
/*                 <div class="panel-body">*/
/*                     Un tuteur école et un tuteur en entreprise.*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="panel panel-default">*/
/*                 <div class="panel-heading"><h3 class="panel-title">Modalités pratiques</h3> <span*/
/*                             class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span></div>*/
/*                 <div class="panel-body" style="display: none;">*/
/*                     Proposition d'un sujet au plus tard le 12 septembre 2015</br>*/
/*                     Présentation aux élèves ingénieurs le 25 septembre 2015</br>*/
/*                     Affectation des projets le 1er octobre 2015 (vœux entreprises et étudiants)</br>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="panel panel-default">*/
/*                 <div class="panel-heading"><h3 class="panel-title">Coût de la prestation</h3> <span*/
/*                             class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span></div>*/
/*                 <div class="panel-body" style="display: none;">*/
/*                     La mise en œuvre d'un projet est assortie d'une convention et représente un coût hors taxes fixé*/
/*                     forfaitairement à 1 200 euros. Ce montant couvre l'ensemble de la prestation (encadrement, petit*/
/*                     consommable, entretien et renouvellement des équipements) à l'exclusion de tous les équipements ou*/
/*                     composants spéciaux et les licences d'exploitation, non disponibles à l'ENSICAEN.*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="panel panel-default">*/
/*                 <div class="panel-heading"><h3 class="panel-title">Financements possibles</h3> <span*/
/*                             class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span></div>*/
/*                 <div class="panel-body" style="display: none;">*/
/*                     OSEO, Crédit d’impôt, Prestation Technologique Réseau, Impulsion Conseil…*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="panel panel-default">*/
/*                 <div class="panel-heading"><h3 class="panel-title">Suites envisageables</h3> <span*/
/*                             class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span></div>*/
/*                 <div class="panel-body" style="display: none;">*/
/*                     Stages de longue durée d'élèves ingénieurs dans l'entreprise (5 à 6 mois),</br>*/
/*                     Embauche d'ingénieurs de l'ENSICAEN,</br>*/
/*                     Thèse de doctorat, thèse CIFRE (aide ANRT)</br>*/
/*                     Prestation de R&D,</br>*/
/*                     Contrat de recherche</br>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/* */
/*     </div>*/
/* */
/*     <script>*/
/* */
/* */
/*     </script>*/
/* */
/* */
/* {% endblock %}*/
