<?php

/* GestionProjetHomePlatformBundle:Default:viewOwnPropositions.html.twig */
class __TwigTemplate_8c12010c58b488bd1b271b188e92ef3f784ada9a0c6da3baf3439dc325345f81 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewOwnPropositions.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_72498da252f726b26bd87fca30d3d088d220775045d19fab9c31e88dc7bb734e = $this->env->getExtension("native_profiler");
        $__internal_72498da252f726b26bd87fca30d3d088d220775045d19fab9c31e88dc7bb734e->enter($__internal_72498da252f726b26bd87fca30d3d088d220775045d19fab9c31e88dc7bb734e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:viewOwnPropositions.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_72498da252f726b26bd87fca30d3d088d220775045d19fab9c31e88dc7bb734e->leave($__internal_72498da252f726b26bd87fca30d3d088d220775045d19fab9c31e88dc7bb734e_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_098a5ec7c78ab8d1348a5830e58147fdbccbf08d4541e6d8556dc976fb81b4b9 = $this->env->getExtension("native_profiler");
        $__internal_098a5ec7c78ab8d1348a5830e58147fdbccbf08d4541e6d8556dc976fb81b4b9->enter($__internal_098a5ec7c78ab8d1348a5830e58147fdbccbf08d4541e6d8556dc976fb81b4b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
        
        $__internal_098a5ec7c78ab8d1348a5830e58147fdbccbf08d4541e6d8556dc976fb81b4b9->leave($__internal_098a5ec7c78ab8d1348a5830e58147fdbccbf08d4541e6d8556dc976fb81b4b9_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_088335a5e395736bc3e0e20947ceb3670a5fe985c460ec8ff697ec71d4b73f75 = $this->env->getExtension("native_profiler");
        $__internal_088335a5e395736bc3e0e20947ceb3670a5fe985c460ec8ff697ec71d4b73f75->enter($__internal_088335a5e395736bc3e0e20947ceb3670a5fe985c460ec8ff697ec71d4b73f75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "



    <div class=\"placeholders col-md-10 col-lg-10 col-md-offset-1\">
    <div class=\"starter-template\">

        <h1 class=\"page-header\">Vos propositions de projets</h1>
</div>
        <div class=\"table-responsive\">
            <h2>Liste de projets</h2>
            <table class=\"table table-striped\">

                <tr>
                    <th> Intitulé du projet </th>
                    <th> Description </th>
                    <th> Status </th>
                    <th> Détails </th>
                </tr>


                ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listProposition"]) ? $context["listProposition"] : $this->getContext($context, "listProposition")));
        foreach ($context['_seq'] as $context["_key"] => $context["proposition"]) {
            // line 29
            echo "                    <tr>
                        <td> ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "intituleProjet", array()), "html", null, true);
            echo " </td>
                        <td> ";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "description", array()), "html", null, true);
            echo " </td>
                            ";
            // line 32
            if (($this->getAttribute($context["proposition"], "valide", array()) == 0)) {
                // line 33
                echo "                        <td> <button type=\"button\" class=\"btn btn-info\">En attente</button></td>
                        ";
            }
            // line 35
            echo "                        ";
            if (($this->getAttribute($context["proposition"], "valide", array()) == 1)) {
                // line 36
                echo "                            <td> <button type=\"button\" class=\"btn btn-success\">Validé</button></td>
                        ";
            }
            // line 38
            echo "
                        ";
            // line 39
            if (($this->getAttribute($context["proposition"], "valide", array()) == 2)) {
                // line 40
                echo "                            <td> <button type=\"button\" class=\"btn btn-danger\">Refusé</button></td>
                        ";
            }
            // line 42
            echo "
                        </td>
                        <td>
                            <a href=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_propostion_in_detail", array("id" => $this->getAttribute($context["proposition"], "id", array()))), "html", null, true);
            echo "\">
                                <button type=\"button\" class=\"btn btn-default\">Voir détails</button>
                            </a>
                        </td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['proposition'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "        </table>
        </div>
</div>

";
        
        $__internal_088335a5e395736bc3e0e20947ceb3670a5fe985c460ec8ff697ec71d4b73f75->leave($__internal_088335a5e395736bc3e0e20947ceb3670a5fe985c460ec8ff697ec71d4b73f75_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewOwnPropositions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 50,  119 => 45,  114 => 42,  110 => 40,  108 => 39,  105 => 38,  101 => 36,  98 => 35,  94 => 33,  92 => 32,  88 => 31,  84 => 30,  81 => 29,  77 => 28,  54 => 7,  48 => 6,  35 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* */
/* */
/* */
/*     <div class="placeholders col-md-10 col-lg-10 col-md-offset-1">*/
/*     <div class="starter-template">*/
/* */
/*         <h1 class="page-header">Vos propositions de projets</h1>*/
/* </div>*/
/*         <div class="table-responsive">*/
/*             <h2>Liste de projets</h2>*/
/*             <table class="table table-striped">*/
/* */
/*                 <tr>*/
/*                     <th> Intitulé du projet </th>*/
/*                     <th> Description </th>*/
/*                     <th> Status </th>*/
/*                     <th> Détails </th>*/
/*                 </tr>*/
/* */
/* */
/*                 {% for proposition in listProposition %}*/
/*                     <tr>*/
/*                         <td> {{ proposition.intituleProjet }} </td>*/
/*                         <td> {{ proposition.description }} </td>*/
/*                             {% if  proposition.valide  == 0 %}*/
/*                         <td> <button type="button" class="btn btn-info">En attente</button></td>*/
/*                         {% endif %}*/
/*                         {% if proposition.valide  == 1 %}*/
/*                             <td> <button type="button" class="btn btn-success">Validé</button></td>*/
/*                         {% endif %}*/
/* */
/*                         {% if proposition.valide  == 2 %}*/
/*                             <td> <button type="button" class="btn btn-danger">Refusé</button></td>*/
/*                         {% endif %}*/
/* */
/*                         </td>*/
/*                         <td>*/
/*                             <a href="{{ path('gestion_projet_home_platform_propostion_in_detail', {'id': proposition.id}) }}">*/
/*                                 <button type="button" class="btn btn-default">Voir détails</button>*/
/*                             </a>*/
/*                         </td>*/
/*                     </tr>*/
/*                 {% endfor %}        </table>*/
/*         </div>*/
/* </div>*/
/* */
/* {% endblock %}*/
