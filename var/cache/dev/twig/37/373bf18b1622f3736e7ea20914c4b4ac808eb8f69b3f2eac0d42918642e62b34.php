<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_08c1fa85d3cff7f9d0bcdcf6808e63dce2abaee8945e760850bca34b5d6812ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b9993d54b7b00a715693ee3f9233e15b4a2b1b70a8aeb36f6c1f814e37eb2132 = $this->env->getExtension("native_profiler");
        $__internal_b9993d54b7b00a715693ee3f9233e15b4a2b1b70a8aeb36f6c1f814e37eb2132->enter($__internal_b9993d54b7b00a715693ee3f9233e15b4a2b1b70a8aeb36f6c1f814e37eb2132_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        // line 1
        echo "<!doctype html>
<html lang=\"en\" class=\"no-js\">
<head>
\t<meta charset=\"UTF-8\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

\t<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,700,400' rel='stylesheet' type='text/css'>

\t<link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/css/reset.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/css/style.css"), "html", null, true);
        echo "\">
\t
\t<script src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/js/modernizr.js"), "html", null, true);
        echo "\"  ></script> <!-- Modernizr -->

\t<title>ENSICAEN - Proposition  de projet</title>
</head>
<body>
<section class=\"cd-intro\">
\t<div class=\"cd-intro-content mask\">

\t\t<h1 data-content=\"Formulaire de connexion\"><span>Formulaire de connexion</span></h1>
\t\t<div class=\"action-wrapper\">
\t\t\t";
        // line 23
        echo "
\t\t\t";
        // line 24
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 76
        echo "\t\t</div>
\t</div>
</section>

<script src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/js/jquery-2.1.4.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/js/js/main.js"), "html", null, true);
        echo "\"></script> <!-- Resource jQuery -->
</body>
</html>


";
        
        $__internal_b9993d54b7b00a715693ee3f9233e15b4a2b1b70a8aeb36f6c1f814e37eb2132->leave($__internal_b9993d54b7b00a715693ee3f9233e15b4a2b1b70a8aeb36f6c1f814e37eb2132_prof);

    }

    // line 24
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b9bb3d45f5f52d1144c3c83f833b0023a533680659ed471006c6edacc4de8ac9 = $this->env->getExtension("native_profiler");
        $__internal_b9bb3d45f5f52d1144c3c83f833b0023a533680659ed471006c6edacc4de8ac9->enter($__internal_b9bb3d45f5f52d1144c3c83f833b0023a533680659ed471006c6edacc4de8ac9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 25
        echo "\t\t\t\t";
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 26
            echo "\t\t\t\t\t<div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
\t\t\t\t";
        }
        // line 28
        echo "
\t\t\t\t<div class=\"container col-md-offset-3\">





\t\t\t\t\t<form action=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\" class=\"form-horizontal\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />

\t\t\t\t\t\t<div class=\"form-group\">

\t\t\t\t\t\t\t<div class=\"col-sm-10 col-md-4\">
\t\t\t\t\t\t\t\t<input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" class=\"form-control\" placeholder=\"Login\"/>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"form-group col-\">

\t\t\t\t\t\t\t<div class=\"col-sm-10 col-md-4\">
\t\t\t\t\t\t\t\t<input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" class=\"form-control\" placeholder=\"Password\"/>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"form-group\">


\t\t\t\t\t\t\t<div class=\"col-sm-10 col-md-\">
\t\t\t\t\t\t\t\t<label for=\"remember_me\" class=\"col-sm-2 control-label\">";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.remember_me", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"col-sm-offset-3 col-sm-8\">
\t\t\t\t\t\t\t\t<input type=\"submit\" id=\"_submit\" name=\"_submit\"  value=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"whiteForm\"href=\"";
        // line 66
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_request");
        echo "\">Forgot password ?</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</form>




\t\t\t\t</div>

\t\t\t";
        
        $__internal_b9bb3d45f5f52d1144c3c83f833b0023a533680659ed471006c6edacc4de8ac9->leave($__internal_b9bb3d45f5f52d1144c3c83f833b0023a533680659ed471006c6edacc4de8ac9_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 66,  147 => 63,  137 => 56,  119 => 41,  111 => 36,  107 => 35,  98 => 28,  92 => 26,  89 => 25,  83 => 24,  70 => 81,  66 => 80,  60 => 76,  58 => 24,  55 => 23,  42 => 12,  37 => 10,  33 => 9,  23 => 1,);
    }
}
/* <!doctype html>*/
/* <html lang="en" class="no-js">*/
/* <head>*/
/* 	<meta charset="UTF-8">*/
/* 	<meta name="viewport" content="width=device-width, initial-scale=1">*/
/* */
/* 	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,700,400' rel='stylesheet' type='text/css'>*/
/* */
/* 	<link rel="stylesheet" href="{{ asset('bundles/HomePlatformBundle/css/reset.css')}}">*/
/* 	<link rel="stylesheet" href="{{ asset('bundles/HomePlatformBundle/css/style.css')}}">*/
/* 	*/
/* 	<script src="{{ asset('bundles/HomePlatformBundle/js/modernizr.js')}}"  ></script> <!-- Modernizr -->*/
/* */
/* 	<title>ENSICAEN - Proposition  de projet</title>*/
/* </head>*/
/* <body>*/
/* <section class="cd-intro">*/
/* 	<div class="cd-intro-content mask">*/
/* */
/* 		<h1 data-content="Formulaire de connexion"><span>Formulaire de connexion</span></h1>*/
/* 		<div class="action-wrapper">*/
/* 			{% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* 			{% block fos_user_content %}*/
/* 				{% if error %}*/
/* 					<div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>*/
/* 				{% endif %}*/
/* */
/* 				<div class="container col-md-offset-3">*/
/* */
/* */
/* */
/* */
/* */
/* 					<form action="{{ path("fos_user_security_check") }}" method="post" class="form-horizontal">*/
/* 						<input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/* */
/* 						<div class="form-group">*/
/* */
/* 							<div class="col-sm-10 col-md-4">*/
/* 								<input type="text" id="username" name="_username" value="{{ last_username }}" required="required" class="form-control" placeholder="Login"/>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 						<div class="form-group col-">*/
/* */
/* 							<div class="col-sm-10 col-md-4">*/
/* 								<input type="password" id="password" name="_password" required="required" class="form-control" placeholder="Password"/>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 						<div class="form-group">*/
/* */
/* */
/* 							<div class="col-sm-10 col-md-">*/
/* 								<label for="remember_me" class="col-sm-2 control-label">{{ 'security.login.remember_me'|trans }}</label>*/
/* 								<input type="checkbox" id="remember_me" name="_remember_me" value="on" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 						<div class="form-group">*/
/* 							<div class="col-sm-offset-3 col-sm-8">*/
/* 								<input type="submit" id="_submit" name="_submit"  value="{{ 'security.login.submit'|trans }}" />*/
/* */
/* 							</div>*/
/* 							<a class="whiteForm"href="{{ path('fos_user_resetting_request') }}">Forgot password ?</a>*/
/* 						</div>*/
/* 					</form>*/
/* */
/* */
/* */
/* */
/* 				</div>*/
/* */
/* 			{% endblock fos_user_content %}*/
/* 		</div>*/
/* 	</div>*/
/* </section>*/
/* */
/* <script src="{{ asset('bundles/HomePlatformBundle/js/jquery-2.1.4.js')}}"></script>*/
/* <script src="{{ asset('bundles/HomePlatformBundle/js/js/main.js')}}"></script> <!-- Resource jQuery -->*/
/* </body>*/
/* </html>*/
/* */
/* */
/* */
