<?php

/* GestionProjetHomePlatformBundle:Default:viewAsking.html.twig */
class __TwigTemplate_5691cb371a9d5a8ce3d736bb41247711aad2034df8947d3fd7bbead290b79d19 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewAsking.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c4f1a6b5f77d4cfe16666205bae046ec6656d5d1471d48c70fdcd3fa3fd4eff = $this->env->getExtension("native_profiler");
        $__internal_3c4f1a6b5f77d4cfe16666205bae046ec6656d5d1471d48c70fdcd3fa3fd4eff->enter($__internal_3c4f1a6b5f77d4cfe16666205bae046ec6656d5d1471d48c70fdcd3fa3fd4eff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:viewAsking.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3c4f1a6b5f77d4cfe16666205bae046ec6656d5d1471d48c70fdcd3fa3fd4eff->leave($__internal_3c4f1a6b5f77d4cfe16666205bae046ec6656d5d1471d48c70fdcd3fa3fd4eff_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_f63ffeb113a81498bec1de8b678f6b8c5f7adc1f2e6839c7d3310abbdfe73490 = $this->env->getExtension("native_profiler");
        $__internal_f63ffeb113a81498bec1de8b678f6b8c5f7adc1f2e6839c7d3310abbdfe73490->enter($__internal_f63ffeb113a81498bec1de8b678f6b8c5f7adc1f2e6839c7d3310abbdfe73490_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
        
        $__internal_f63ffeb113a81498bec1de8b678f6b8c5f7adc1f2e6839c7d3310abbdfe73490->leave($__internal_f63ffeb113a81498bec1de8b678f6b8c5f7adc1f2e6839c7d3310abbdfe73490_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_aac8d979f120502392a043f760d9392065e951fb57b8e787e97b38f9d80203c9 = $this->env->getExtension("native_profiler");
        $__internal_aac8d979f120502392a043f760d9392065e951fb57b8e787e97b38f9d80203c9->enter($__internal_aac8d979f120502392a043f760d9392065e951fb57b8e787e97b38f9d80203c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "

    <div class=\"placeholders col-md-10 col-lg-10 col-md-offset-1\">
    <div class=\"starter-template\">
    <div class=\"table-responsive\">
        <h1>Liste des demandes de création de compte</h1>
        </div>
        <table class=\"table table-striped\">

            <tr>
                <th> Entrerprise</th>
                <th> Nom </th>
                <th> Prénom </th>
                <th> Créer le compte</th>
            </tr>


            ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listAsking"]) ? $context["listAsking"] : $this->getContext($context, "listAsking")));
        foreach ($context['_seq'] as $context["_key"] => $context["account"]) {
            // line 25
            echo "                <tr>
                    <td> ";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["account"], "entreprise", array()), "html", null, true);
            echo " </td>
                    <td> ";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["account"], "nom", array()), "html", null, true);
            echo " </td>
                    <td> ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["account"], "prenom", array()), "html", null, true);
            echo " </td>
                    <td><a href=\"";
            // line 29
            echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_add_account");
            echo "\">Créer le compte</a>  </td>


                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['account'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "
        </table>
    </div>
    </div>
";
        
        $__internal_aac8d979f120502392a043f760d9392065e951fb57b8e787e97b38f9d80203c9->leave($__internal_aac8d979f120502392a043f760d9392065e951fb57b8e787e97b38f9d80203c9_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewAsking.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 34,  92 => 29,  88 => 28,  84 => 27,  80 => 26,  77 => 25,  73 => 24,  54 => 7,  48 => 6,  35 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* */
/*     <div class="placeholders col-md-10 col-lg-10 col-md-offset-1">*/
/*     <div class="starter-template">*/
/*     <div class="table-responsive">*/
/*         <h1>Liste des demandes de création de compte</h1>*/
/*         </div>*/
/*         <table class="table table-striped">*/
/* */
/*             <tr>*/
/*                 <th> Entrerprise</th>*/
/*                 <th> Nom </th>*/
/*                 <th> Prénom </th>*/
/*                 <th> Créer le compte</th>*/
/*             </tr>*/
/* */
/* */
/*             {% for account in listAsking %}*/
/*                 <tr>*/
/*                     <td> {{ account.entreprise }} </td>*/
/*                     <td> {{ account.nom}} </td>*/
/*                     <td> {{ account.prenom}} </td>*/
/*                     <td><a href="{{ path('gestion_projet_home_platform_add_account') }}">Créer le compte</a>  </td>*/
/* */
/* */
/*                 </tr>*/
/*             {% endfor %}*/
/* */
/*         </table>*/
/*     </div>*/
/*     </div>*/
/* {% endblock %}*/
