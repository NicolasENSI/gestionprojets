<?php

/* @GestionProjetHomePlatform/Emails/acceptation.html.twig */
class __TwigTemplate_4f0c33a6fcdaf9f254376e56881cc64826bc4f4d782623f5e950b7491a35abf2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_890c304a208000c2eed273dd81621cc36a0a3518f2bad1a1940fc51b4521d9ab = $this->env->getExtension("native_profiler");
        $__internal_890c304a208000c2eed273dd81621cc36a0a3518f2bad1a1940fc51b4521d9ab->enter($__internal_890c304a208000c2eed273dd81621cc36a0a3518f2bad1a1940fc51b4521d9ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@GestionProjetHomePlatform/Emails/acceptation.html.twig"));

        // line 2
        echo "<h3>Félicitation ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo ", votre projet \"";
        echo twig_escape_filter($this->env, (isset($context["titre"]) ? $context["titre"] : $this->getContext($context, "titre")), "html", null, true);
        echo "\"est validé</h3>

";
        // line 5
        echo "<h4> Nous allons vous recontacter pour plus de détails..</h4>

<h4>En attendant vous pouvez aller voir l'état de vos autres projets <a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_view_own_propositions");
        echo "\"> ici </a> </h4>

<h4>Vous pouvez également proposer d'autres projet <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_propositionPage");
        echo "\"> ici </a> </h4>";
        
        $__internal_890c304a208000c2eed273dd81621cc36a0a3518f2bad1a1940fc51b4521d9ab->leave($__internal_890c304a208000c2eed273dd81621cc36a0a3518f2bad1a1940fc51b4521d9ab_prof);

    }

    public function getTemplateName()
    {
        return "@GestionProjetHomePlatform/Emails/acceptation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 9,  34 => 7,  30 => 5,  22 => 2,);
    }
}
/* {# app/Resources/views/Emails/registration.html.twig #}*/
/* <h3>Félicitation {{ name }}, votre projet "{{ titre }}"est validé</h3>*/
/* */
/* {# example, assuming you have a route named "login" #}*/
/* <h4> Nous allons vous recontacter pour plus de détails..</h4>*/
/* */
/* <h4>En attendant vous pouvez aller voir l'état de vos autres projets <a href="{{ path('gestion_projet_home_platform_view_own_propositions') }}"> ici </a> </h4>*/
/* */
/* <h4>Vous pouvez également proposer d'autres projet <a href="{{ path('gestion_projet_home_platform_propositionPage') }}"> ici </a> </h4>*/
