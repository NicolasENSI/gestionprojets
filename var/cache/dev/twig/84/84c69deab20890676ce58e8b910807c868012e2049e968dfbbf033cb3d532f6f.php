<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_b066f5acde3508da369ea40feba7328482fb8e1df9ee4c2c7bc66e0419c3552d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_94d4dae7bf38f67b9fd37e2efe0388d82d5740245c196af946276f3a8ec2c35a = $this->env->getExtension("native_profiler");
        $__internal_94d4dae7bf38f67b9fd37e2efe0388d82d5740245c196af946276f3a8ec2c35a->enter($__internal_94d4dae7bf38f67b9fd37e2efe0388d82d5740245c196af946276f3a8ec2c35a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_94d4dae7bf38f67b9fd37e2efe0388d82d5740245c196af946276f3a8ec2c35a->leave($__internal_94d4dae7bf38f67b9fd37e2efe0388d82d5740245c196af946276f3a8ec2c35a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_0591e429d4d7cf130b2b78242b0d00c9db4bdddf61f7d9e395e93914a034d563 = $this->env->getExtension("native_profiler");
        $__internal_0591e429d4d7cf130b2b78242b0d00c9db4bdddf61f7d9e395e93914a034d563->enter($__internal_0591e429d4d7cf130b2b78242b0d00c9db4bdddf61f7d9e395e93914a034d563_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_0591e429d4d7cf130b2b78242b0d00c9db4bdddf61f7d9e395e93914a034d563->leave($__internal_0591e429d4d7cf130b2b78242b0d00c9db4bdddf61f7d9e395e93914a034d563_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:request_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
