<?php

/* GestionProjetHomePlatformBundle:Default:success.html.twig */
class __TwigTemplate_203372c6c24bec1f66d6a1da19fe4a6b04445dc57b546b112899a067987d83de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ceb03fe4d5f5afb5d57b80df15f0f8260d19b62c3eef12cbc32390d9ddddc3b1 = $this->env->getExtension("native_profiler");
        $__internal_ceb03fe4d5f5afb5d57b80df15f0f8260d19b62c3eef12cbc32390d9ddddc3b1->enter($__internal_ceb03fe4d5f5afb5d57b80df15f0f8260d19b62c3eef12cbc32390d9ddddc3b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "GestionProjetHomePlatformBundle:Default:success.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
   <!-- HTML meta refresh URL redirection -->
   <meta http-equiv=\"refresh\"
   content=\"4; url=";
        // line 6
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_homepage");
        echo "\">
</head>
<body>
   Félicitation votre proposition de stage a bien été envoyée. Vous allez être redirigé vers l'acceuil dans 3 secondes.
</body>
</html>";
        
        $__internal_ceb03fe4d5f5afb5d57b80df15f0f8260d19b62c3eef12cbc32390d9ddddc3b1->leave($__internal_ceb03fe4d5f5afb5d57b80df15f0f8260d19b62c3eef12cbc32390d9ddddc3b1_prof);

    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 6,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/*    <!-- HTML meta refresh URL redirection -->*/
/*    <meta http-equiv="refresh"*/
/*    content="4; url={{ path('gestion_projet_home_platform_homepage') }}">*/
/* </head>*/
/* <body>*/
/*    Félicitation votre proposition de stage a bien été envoyée. Vous allez être redirigé vers l'acceuil dans 3 secondes.*/
/* </body>*/
/* </html>*/
