<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appProdUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = array(
        'gestion_projet_home_platform_homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::homeAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/home',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_homepage_promo' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::homePromoAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/home/promo',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_adminpage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::adminAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_askingpage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::askAccountAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/home/askAccount',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_propositionPage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::propositionAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/home/proposition',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_modalites' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::modalitesAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/home/modalites',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_projet_sent_success' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::successAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/home/proposition/success',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_view_propositions' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::viewPropositionsAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/viewPropositions',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_validate' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::validateAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/viewValidatePropositions',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_pending' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::pendingAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/viewPendingPropositions',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_notValidate' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::notValidateAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/viewNotValidatePropositions',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_add_account' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::addAccountAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/addAccount',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_view_account' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::viewAccountAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/viewAccount',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_view_ask' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::viewAskingAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/viewAsk',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_propostion_in_detail' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::propositionInDetailAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/detail',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_validation_project' => array (  0 =>   array (    0 => 'id',    1 => 'state',  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::validationAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'state',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    2 =>     array (      0 => 'text',      1 => '/admin/validation',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_propositions_validate_in_detail' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::propositionsValidateInDetailAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/viewDetailValidatePropositions',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_view_own_propositions' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::viewOwnPropositionsAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/home/viewOwnPropositions',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'gestion_projet_home_platform_delete_proposition' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::deletePropositionAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/home/deleteProposition',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_security_login' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/login',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_security_check' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/login_check',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_security_logout' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/logout',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_profile_show' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/profile/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_profile_edit' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/profile/edit',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_registration_register' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/register/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_registration_check_email' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/register/check-email',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_registration_confirm' => array (  0 =>   array (    0 => 'token',  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'token',    ),    1 =>     array (      0 => 'text',      1 => '/register/confirm',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_registration_confirmed' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/register/confirmed',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_resetting_request' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/resetting/request',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_resetting_send_email' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/resetting/send-email',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_resetting_check_email' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/resetting/check-email',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_resetting_reset' => array (  0 =>   array (    0 => 'token',  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'token',    ),    1 =>     array (      0 => 'text',      1 => '/resetting/reset',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'fos_user_change_password' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/profile/change-password',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    );
        }
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
