<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/home')) {
            // gestion_projet_home_platform_homepage
            if ($pathinfo === '/home') {
                return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::homeAction',  '_route' => 'gestion_projet_home_platform_homepage',);
            }

            // gestion_projet_home_platform_homepage_promo
            if ($pathinfo === '/home/promo') {
                return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::homePromoAction',  '_route' => 'gestion_projet_home_platform_homepage_promo',);
            }

        }

        // gestion_projet_home_platform_adminpage
        if ($pathinfo === '/admin') {
            return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::adminAction',  '_route' => 'gestion_projet_home_platform_adminpage',);
        }

        if (0 === strpos($pathinfo, '/home')) {
            // gestion_projet_home_platform_askingpage
            if ($pathinfo === '/home/askAccount') {
                return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::askAccountAction',  '_route' => 'gestion_projet_home_platform_askingpage',);
            }

            // gestion_projet_home_platform_propositionPage
            if ($pathinfo === '/home/proposition') {
                return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::propositionAction',  '_route' => 'gestion_projet_home_platform_propositionPage',);
            }

            // gestion_projet_home_platform_modalites
            if ($pathinfo === '/home/modalites') {
                return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::modalitesAction',  '_route' => 'gestion_projet_home_platform_modalites',);
            }

            // gestion_projet_home_platform_projet_sent_success
            if ($pathinfo === '/home/proposition/success') {
                return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::successAction',  '_route' => 'gestion_projet_home_platform_projet_sent_success',);
            }

        }

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/view')) {
                // gestion_projet_home_platform_view_propositions
                if ($pathinfo === '/admin/viewPropositions') {
                    return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::viewPropositionsAction',  '_route' => 'gestion_projet_home_platform_view_propositions',);
                }

                // gestion_projet_home_platform_validate
                if ($pathinfo === '/admin/viewValidatePropositions') {
                    return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::validateAction',  '_route' => 'gestion_projet_home_platform_validate',);
                }

                // gestion_projet_home_platform_pending
                if ($pathinfo === '/admin/viewPendingPropositions') {
                    return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::pendingAction',  '_route' => 'gestion_projet_home_platform_pending',);
                }

                // gestion_projet_home_platform_notValidate
                if ($pathinfo === '/admin/viewNotValidatePropositions') {
                    return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::notValidateAction',  '_route' => 'gestion_projet_home_platform_notValidate',);
                }

            }

            // gestion_projet_home_platform_add_account
            if ($pathinfo === '/admin/addAccount') {
                return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::addAccountAction',  '_route' => 'gestion_projet_home_platform_add_account',);
            }

            if (0 === strpos($pathinfo, '/admin/viewA')) {
                // gestion_projet_home_platform_view_account
                if ($pathinfo === '/admin/viewAccount') {
                    return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::viewAccountAction',  '_route' => 'gestion_projet_home_platform_view_account',);
                }

                // gestion_projet_home_platform_view_ask
                if ($pathinfo === '/admin/viewAsk') {
                    return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::viewAskingAction',  '_route' => 'gestion_projet_home_platform_view_ask',);
                }

            }

        }

        // gestion_projet_home_platform_propostion_in_detail
        if (0 === strpos($pathinfo, '/detail') && preg_match('#^/detail/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'gestion_projet_home_platform_propostion_in_detail')), array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::propositionInDetailAction',));
        }

        if (0 === strpos($pathinfo, '/admin/v')) {
            // gestion_projet_home_platform_validation_project
            if (0 === strpos($pathinfo, '/admin/validation') && preg_match('#^/admin/validation/(?P<id>[^/]++)/(?P<state>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'gestion_projet_home_platform_validation_project')), array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::validationAction',));
            }

            // gestion_projet_home_platform_propositions_validate_in_detail
            if ($pathinfo === '/admin/viewDetailValidatePropositions') {
                return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::propositionsValidateInDetailAction',  '_route' => 'gestion_projet_home_platform_propositions_validate_in_detail',);
            }

        }

        if (0 === strpos($pathinfo, '/home')) {
            // gestion_projet_home_platform_view_own_propositions
            if ($pathinfo === '/home/viewOwnPropositions') {
                return array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::viewOwnPropositionsAction',  '_route' => 'gestion_projet_home_platform_view_own_propositions',);
            }

            // gestion_projet_home_platform_delete_proposition
            if (0 === strpos($pathinfo, '/home/deleteProposition') && preg_match('#^/home/deleteProposition/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'gestion_projet_home_platform_delete_proposition')), array (  '_controller' => 'GestionProjet\\HomePlatformBundle\\Controller\\DefaultController::deletePropositionAction',));
            }

        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
