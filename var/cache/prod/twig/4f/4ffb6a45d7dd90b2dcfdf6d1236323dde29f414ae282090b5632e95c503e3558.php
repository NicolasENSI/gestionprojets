<?php

/* GestionProjetHomePlatformBundle:Default:viewOwnPropositions.html.twig */
class __TwigTemplate_8cac4a962acd15c8fd593b5d5aba6129bdd8379fa52ca8abade519a6c3f97a54 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewOwnPropositions.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "




    <div class=\"col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main\">
        <h1 class=\"page-header\">Vos propositions de projets</h1>

        <div class=\"table-responsive\">
            <h2>Liste de projets</h2>
            <table class=\"table table-striped\">

                <tr>
                    <th> Intitulé du projet </th>
                    <th> Description </th>
                    <th> Status </th>
                    <th> Détails </th>
                </tr>


                ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listProposition"]) ? $context["listProposition"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["proposition"]) {
            // line 28
            echo "                    <tr>
                        <td> ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "intituleProjet", array()), "html", null, true);
            echo " </td>
                        <td> ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "description", array()), "html", null, true);
            echo " </td>
                            ";
            // line 31
            if (($this->getAttribute($context["proposition"], "valide", array()) == 0)) {
                // line 32
                echo "                        <td> <button type=\"button\" class=\"btn btn-info\">En attente</button></td>
                        ";
            }
            // line 34
            echo "                        ";
            if (($this->getAttribute($context["proposition"], "valide", array()) == 1)) {
                // line 35
                echo "                            <td> <button type=\"button\" class=\"btn btn-success\">Validé</button></td>
                        ";
            }
            // line 37
            echo "
                        ";
            // line 38
            if (($this->getAttribute($context["proposition"], "valide", array()) == 2)) {
                // line 39
                echo "                            <td> <button type=\"button\" class=\"btn btn-danger\">Refusé</button></td>
                        ";
            }
            // line 41
            echo "
                        </td>
                        <td>
                            <a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_propostion_in_detail", array("id" => $this->getAttribute($context["proposition"], "id", array()))), "html", null, true);
            echo "\">
                                <button type=\"button\" class=\"btn btn-default\">Voir détails</button>
                            </a>
                        </td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['proposition'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "        </table>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewOwnPropositions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 49,  103 => 44,  98 => 41,  94 => 39,  92 => 38,  89 => 37,  85 => 35,  82 => 34,  78 => 32,  76 => 31,  72 => 30,  68 => 29,  65 => 28,  61 => 27,  39 => 7,  36 => 6,  29 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* */
/* */
/* */
/* */
/*     <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">*/
/*         <h1 class="page-header">Vos propositions de projets</h1>*/
/* */
/*         <div class="table-responsive">*/
/*             <h2>Liste de projets</h2>*/
/*             <table class="table table-striped">*/
/* */
/*                 <tr>*/
/*                     <th> Intitulé du projet </th>*/
/*                     <th> Description </th>*/
/*                     <th> Status </th>*/
/*                     <th> Détails </th>*/
/*                 </tr>*/
/* */
/* */
/*                 {% for proposition in listProposition %}*/
/*                     <tr>*/
/*                         <td> {{ proposition.intituleProjet }} </td>*/
/*                         <td> {{ proposition.description }} </td>*/
/*                             {% if  proposition.valide  == 0 %}*/
/*                         <td> <button type="button" class="btn btn-info">En attente</button></td>*/
/*                         {% endif %}*/
/*                         {% if proposition.valide  == 1 %}*/
/*                             <td> <button type="button" class="btn btn-success">Validé</button></td>*/
/*                         {% endif %}*/
/* */
/*                         {% if proposition.valide  == 2 %}*/
/*                             <td> <button type="button" class="btn btn-danger">Refusé</button></td>*/
/*                         {% endif %}*/
/* */
/*                         </td>*/
/*                         <td>*/
/*                             <a href="{{ path('gestion_projet_home_platform_propostion_in_detail', {'id': proposition.id}) }}">*/
/*                                 <button type="button" class="btn btn-default">Voir détails</button>*/
/*                             </a>*/
/*                         </td>*/
/*                     </tr>*/
/*                 {% endfor %}        </table>*/
/*         </div>*/
/*     </div>*/
/* */
/* {% endblock %}*/
