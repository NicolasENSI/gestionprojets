<?php

/* GestionProjetHomePlatformBundle:Default:viewDetailProposition.html.twig */
class __TwigTemplate_a022e98b39bdbfaf3231ea6dce82367922033f07aea3692dfbaac3e09143a085 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewDetailProposition.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "
 <div class=\"col-sm-9 col-sm-offset-4 col-md-10 col-md-offset-2 main\">

     <h1 class=\"page-header\">Le projet en détail : </h1>
     <h3> Intitulé : ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "intituleProjet", array()), "html", null, true);
        echo "</h3>
     <h3> Type de projet : ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "mission", array()), "html", null, true);
        echo "</h3>
     <h3> Entreprise : ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "entreprise", array()), "html", null, true);
        echo "</h3>
     <h3> Nom : ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "nom", array()), "html", null, true);
        echo " </h3>
     <h3> Prenom : ";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "prenom", array()), "html", null, true);
        echo " </h3>
    ";
        // line 17
        if ($this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "brochure", array())) {
            // line 18
            echo "     <h3> Sujet en détail : <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/brochures/" . $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "brochure", array()))), "html", null, true);
            echo "\">Voir détail (PDF)</a> </h3>
     ";
        }
        // line 20
        echo "     <h3> Resumé : </h3>
     <h4>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "description", array()), "html", null, true);
        echo "</h4>



     ";
        // line 25
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 26
            echo "
     ";
            // line 27
            if (($this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "valide", array()) == 0)) {
                // line 28
                echo "        <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_validation_project", array("id" => $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "id", array()), "state" => 1)), "html", null, true);
                echo "\">
            <button type=\"button\" class=\"btn btn-success\">Valider le projet</button>
        </a>
        <button type=\"button\" class=\"btn btn-default\">Contacter</button>
     <a href=\"";
                // line 32
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_validation_project", array("id" => $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "id", array()), "state" => 2)), "html", null, true);
                echo "\">
        <button type=\"button\" class=\"btn btn-danger\">Refuser le projet</button>
         </a>
     ";
            }
            // line 36
            echo "
     ";
            // line 37
            if (($this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "valide", array()) == 1)) {
                // line 38
                echo "     <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_validation_project", array("id" => $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "id", array()), "state" => 2)), "html", null, true);
                echo "\">
     <button type=\"button\" class=\"btn btn-danger\"> Dévalider le projet </button>
         </a>
     <button type=\"button\" class=\"btn btn-default\">Contacter</button>
     ";
            }
            // line 43
            echo "


     ";
            // line 46
            if (($this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "valide", array()) == 2)) {
                // line 47
                echo "     <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_validation_project", array("id" => $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "id", array()), "state" => 1)), "html", null, true);
                echo "\">
     <button type=\"button\" class=\"btn btn-danger\"> Valider quand même le projet </button>
         </a>
     <button type=\"button\" class=\"btn btn-default\">Contacter</button>
     ";
            }
            // line 52
            echo "
     ";
        }
        // line 54
        echo "
     ";
        // line 55
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            // line 56
            echo "     <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_delete_proposition", array("id" => $this->getAttribute((isset($context["proposition"]) ? $context["proposition"] : null), "id", array()))), "html", null, true);
            echo "\">
         <button type=\"button\" class=\"btn btn-danger\"> Supprimer cette proposition </button>
     </a>

         </div>
     ";
        }
        // line 62
        echo "



";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewDetailProposition.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 62,  145 => 56,  143 => 55,  140 => 54,  136 => 52,  127 => 47,  125 => 46,  120 => 43,  111 => 38,  109 => 37,  106 => 36,  99 => 32,  91 => 28,  89 => 27,  86 => 26,  84 => 25,  77 => 21,  74 => 20,  68 => 18,  66 => 17,  62 => 16,  58 => 15,  54 => 14,  50 => 13,  46 => 12,  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends 'GestionProjetHomePlatformBundle::layout.html.twig' %}*/
/* */
/* {% block title %}*/
/* */
/* {% endblock %}*/
/* */
/* {% block body %}*/
/* */
/*  <div class="col-sm-9 col-sm-offset-4 col-md-10 col-md-offset-2 main">*/
/* */
/*      <h1 class="page-header">Le projet en détail : </h1>*/
/*      <h3> Intitulé : {{ proposition.intituleProjet }}</h3>*/
/*      <h3> Type de projet : {{ proposition.mission }}</h3>*/
/*      <h3> Entreprise : {{ proposition.entreprise }}</h3>*/
/*      <h3> Nom : {{ proposition.nom }} </h3>*/
/*      <h3> Prenom : {{ proposition.prenom }} </h3>*/
/*     {% if proposition.brochure %}*/
/*      <h3> Sujet en détail : <a href="{{ asset('uploads/brochures/' ~ proposition.brochure) }}">Voir détail (PDF)</a> </h3>*/
/*      {% endif %}*/
/*      <h3> Resumé : </h3>*/
/*      <h4>{{ proposition.description }}</h4>*/
/* */
/* */
/* */
/*      {% if is_granted('ROLE_ADMIN') %}*/
/* */
/*      {% if proposition.valide == 0 %}*/
/*         <a href="{{ path('gestion_projet_home_platform_validation_project', {'id': proposition.id, 'state' : 1}) }}">*/
/*             <button type="button" class="btn btn-success">Valider le projet</button>*/
/*         </a>*/
/*         <button type="button" class="btn btn-default">Contacter</button>*/
/*      <a href="{{ path('gestion_projet_home_platform_validation_project', {'id': proposition.id, 'state' : 2}) }}">*/
/*         <button type="button" class="btn btn-danger">Refuser le projet</button>*/
/*          </a>*/
/*      {% endif %}*/
/* */
/*      {% if proposition.valide == 1 %}*/
/*      <a href="{{ path('gestion_projet_home_platform_validation_project', {'id': proposition.id, 'state' : 2}) }}">*/
/*      <button type="button" class="btn btn-danger"> Dévalider le projet </button>*/
/*          </a>*/
/*      <button type="button" class="btn btn-default">Contacter</button>*/
/*      {% endif %}*/
/* */
/* */
/* */
/*      {% if proposition.valide == 2 %}*/
/*      <a href="{{ path('gestion_projet_home_platform_validation_project', {'id': proposition.id, 'state' : 1}) }}">*/
/*      <button type="button" class="btn btn-danger"> Valider quand même le projet </button>*/
/*          </a>*/
/*      <button type="button" class="btn btn-default">Contacter</button>*/
/*      {% endif %}*/
/* */
/*      {% endif %}*/
/* */
/*      {% if is_granted('ROLE_USER') %}*/
/*      <a href="{{ path('gestion_projet_home_platform_delete_proposition', {'id': proposition.id}) }}">*/
/*          <button type="button" class="btn btn-danger"> Supprimer cette proposition </button>*/
/*      </a>*/
/* */
/*          </div>*/
/*      {% endif %}*/
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
