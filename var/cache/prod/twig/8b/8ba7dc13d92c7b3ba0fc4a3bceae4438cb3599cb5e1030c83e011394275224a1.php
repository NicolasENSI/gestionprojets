<?php

/* GestionProjetHomePlatformBundle:Exception:error.html.twig */
class __TwigTemplate_f59eb2160d1027d0490abfc61ac7850f7d6007e7ce023bd52c9d46b1452bd301 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@GestionProjetHomePlatform/layout.html.twig", "GestionProjetHomePlatformBundle:Exception:error.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@GestionProjetHomePlatform/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <h1>Page not found</h1>



    <p>
        The requested page couldn't be located. Checkout for any URL
        misspelling or <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\">return to the homepage</a>.
    </p>
";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Exception:error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 10,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '@GestionProjetHomePlatform/layout.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <h1>Page not found</h1>*/
/* */
/* */
/* */
/*     <p>*/
/*         The requested page couldn't be located. Checkout for any URL*/
/*         misspelling or <a href="{{ path('homepage') }}">return to the homepage</a>.*/
/*     </p>*/
/* {% endblock %}*/
