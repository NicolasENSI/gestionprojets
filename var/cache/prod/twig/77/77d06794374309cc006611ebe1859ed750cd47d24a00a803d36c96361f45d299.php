<?php

/* GestionProjetHomePlatformBundle:Default:index.html.twig */
class __TwigTemplate_9bcba1b224339eb86c9ca1b772f304c621e08b2f1c0dc96585ce172485d0b7d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "
  <div class=\"container  col-md-12 col-lg-12\">

      <div class=\"starter-template\">
        <h1>Proposer un projet à l'ENSICAEN en <span style=\"background-color: #2b669a; color: white\"> 3  étapes </span></h1>

      </div>

      <hr class=\"featurette-divider\">

      <div class=\"row featurette\">
          <div class=\"col-md-7\">
              <h2 class=\"featurette-heading\"> <span class=\"glyphicon glyphicon-cog\"></span> Création de compte. <span class=\"text-muted\"> Remplissez <a class=\"homelink\" href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_askingpage");
        echo "\">ce formulaire</a></span></h2>
              <p class=\"lead\">Avant de pouvoir proposer un sujet de projet, il vous faut un compte, remplissez le formulaire et attendez de recevoir un mail de confirmation avec vos idenifiants.</p>
          </div>
          <div class=\"col-md-5\">
              <a href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_askingpage");
        echo "\"><img class=\"featurette-image img-responsive center-block\"  alt=\"Generic placeholder image\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/images/one.png"), "html", null, true);
        echo "\" height='200' width=\"200\">
          </a>
          </div>
      </div>

      <hr class=\"featurette-divider\">

      <div class=\"row featurette\">
          <div class=\"col-md-7 col-md-push-5\">
              <h2 class=\"featurette-heading\"> <span class=\"glyphicon glyphicon-lock\"></span> Connectez vous ! <span class=\"text-muted\"> <a class=\"homelink\" href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
        echo "\"> Page de connexion</a></span></h2>
              <p class=\"lead\"> A partir de maintenant vous allez pouvoir faire des propositions de projet.</p>
          </div>
          <div class=\"col-md-5 col-md-pull-7\">
              <img class=\"featurette-image img-responsive center-block\" data-src=\"holder.js/500x500/auto\" alt=\"Generic placeholder image\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/images/two.png"), "html", null, true);
        echo "\" height='200' width=\"200\">
          </div>
      </div>

      <hr class=\"featurette-divider\">

      <div class=\"row featurette\">
          <div class=\"col-md-7\">
              <h2 class=\"featurette-heading\"> <span class=\"glyphicon glyphicon-th-list\"></span>Remplissez les formulaires de propositions.<span class=\"text-muted\"> Et voilà !</span></h2>
              <p class=\"lead\">Remplissez <a href=\"";
        // line 45
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_propositionPage");
        echo "\" class=\"homelink\"  > le formulaire</a> et vous serez notifié quand la proposition de projet aura été acceptée.</p>
          </div>
          <div class=\"col-md-5\">
              <img class=\"featurette-image img-responsive center-block\" data-src=\"holder.js/500x500/auto\" alt=\"Generic placeholder image\" src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/images/three.png"), "html", null, true);
        echo "\" height='200' width=\"200\">
          </div>
      </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 48,  93 => 45,  81 => 36,  74 => 32,  60 => 23,  53 => 19,  39 => 7,  36 => 6,  29 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/*   <div class="container  col-md-12 col-lg-12">*/
/* */
/*       <div class="starter-template">*/
/*         <h1>Proposer un projet à l'ENSICAEN en <span style="background-color: #2b669a; color: white"> 3  étapes </span></h1>*/
/* */
/*       </div>*/
/* */
/*       <hr class="featurette-divider">*/
/* */
/*       <div class="row featurette">*/
/*           <div class="col-md-7">*/
/*               <h2 class="featurette-heading"> <span class="glyphicon glyphicon-cog"></span> Création de compte. <span class="text-muted"> Remplissez <a class="homelink" href="{{ path('gestion_projet_home_platform_askingpage') }}">ce formulaire</a></span></h2>*/
/*               <p class="lead">Avant de pouvoir proposer un sujet de projet, il vous faut un compte, remplissez le formulaire et attendez de recevoir un mail de confirmation avec vos idenifiants.</p>*/
/*           </div>*/
/*           <div class="col-md-5">*/
/*               <a href="{{ path('gestion_projet_home_platform_askingpage') }}"><img class="featurette-image img-responsive center-block"  alt="Generic placeholder image" src="{{ asset('bundles/HomePlatformBundle/images/one.png') }}" height='200' width="200">*/
/*           </a>*/
/*           </div>*/
/*       </div>*/
/* */
/*       <hr class="featurette-divider">*/
/* */
/*       <div class="row featurette">*/
/*           <div class="col-md-7 col-md-push-5">*/
/*               <h2 class="featurette-heading"> <span class="glyphicon glyphicon-lock"></span> Connectez vous ! <span class="text-muted"> <a class="homelink" href="{{ path('fos_user_security_login') }}"> Page de connexion</a></span></h2>*/
/*               <p class="lead"> A partir de maintenant vous allez pouvoir faire des propositions de projet.</p>*/
/*           </div>*/
/*           <div class="col-md-5 col-md-pull-7">*/
/*               <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="{{ asset('bundles/HomePlatformBundle/images/two.png') }}" height='200' width="200">*/
/*           </div>*/
/*       </div>*/
/* */
/*       <hr class="featurette-divider">*/
/* */
/*       <div class="row featurette">*/
/*           <div class="col-md-7">*/
/*               <h2 class="featurette-heading"> <span class="glyphicon glyphicon-th-list"></span>Remplissez les formulaires de propositions.<span class="text-muted"> Et voilà !</span></h2>*/
/*               <p class="lead">Remplissez <a href="{{ path('gestion_projet_home_platform_propositionPage') }}" class="homelink"  > le formulaire</a> et vous serez notifié quand la proposition de projet aura été acceptée.</p>*/
/*           </div>*/
/*           <div class="col-md-5">*/
/*               <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="{{ asset('bundles/HomePlatformBundle/images/three.png') }}" height='200' width="200">*/
/*           </div>*/
/*       </div>*/
/* */
/*     </div>*/
/* */
/* {% endblock %}*/
/* */
/* */
/* */
