<?php

/* GestionProjetHomePlatformBundle:Default:viewPendingPropositions.html.twig */
class __TwigTemplate_8a900ebc822d77cac41eb14b756c0cc7292043745f66cf5872042d3ab45e2603 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewPendingPropositions.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "




    <h1 class=\"page-header\">Tableau de bord</h1>

    ";
        // line 14
        try {
            $this->loadTemplate("GestionProjetHomePlatformBundle:Default:leftBoard.html.twig", "GestionProjetHomePlatformBundle:Default:viewPendingPropositions.html.twig", 14)->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 15
        echo "

    <div class=\"table-responsive\">
        <h2>Liste de projets</h2>
        ";
        // line 19
        try {
            $this->loadTemplate("GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig", "GestionProjetHomePlatformBundle:Default:viewPendingPropositions.html.twig", 19)->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 20
        echo "    </div>

";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewPendingPropositions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 20,  61 => 19,  55 => 15,  48 => 14,  39 => 7,  36 => 6,  29 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* */
/* */
/* */
/* */
/*     <h1 class="page-header">Tableau de bord</h1>*/
/* */
/*     {% include 'GestionProjetHomePlatformBundle:Default:leftBoard.html.twig' ignore missing %}*/
/* */
/* */
/*     <div class="table-responsive">*/
/*         <h2>Liste de projets</h2>*/
/*         {% include 'GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig' ignore missing %}*/
/*     </div>*/
/* */
/* {% endblock %}*/
