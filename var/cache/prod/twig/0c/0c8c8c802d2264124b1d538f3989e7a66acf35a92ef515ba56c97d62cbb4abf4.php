<?php

/* GestionProjetHomePlatformBundle:Default:viewAccount.html.twig */
class __TwigTemplate_c01210984e0296138f50a1bb1aea6ff6eacba9e86110a4b1933079418a996746 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewAccount.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "
<div class=\"table-responsive\">
    <h2>Liste de compte entreprise</h2>
    <table class=\"table table-striped\">

        <tr>
            <th> ID </th>
            <th> Login </th>
            <th> Email </th>
            <th> Confirmé</th>
            <th> ToolBox</th>
        </tr>


        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listAccount"]) ? $context["listAccount"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["account"]) {
            // line 22
            echo "            <tr>
                <td> ";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["account"], "id", array()), "html", null, true);
            echo " </td>
                <td> ";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["account"], "username", array()), "html", null, true);
            echo " </td>
                <td> ";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["account"], "email", array()), "html", null, true);
            echo " </td>
                <td>
                    ";
            // line 27
            if ($this->getAttribute($context["account"], "enabled", array())) {
                // line 28
                echo "                        <img src=\"http://www.webasun.com/wp-content/themes/webasun-theme-2014/images/coche-verte.png\" alt=\"Validé\" height=\"32\" width=\"32\">
                    ";
            } else {
                // line 30
                echo "                        <img src=\"http://iconizer.net/files/Farm-fresh/orig/hourglass.png\" alt=\"En attente\" height=\"32\" width=\"32\">
                    ";
            }
            // line 32
            echo "

                </td>
                <td> supprimer / modifier / envoyer mail</td>


            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['account'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "
    </table>
</div>
";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewAccount.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 40,  85 => 32,  81 => 30,  77 => 28,  75 => 27,  70 => 25,  66 => 24,  62 => 23,  59 => 22,  55 => 21,  39 => 7,  36 => 6,  29 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* <div class="table-responsive">*/
/*     <h2>Liste de compte entreprise</h2>*/
/*     <table class="table table-striped">*/
/* */
/*         <tr>*/
/*             <th> ID </th>*/
/*             <th> Login </th>*/
/*             <th> Email </th>*/
/*             <th> Confirmé</th>*/
/*             <th> ToolBox</th>*/
/*         </tr>*/
/* */
/* */
/*         {% for account in listAccount %}*/
/*             <tr>*/
/*                 <td> {{ account.id }} </td>*/
/*                 <td> {{ account.username }} </td>*/
/*                 <td> {{ account.email }} </td>*/
/*                 <td>*/
/*                     {% if(account.enabled) %}*/
/*                         <img src="http://www.webasun.com/wp-content/themes/webasun-theme-2014/images/coche-verte.png" alt="Validé" height="32" width="32">*/
/*                     {% else %}*/
/*                         <img src="http://iconizer.net/files/Farm-fresh/orig/hourglass.png" alt="En attente" height="32" width="32">*/
/*                     {% endif %}*/
/* */
/* */
/*                 </td>*/
/*                 <td> supprimer / modifier / envoyer mail</td>*/
/* */
/* */
/*             </tr>*/
/*         {% endfor %}*/
/* */
/*     </table>*/
/* </div>*/
/* {% endblock %}*/
/* */
/* */
/* */
