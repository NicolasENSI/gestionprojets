<?php

/* GestionProjetHomePlatformBundle:Default:viewAsking.html.twig */
class __TwigTemplate_6e71e8d1242f5e41260e0752f159d9ce488ec15e5a896765266922e708b8042a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewAsking.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "
    <div class=\"table-responsive\">
        <h2>Liste des demandes de création de compte</h2>
        <table class=\"table table-striped\">

            <tr>
                <th> Entrerprise</th>
                <th> Nom </th>
                <th> Prénom </th>
                <th> Créer le compte</th>
            </tr>


            ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listAsking"]) ? $context["listAsking"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["account"]) {
            // line 21
            echo "                <tr>
                    <td> ";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["account"], "entreprise", array()), "html", null, true);
            echo " </td>
                    <td> ";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["account"], "nom", array()), "html", null, true);
            echo " </td>
                    <td> ";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["account"], "prenom", array()), "html", null, true);
            echo " </td>
                    <td><a href=\"";
            // line 25
            echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_add_account");
            echo "\">Créer le compte</a>  </td>


                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['account'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "
        </table>
    </div>
";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewAsking.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 30,  73 => 25,  69 => 24,  65 => 23,  61 => 22,  58 => 21,  54 => 20,  39 => 7,  36 => 6,  29 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/*     <div class="table-responsive">*/
/*         <h2>Liste des demandes de création de compte</h2>*/
/*         <table class="table table-striped">*/
/* */
/*             <tr>*/
/*                 <th> Entrerprise</th>*/
/*                 <th> Nom </th>*/
/*                 <th> Prénom </th>*/
/*                 <th> Créer le compte</th>*/
/*             </tr>*/
/* */
/* */
/*             {% for account in listAsking %}*/
/*                 <tr>*/
/*                     <td> {{ account.entreprise }} </td>*/
/*                     <td> {{ account.nom}} </td>*/
/*                     <td> {{ account.prenom}} </td>*/
/*                     <td><a href="{{ path('gestion_projet_home_platform_add_account') }}">Créer le compte</a>  </td>*/
/* */
/* */
/*                 </tr>*/
/*             {% endfor %}*/
/* */
/*         </table>*/
/*     </div>*/
/* {% endblock %}*/
