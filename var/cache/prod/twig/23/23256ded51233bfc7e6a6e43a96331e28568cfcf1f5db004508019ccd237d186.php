<?php

/* GestionProjetHomePlatformBundle:Default:askAccount.html.twig */
class __TwigTemplate_b6c12c79b6a990a4300048fcdf2b3dcb62000b31a34aac3907660ff2090a3335 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "





<!doctype html>
<html lang=\"en\" class=\"no-js\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,700,400' rel='stylesheet' type='text/css'>
    <link rel=\"stylesheet\" href=\" ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/gestionprojethomeplatform/css/bootstrap.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/css/reset.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/css/style.css"), "html", null, true);
        echo "\">


    <script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/js/modernizr.js"), "html", null, true);
        echo "\"  ></script> <!-- Modernizr -->

    <title>ENSICAEN - Proposition  de projet</title>
</head>
<body>
<section class=\"cd-intro\">
    <div class=\"pull-left\"><span class=\"glyphicon glyphicon-remove \"></span> </div>
    <div class=\"cd-intro-content mask\">

        <h1 data-content=\"Demande de création de compte \"><span>Demande de création de compte</span></h1>
        <div class=\"action-wrapper\">
            ";
        // line 31
        echo "
            ";
        // line 32
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 93
        echo "        </div>
    </div>
</section>

<script src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/js/jquery-2.1.4.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/js/js/main.js"), "html", null, true);
        echo "\"></script> <!-- Resource jQuery -->
</body>
</html>



";
    }

    // line 32
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 33
        echo "

                <div class=\"\">





                    ";
        // line 41
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("class" => "form-horizontal")));
        echo "

                    <div class=\"form-group\">
                        <label for=\"name\" class=\"\"> Entreprise ou établissement  </label>
                        <div class=\"col-sm-10\">
                            ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entreprise", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label for=\"name\" class=\"col-sm-2 control-label\">Nom</label>
                        <div class=\"col-sm-10\">
                            ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nom", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label for=\"name\" class=\"col-sm-2 control-label\">Prénom</label>
                        <div class=\"col-sm-10\">
                            ";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "prenom", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label for=\"email\" class=\"col-sm-2 control-label\">Email</label>
                        <div class=\"col-sm-10\">
                            ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mail", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label for=\"message\" class=\"col-sm-2 control-label\">Message</label>
                        <div class=\"col-sm-10\">
                            ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "message", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                        </div>
                    </div>


                    <div class=\"form-group\">
                        ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "save", array()), 'widget', array("attr" => array("class" => "btn btn-primary col-sm-3 col-sm-offset-2")));
        echo "
                    </div>



                    ";
        // line 85
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "




                </div>

            ";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:askAccount.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 85,  159 => 80,  150 => 74,  140 => 67,  130 => 60,  120 => 53,  110 => 46,  102 => 41,  92 => 33,  89 => 32,  78 => 98,  74 => 97,  68 => 93,  66 => 32,  63 => 31,  49 => 19,  43 => 16,  39 => 15,  35 => 14,  20 => 1,);
    }
}
/* */
/* */
/* */
/* */
/* */
/* */
/* <!doctype html>*/
/* <html lang="en" class="no-js">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* */
/*     <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,700,400' rel='stylesheet' type='text/css'>*/
/*     <link rel="stylesheet" href=" {{ asset('bundles/gestionprojethomeplatform/css/bootstrap.css') }}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/HomePlatformBundle/css/reset.css')}}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/HomePlatformBundle/css/style.css')}}">*/
/* */
/* */
/*     <script src="{{ asset('bundles/HomePlatformBundle/js/modernizr.js')}}"  ></script> <!-- Modernizr -->*/
/* */
/*     <title>ENSICAEN - Proposition  de projet</title>*/
/* </head>*/
/* <body>*/
/* <section class="cd-intro">*/
/*     <div class="pull-left"><span class="glyphicon glyphicon-remove "></span> </div>*/
/*     <div class="cd-intro-content mask">*/
/* */
/*         <h1 data-content="Demande de création de compte "><span>Demande de création de compte</span></h1>*/
/*         <div class="action-wrapper">*/
/*             {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/*             {% block fos_user_content %}*/
/* */
/* */
/*                 <div class="">*/
/* */
/* */
/* */
/* */
/* */
/*                     {{ form_start(form, {'attr': {'class': 'form-horizontal'}}) }}*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="name" class=""> Entreprise ou établissement  </label>*/
/*                         <div class="col-sm-10">*/
/*                             {{ form_widget(form.entreprise, {'attr': {'class': 'form-control'}}) }}*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="name" class="col-sm-2 control-label">Nom</label>*/
/*                         <div class="col-sm-10">*/
/*                             {{ form_widget(form.nom,{'attr': {'class': 'form-control'}}) }}*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="name" class="col-sm-2 control-label">Prénom</label>*/
/*                         <div class="col-sm-10">*/
/*                             {{ form_widget(form.prenom, {'attr': {'class': 'form-control'}}) }}*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="email" class="col-sm-2 control-label">Email</label>*/
/*                         <div class="col-sm-10">*/
/*                             {{ form_widget(form.mail, {'attr': {'class': 'form-control'}}) }}*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label for="message" class="col-sm-2 control-label">Message</label>*/
/*                         <div class="col-sm-10">*/
/*                             {{ form_widget(form.message, {'attr': {'class': 'form-control'}}) }}*/
/*                         </div>*/
/*                     </div>*/
/* */
/* */
/*                     <div class="form-group">*/
/*                         {{ form_widget(form.save, {'attr': {'class': 'btn btn-primary col-sm-3 col-sm-offset-2'}}) }}*/
/*                     </div>*/
/* */
/* */
/* */
/*                     {{ form_end(form) }}*/
/* */
/* */
/* */
/* */
/*                 </div>*/
/* */
/*             {% endblock fos_user_content %}*/
/*         </div>*/
/*     </div>*/
/* </section>*/
/* */
/* <script src="{{ asset('bundles/HomePlatformBundle/js/jquery-2.1.4.js')}}"></script>*/
/* <script src="{{ asset('bundles/HomePlatformBundle/js/js/main.js')}}"></script> <!-- Resource jQuery -->*/
/* </body>*/
/* </html>*/
/* */
/* */
/* */
/* */
