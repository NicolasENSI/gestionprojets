<?php

/* GestionProjetHomePlatformBundle:Default:viewValidatePropositions.html.twig */
class __TwigTemplate_339b358300bc0ce27eda9c9bd7eb07d6602ccbac621186ab5e62ead72e11cf99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:viewValidatePropositions.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "





    <h1 class=\"page-header\">Tableau de bord</h1>
    ";
        // line 14
        try {
            $this->loadTemplate("GestionProjetHomePlatformBundle:Default:leftBoard.html.twig", "GestionProjetHomePlatformBundle:Default:viewValidatePropositions.html.twig", 14)->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 15
        echo "

    <div class=\"table-responsive\">
        <h2>Liste de projets <validés></validés></h2>
        ";
        // line 19
        $this->loadTemplate("@GestionProjetHomePlatform/Default/headerArrayProposition.html.twig", "GestionProjetHomePlatformBundle:Default:viewValidatePropositions.html.twig", 19)->display($context);
        // line 20
        echo "
";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:viewValidatePropositions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 20,  61 => 19,  55 => 15,  48 => 14,  39 => 7,  36 => 6,  29 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/*     <h1 class="page-header">Tableau de bord</h1>*/
/*     {% include 'GestionProjetHomePlatformBundle:Default:leftBoard.html.twig' ignore missing %}*/
/* */
/* */
/*     <div class="table-responsive">*/
/*         <h2>Liste de projets <validés></validés></h2>*/
/*         {% include '@GestionProjetHomePlatform/Default/headerArrayProposition.html.twig'  %}*/
/* */
/* {% endblock %}*/
