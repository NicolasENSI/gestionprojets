<?php

/* GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig */
class __TwigTemplate_8ee65a36917d968dd68ee4f493e902b4b22f946c982e6f9544c469745cb20583 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'hearderproposition' => array($this, 'block_hearderproposition'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('hearderproposition', $context, $blocks);
    }

    public function block_hearderproposition($context, array $blocks = array())
    {
        // line 2
        echo "    <table class=\"table table-striped\">

        <tr>
            <th> Entreprise</th>
            <th> Intitulé du projet</th>
            <th> Proposé le</th>
            <th> Description</th>
            <th> Status</th>
            <th> Détails</th>
        </tr>


        ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listProposition"]) ? $context["listProposition"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["proposition"]) {
            // line 15
            echo "            <tr>
                <td> ";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "entreprise", array()), "html", null, true);
            echo " </td>
                <td> ";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "intituleProjet", array()), "html", null, true);
            echo " </td>
                <td> ";
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["proposition"], "insertDate", array()), "m/d/Y"), "html", null, true);
            echo "</td>
                <td> ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["proposition"], "description", array()), "html", null, true);
            echo " </td>
                ";
            // line 20
            if (($this->getAttribute($context["proposition"], "valide", array()) == 0)) {
                // line 21
                echo "                    <td>
                        <button type=\"button\" class=\"btn btn-info\">En attente</button>
                    </td>
                ";
            }
            // line 25
            echo "                ";
            if (($this->getAttribute($context["proposition"], "valide", array()) == 1)) {
                // line 26
                echo "                    <td>
                        <button type=\"button\" class=\"btn btn-success\">Validé</button>
                    </td>
                ";
            }
            // line 30
            echo "
                ";
            // line 31
            if (($this->getAttribute($context["proposition"], "valide", array()) == 2)) {
                // line 32
                echo "                    <td>
                        <button type=\"button\" class=\"btn btn-danger\">Refusé</button>
                    </td>
                ";
            }
            // line 36
            echo "                <td><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_propostion_in_detail", array("id" => $this->getAttribute($context["proposition"], "id", array()))), "html", null, true);
            echo "\">
                        <button type=\"button\" class=\"btn btn-default\">Voir détails &raquo;</button>
                    </a></td>


            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['proposition'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "    </table>
";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:headerArrayProposition.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  105 => 43,  91 => 36,  85 => 32,  83 => 31,  80 => 30,  74 => 26,  71 => 25,  65 => 21,  63 => 20,  59 => 19,  55 => 18,  51 => 17,  47 => 16,  44 => 15,  40 => 14,  26 => 2,  20 => 1,);
    }
}
/* {% block hearderproposition %}*/
/*     <table class="table table-striped">*/
/* */
/*         <tr>*/
/*             <th> Entreprise</th>*/
/*             <th> Intitulé du projet</th>*/
/*             <th> Proposé le</th>*/
/*             <th> Description</th>*/
/*             <th> Status</th>*/
/*             <th> Détails</th>*/
/*         </tr>*/
/* */
/* */
/*         {% for proposition in listProposition %}*/
/*             <tr>*/
/*                 <td> {{ proposition.entreprise }} </td>*/
/*                 <td> {{ proposition.intituleProjet }} </td>*/
/*                 <td> {{ proposition.insertDate|date("m/d/Y") }}</td>*/
/*                 <td> {{ proposition.description }} </td>*/
/*                 {% if  proposition.valide  == 0 %}*/
/*                     <td>*/
/*                         <button type="button" class="btn btn-info">En attente</button>*/
/*                     </td>*/
/*                 {% endif %}*/
/*                 {% if proposition.valide  == 1 %}*/
/*                     <td>*/
/*                         <button type="button" class="btn btn-success">Validé</button>*/
/*                     </td>*/
/*                 {% endif %}*/
/* */
/*                 {% if proposition.valide  == 2 %}*/
/*                     <td>*/
/*                         <button type="button" class="btn btn-danger">Refusé</button>*/
/*                     </td>*/
/*                 {% endif %}*/
/*                 <td><a href="{{ path('gestion_projet_home_platform_propostion_in_detail', {'id': proposition.id}) }}">*/
/*                         <button type="button" class="btn btn-default">Voir détails &raquo;</button>*/
/*                     </a></td>*/
/* */
/* */
/*             </tr>*/
/*         {% endfor %}*/
/*     </table>*/
/* {% endblock %}*/
