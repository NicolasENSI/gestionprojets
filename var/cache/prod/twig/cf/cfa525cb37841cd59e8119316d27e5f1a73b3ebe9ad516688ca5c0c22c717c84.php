<?php

/* GestionProjetHomePlatformBundle:Default:success.html.twig */
class __TwigTemplate_bfa43852585a6774432d2ecc94ae79d4928233b305b8ba4fbcf646f5bd808847 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
   <!-- HTML meta refresh URL redirection -->
   <meta http-equiv=\"refresh\"
   content=\"4; url=";
        // line 6
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_homepage");
        echo "\">
</head>
<body>
   Félicitation votre proposition de stage a bien été envoyée. Vous allez être redirigé vers l'acceuil dans 3 secondes.
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 6,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/*    <!-- HTML meta refresh URL redirection -->*/
/*    <meta http-equiv="refresh"*/
/*    content="4; url={{ path('gestion_projet_home_platform_homepage') }}">*/
/* </head>*/
/* <body>*/
/*    Félicitation votre proposition de stage a bien été envoyée. Vous allez être redirigé vers l'acceuil dans 3 secondes.*/
/* </body>*/
/* </html>*/
