<?php

/* GestionProjetHomePlatformBundle:Default:indexPromo.html.twig */
class __TwigTemplate_0364df44745dab53391c47a55b6db1e479b85a30dc77850f5523032216398a3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"en\" class=\"no-js\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,700,400' rel='stylesheet' type='text/css'>

    <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/css/reset.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/css/style.css"), "html", null, true);
        echo "\">
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/js/modernizr.js"), "html", null, true);
        echo "\"  ></script> <!-- Modernizr -->

    <title>ENSICAEN - Proposition  de projet</title>
</head>
<body>
<section class=\"cd-intro\">
    <div class=\"cd-intro-content mask\">
        <h1 data-content=\"Propostion de stages à l'ENSICAEN\"><span>Propostion de stages à l'ENSICAEN</span></h1>
        <div class=\"action-wrapper\">
            <p>
                <a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_propositionPage");
        echo "\" class=\"cd-btn main-action\">Proposer un stage</a>
                <a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_modalites");
        echo "\" class=\"cd-btn\">En savoir plus</a>
            </p>
        </div>
    </div>
</section>

<script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/js/jquery-2.1.4.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/HomePlatformBundle/js/js/main.js"), "html", null, true);
        echo "\"></script> <!-- Resource jQuery -->
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:indexPromo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 29,  63 => 28,  54 => 22,  50 => 21,  37 => 11,  33 => 10,  29 => 9,  19 => 1,);
    }
}
/* <!doctype html>*/
/* <html lang="en" class="no-js">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* */
/*     <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,700,400' rel='stylesheet' type='text/css'>*/
/* */
/*     <link rel="stylesheet" href="{{ asset('bundles/HomePlatformBundle/css/reset.css')}}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/HomePlatformBundle/css/style.css')}}">*/
/*     <script src="{{ asset('bundles/HomePlatformBundle/js/modernizr.js')}}"  ></script> <!-- Modernizr -->*/
/* */
/*     <title>ENSICAEN - Proposition  de projet</title>*/
/* </head>*/
/* <body>*/
/* <section class="cd-intro">*/
/*     <div class="cd-intro-content mask">*/
/*         <h1 data-content="Propostion de stages à l'ENSICAEN"><span>Propostion de stages à l'ENSICAEN</span></h1>*/
/*         <div class="action-wrapper">*/
/*             <p>*/
/*                 <a href="{{ path("gestion_projet_home_platform_propositionPage") }}" class="cd-btn main-action">Proposer un stage</a>*/
/*                 <a href="{{ path("gestion_projet_home_platform_modalites") }}" class="cd-btn">En savoir plus</a>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* </section>*/
/* */
/* <script src="{{ asset('bundles/HomePlatformBundle/js/jquery-2.1.4.js')}}"></script>*/
/* <script src="{{ asset('bundles/HomePlatformBundle/js/js/main.js')}}"></script> <!-- Resource jQuery -->*/
/* </body>*/
/* </html>*/
