<?php

/* GestionProjetHomePlatformBundle:Emails:acceptation.html.twig */
class __TwigTemplate_1dfb99b435f0795d5194cee80f3b4ed5170750ed01b0ea83e23df016f94b3536 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<h3>Félicitation ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo ", votre projet \"";
        echo twig_escape_filter($this->env, (isset($context["titre"]) ? $context["titre"] : null), "html", null, true);
        echo "\"est validé</h3>

";
        // line 5
        echo "<h4> Nous allons vous recontacter pour plus de détails..</h4>

<h4>En attendant vous pouvez aller voir l'état de vos autres projets <a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_view_own_propositions");
        echo "\"> ici </a> </h4>

<h4>Vous pouvez également proposer d'autres projet <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_propositionPage");
        echo "\"> ici </a> </h4>";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Emails:acceptation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 9,  31 => 7,  27 => 5,  19 => 2,);
    }
}
/* {# app/Resources/views/Emails/registration.html.twig #}*/
/* <h3>Félicitation {{ name }}, votre projet "{{ titre }}"est validé</h3>*/
/* */
/* {# example, assuming you have a route named "login" #}*/
/* <h4> Nous allons vous recontacter pour plus de détails..</h4>*/
/* */
/* <h4>En attendant vous pouvez aller voir l'état de vos autres projets <a href="{{ path('gestion_projet_home_platform_view_own_propositions') }}"> ici </a> </h4>*/
/* */
/* <h4>Vous pouvez également proposer d'autres projet <a href="{{ path('gestion_projet_home_platform_propositionPage') }}"> ici </a> </h4>*/
