<?php

/* base.html.twig */
class __TwigTemplate_87dd813f299056de6393b5fc0b5f74f3c5cd397c92cc1414b53eb628d3aae97c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
   <!-- HTML meta refresh URL redirection -->
   <meta http-equiv=\"refresh\"
   content=\"0; url=";
        // line 6
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_homepage");
        echo "\">
</head>
<body>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 6,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/*    <!-- HTML meta refresh URL redirection -->*/
/*    <meta http-equiv="refresh"*/
/*    content="0; url={{ path('gestion_projet_home_platform_homepage') }}">*/
/* </head>*/
/* <body>*/
/* </body>*/
/* </html>*/
/* */
