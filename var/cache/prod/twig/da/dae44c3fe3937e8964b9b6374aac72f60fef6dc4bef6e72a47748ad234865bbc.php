<?php

/* GestionProjetHomePlatformBundle:Default:leftBoard.html.twig */
class __TwigTemplate_c4dec1f85384c61374ab52ddb6fec5f55e8052b7dd792056c195d8cfe3fae9d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'board' => array($this, 'block_board'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "


";
        // line 4
        $this->displayBlock('board', $context, $blocks);
        // line 25
        echo "
";
    }

    // line 4
    public function block_board($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"row placeholders\">

        <div class=\"col-xs-6 col-sm-3 col-sm-offset-1 placeholder\">
            <img src=\"http://www.icone-png.com/png/54/53901.png\" width=\"150\" height=\"150\" class=\"img-responsive\" alt=\"Generic placeholder thumbnail\">
            <h4> <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_validate");
        echo "\"> Validés (";
        echo twig_escape_filter($this->env, $this->env->getExtension('home_paltform_function')->getNumberOfValide(), "html", null, true);
        echo ")</a></h4>
            <span class=\"text-muted\"></span>
        </div>
        <div class=\"col-xs-6 col-sm-3 placeholder\">
            <img src=\"http://ihourlyquiz.com/images/twitquizlogo.jpg\" width=\"150\" height=\"150\" class=\"img-responsive\" alt=\"Generic placeholder thumbnail\">
            <h4><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_pending");
        echo "\"> En attente (";
        echo twig_escape_filter($this->env, $this->env->getExtension('home_paltform_function')->getNumberOfPending(), "html", null, true);
        echo ")</a></h4>
            <span class=\"text-muted\"></span>
        </div>
        <div class=\"col-xs-6 col-sm-3 placeholder\">
            <img src=\"https://pixabay.com/static/uploads/photo/2013/07/12/12/40/abort-146072_960_720.png\" width=\"150\" height=\"150\" class=\"img-responsive\" alt=\"Generic placeholder thumbnail\">
            <h4><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("gestion_projet_home_platform_notValidate");
        echo "\"> Non validés (";
        echo twig_escape_filter($this->env, $this->env->getExtension('home_paltform_function')->getNumberOfNotValide(), "html", null, true);
        echo ")</a></h4>
            <span class=\"text-muted\"></span>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:leftBoard.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  61 => 19,  51 => 14,  41 => 9,  35 => 5,  32 => 4,  27 => 25,  25 => 4,  20 => 1,);
    }
}
/* */
/* */
/* */
/* {% block board %}*/
/*     <div class="row placeholders">*/
/* */
/*         <div class="col-xs-6 col-sm-3 col-sm-offset-1 placeholder">*/
/*             <img src="http://www.icone-png.com/png/54/53901.png" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">*/
/*             <h4> <a href="{{ path('gestion_projet_home_platform_validate') }}"> Validés ({{ getNumberOfValide() }})</a></h4>*/
/*             <span class="text-muted"></span>*/
/*         </div>*/
/*         <div class="col-xs-6 col-sm-3 placeholder">*/
/*             <img src="http://ihourlyquiz.com/images/twitquizlogo.jpg" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">*/
/*             <h4><a href="{{ path('gestion_projet_home_platform_pending') }}"> En attente ({{ getNumberOfPending() }})</a></h4>*/
/*             <span class="text-muted"></span>*/
/*         </div>*/
/*         <div class="col-xs-6 col-sm-3 placeholder">*/
/*             <img src="https://pixabay.com/static/uploads/photo/2013/07/12/12/40/abort-146072_960_720.png" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">*/
/*             <h4><a href="{{ path('gestion_projet_home_platform_notValidate') }}"> Non validés ({{ getNumberOfNotValide() }})</a></h4>*/
/*             <span class="text-muted"></span>*/
/*         </div>*/
/*     </div>*/
/* */
/* {%endblock%}*/
/* */
/* */
