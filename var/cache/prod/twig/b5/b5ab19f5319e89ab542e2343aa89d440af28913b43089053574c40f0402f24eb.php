<?php

/* GestionProjetHomePlatformBundle:Default:proposition.html.twig */
class __TwigTemplate_a730a04fb4e35c48a7377b91256a4f2483cad64e4b3d424da8c320f92cb38aca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("GestionProjetHomePlatformBundle::layout.html.twig", "GestionProjetHomePlatformBundle:Default:proposition.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GestionProjetHomePlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Index";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "

    <div class=\"container  col-md-10 col-lg-10 col-md-offset-1\">

        <div class=\"starter-template\">
        <h1>Formulaire de proposition d'un projet </h1></br>
        </div>

        ";
        // line 15
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("class" => "form-horizontal form-prop")));
        echo "

        ";
        // line 18
        echo "

        <div class=\"form-group\">
            ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nom", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label "), "label" => "Nom"));
        echo "
            <div class=\"col-sm-10\">
                ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nom", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "prenom", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Prénom"));
        echo "
            <div class=\"col-sm-10\">
                ";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "prenom", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entreprise", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Entreprise"));
        echo "
            <div class=\"col-sm-10\">
                ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entreprise", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "intituleProjet", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Intitulé du projet"));
        echo "
            <div class=\"col-sm-10\">
                ";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "intituleProjet", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Description du projet"));
        echo "
            <div class=\"col-sm-10\">
                ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mission", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Type de mission (1-etude, 2-prototypage, 3-veille, 4-recherche documentaire)"));
        echo "
            <div class=\"col-sm-10\">
                ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mission", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "accompagnant", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Je viens accompagné de "));
        echo "
            <div class=\"col-sm-2\">
                ";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "accompagnant", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "present", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Je serai présent à la réunion"));
        echo "
            <div class=\"col-sm-1\">
                ";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "present", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            ";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "repas", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Je serai présent au repas"));
        echo "
            <div class=\"col-sm-1\">
                ";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "repas", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
        </div>



        ";
        // line 85
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "repas", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Fichier descriptif (optionnel)"));
        echo "
            ";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "brochure", array()), 'widget');
        echo "
        </div>



        <div class=\"form-group\">
            <div class=\"form-group\">
                ";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "save", array()), 'widget', array("attr" => array("class" => "btn btn-primary col-sm-3 col-sm-offset-2 form-prop-save")));
        echo "
            </div>
        </div>
    ";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "



    </div>

";
    }

    public function getTemplateName()
    {
        return "GestionProjetHomePlatformBundle:Default:proposition.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 96,  191 => 93,  181 => 86,  177 => 85,  168 => 79,  163 => 77,  155 => 72,  150 => 70,  142 => 65,  137 => 63,  129 => 58,  124 => 56,  116 => 51,  111 => 49,  103 => 44,  98 => 42,  90 => 37,  85 => 35,  77 => 30,  72 => 28,  64 => 23,  59 => 21,  54 => 18,  49 => 15,  39 => 7,  36 => 6,  29 => 4,  11 => 1,);
    }
}
/* {% extends "GestionProjetHomePlatformBundle::layout.html.twig" %}*/
/* */
/* */
/* {% block title %}{{ parent() }} - Index{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* */
/*     <div class="container  col-md-10 col-lg-10 col-md-offset-1">*/
/* */
/*         <div class="starter-template">*/
/*         <h1>Formulaire de proposition d'un projet </h1></br>*/
/*         </div>*/
/* */
/*         {{ form_start(form, {'attr': {'class': 'form-horizontal form-prop'}}) }}*/
/* */
/*         {# Le formulaire, avec URL de soumission vers la routea completer comme on l'a vu #}*/
/* */
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.nom, "Nom", {'label_attr': {'class': 'col-sm-2 control-label '}}) }}*/
/*             <div class="col-sm-10">*/
/*                 {{ form_widget(form.nom, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.prenom, "Prénom", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-10">*/
/*                 {{ form_widget(form.prenom, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.entreprise, "Entreprise", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-10">*/
/*                 {{ form_widget(form.entreprise, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.intituleProjet, "Intitulé du projet", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-10">*/
/*                 {{ form_widget(form.intituleProjet, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.description, "Description du projet", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-10">*/
/*                 {{ form_widget(form.description, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.mission, "Type de mission (1-etude, 2-prototypage, 3-veille, 4-recherche documentaire)", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-10">*/
/*                 {{ form_widget(form.mission, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.accompagnant, "Je viens accompagné de ", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-2">*/
/*                 {{ form_widget(form.accompagnant, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.present, "Je serai présent à la réunion", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-1">*/
/*                 {{ form_widget(form.present, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="form-group">*/
/*             {{ form_label(form.repas, "Je serai présent au repas", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             <div class="col-sm-1">*/
/*                 {{ form_widget(form.repas, {'attr': {'class': 'form-control'}}) }}*/
/*             </div>*/
/*         </div>*/
/* */
/* */
/* */
/*         {{ form_label(form.repas, "Fichier descriptif (optionnel)", {'label_attr': {'class': 'col-sm-2 control-label'}}) }}*/
/*             {{ form_widget(form.brochure) }}*/
/*         </div>*/
/* */
/* */
/* */
/*         <div class="form-group">*/
/*             <div class="form-group">*/
/*                 {{ form_widget(form.save, {'attr': {'class': 'btn btn-primary col-sm-3 col-sm-offset-2 form-prop-save'}}) }}*/
/*             </div>*/
/*         </div>*/
/*     {{ form_rest(form) }}*/
/* */
/* */
/* */
/*     </div>*/
/* */
/* {% endblock %}*/
/* */
